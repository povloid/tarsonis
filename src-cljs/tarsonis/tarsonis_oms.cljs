(ns tarsonis.tarsonis-oms

  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [put! chan <!]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]

            ;;[sablono.core :as html :refer-macros [html]]

            [ixinfestor.net :as ixnet]
            [ixinfestor.omut :as omut]

            ))





#_(ixnet/get-data
   "/tc/rb/product/test"
   {}
   (fn [r]
     (println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" r)))



;;**************************************************************************************************
;;* BEGIN tag
;;* tag: <tag>
;;*
;;* description: Элементы управления тегами
;;*
;;**************************************************************************************************

(def webdoctag-select-view-app-init
  {:webdoctag-edit-table []
   :keyname-loockup {}
   })


(defn webdoctag-select-view [app owner {:keys [chan-update]}]
  (reify
    om/IWillMount
    (will-mount [this]
      (when chan-update
        (go
          (while true
            (let [id (<! chan-update)]
              (ixnet/get-data
               "/tc/rb/product/tags-edit-table"
               {:id id}
               (fn [webdoctag-edit-table]
                 (om/transact! app
                               (fn [app]
                                 (assoc app
                                        :keyname-loockup (->> webdoctag-edit-table
                                                              (reduce into)
                                                              (reduce #(assoc % (%2 :id) %2) {}))
                                        :webdoctag-edit-table (->> webdoctag-edit-table
                                                                   (map vec)
                                                                   vec)))))))))))
    om/IRender
    (render [_]
      (println @app)
      (->> @app
           :webdoctag-edit-table
           (map (fn [i tags-group]
                  (dom/div
                   nil
                   (dom/a #js {:name (str "gr-" (-> tags-group first :id))
                               :style #js {:display "block"
                                           :height 170
                                           :marginTop -170
                                           :visibility "hidden"}})
                   (dom/table #js {:className "table table-striped table-condensed table-responsive"}
                              (dom/thead nil (dom/tr nil (dom/th nil (-> tags-group first :tagname))))
                              (apply
                               dom/tbody nil
                               (map
                                (fn [{tag-id :id contain? :contain? path :path :as tag} j]
                                  (let [[this-tag-id & path-to] (reverse path)
                                        {:keys [tagname const]} (get-in @app [:keyname-loockup this-tag-id])
                                        css-const (if const "text-warning" "text-primary")]

                                    (dom/tr #js {:onClick (fn [_]
                                                            (om/transact! app [:webdoctag-edit-table i j :contain?] not)
                                                            1)
                                                 :className (if contain? "success" "")
                                                 :style #js {:cursor "pointer"}}
                                            (apply
                                             dom/td nil
                                             (reduce
                                              (fn [a x]
                                                (let [{:keys [tagname const]} (get-in app [:keyname-loockup x])
                                                      css-const (if const "text-warning" "text-success")]
                                                  (conj a (dom/span #js {:className css-const}
                                                                    (dom/span #js {:className (str "glyphicon glyphicon-asterisk " css-const)
                                                                                   :aria-hidden "true"
                                                                                   :style #js {:marginLeft 10}})
                                                                    "    " tagname))))
                                              (list (dom/span #js {:className css-const}
                                                              (dom/span #js {:className (str "glyphicon glyphicon-asterisk " css-const)
                                                                             :aria-hidden "true"
                                                                             :style #js {:marginLeft 10}})
                                                              "    " tagname))
                                              path-to)
                                             ) )))
                                tags-group (range)) ))
                   )) (range))
           (apply dom/div nil) ))))

;; END tag
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN product
;;* tag: <product>
;;*
;;* description: Элементы управления продуктом
;;*
;;**************************************************************************************************

;;------------------------------------------------------------------------------
;; BEGIN: AX input elements
;; tag: <ax input>
;; description: Элементы ввода количества товара
;;------------------------------------------------------------------------------


;; Функция-сервис для выполнения сложных вычислений
(defn ax-calc-symbolic-expression [expr f]
  (ixnet/get-data "/tc/opt/rest/ax/calc" {:expr expr}
                  (fn [{:keys [result]}]
                    ;; (println "AX CALC: " (str expr) " = " result)
                    (f result))))



;;----------------------------------------------------------
;; BEGIN: AX input
;; tag: <input ax>
;; description: Элемент ввода количества
;;----------------------------------------------------------

(defn input-ax [app _ {:keys [edit-base-measure?]}]
  (reify
    om/IRender
    (render [_]
      (let [[tip _] app]
        (condp = tip
          :bigint
          (dom/input #js {:value (app 1)
                          :onChange (fn [e]
                                      (let [v (.. e -target -value)]
                                        (om/update! app 1 (new js/Number v))))
                          :placeholder "Целое число"
                          :type "number" :min "0" :step "1"
                          :className "form-control"})

          :numeric
          (dom/input #js {:value (app 1)
                          :onChange (fn [e]
                                      (let [v (.. e -target -value)]
                                        (om/update! app 1 (new js/Number v))))
                          :placeholder "Десятичное число"
                          :type "number" :min "0.000" :step "1.000"
                          :className "form-control"})


          :money
          (dom/input #js {:value (app 1)
                          :onChange (fn [e]
                                      (let [v (.. e -target -value)]
                                        (om/update! app 1 (new js/Number v))))
                          :placeholder "Десятичное число"
                          :type "number" :min "0.00" :step "1.00"
                          :className "form-control"})

          :ratio
          (let [[_ [n d]] app]
            (dom/div nil
                     (dom/div #js {:className "input-group"
                                   :style #js {:maxWidth 150}}
                              (dom/input #js {:value n
                                              :onChange (fn [e]
                                                          (let [v (.. e -target -value)]
                                                            (om/update! app [1 0] (new js/Number v))))
                                              :placeholder "Числитель дроби"
                                              :type "number" :min "0" :step "1"
                                              :className "form-control"})

                              (if edit-base-measure?
                                (dom/input #js {:value d
                                                :onChange (fn [e]
                                                            (let [v (.. e -target -value)]
                                                              (om/update! app [1 1] (new js/Number v))))
                                                :placeholder "Знаминатель дроби"
                                                :type "number" :min "1" :step "1"
                                                :className "form-control"})

                                (dom/span #js {:className "input-group-btn"}
                                          (dom/button #js {:className "btn btn-default" :type "button"}
                                                      " / " d )))
                              )))

          (dom/div nil "Элемент ввода неопределен"))))))

;; END AX input
;;..........................................................




(defn qpanel-caval-info [app _]
  (reify
    om/IRender
    (render [_]
      (let [{[tip v] :val :as caval-root} (app 0)]
        (condp = tip
          :bigint
          (dom/div nil v)

          :numeric
          (dom/div nil v)

          :money
          (dom/div nil v)

          :ratio
          (dom/div nil (first v) "/" (second v))

          (dom/div nil "Элемент неопределен"))))))


(defn input-caval [app _]
  (reify
    om/IRender
    (render [_]
      (let [m (reduce (fn [m {:keys [id parent_id]}]
                        (assoc m id parent_id))
                      {(-> app first :id) nil} (rest app))]
        (letfn [(get-path-length [k]
                  (if-let [mk (m k)]
                    (+ 1 (get-path-length mk))
                    1))

                (check-sub-summ? [{:keys [id val]}]
                  (let [f-list (filter #(= id (:parent_id %)) app)]
                    (if (empty? f-list) true
                        (->> f-list
                             (reduce (fn [aval {[tip v] :val}]
                                       (condp = tip
                                         :bigint (update-in aval [1] - v)
                                         :numeric (update-in aval [1] - v)
                                         :money (update-in aval [1] - v)
                                         :ratio (update-in aval [1 0] - (first v))
                                         aval))
                                     val)
                             ((fn [[tip av]]
                                (condp = tip
                                  :bigint  (= av 0)
                                  :numeric (= av 0)
                                  :money   (= av 0)
                                  :ratio   (= (first av) 0)
                                  false)))))))]
          (apply dom/div nil
                 (map (fn [row]
                        (let [check-sub-summ? (check-sub-summ? row)]
                          (dom/div
                           #js {:style #js {:paddingLeft (-> row :id get-path-length (* 6))}}

                           (when (row :parent_id)
                             (dom/span #js {:style #js {:float "left"}
                                            :className "glyphicon glyphicon-plus" :aria-hidden "true"}))

                           (dom/div #js {:className (str "form-group " (if check-sub-summ? "has-success" "has-warning"))}
                                    (dom/label #js {:className "control-label" } (:title row))
                                    (om/build input-ax (:val row))
                                    (when (not check-sub-summ?)
                                      (dom/p #js {:className "text-warning"} "сумма не сходится"))))))
                      app)))))))


(def select-ca-app-init
  {:ca_id nil
   :tip nil
   :list []
   :base-ax []})

(defn select-ca [app _ {:keys [onChange-fn]}]
  (reify
    om/IRender
    (render [_]
      (letfn [(select-ca [id]
                (first (filter #(= id (:id %)) (:list app))))]
        (let [ca_id (:ca_id app)]
          (dom/div #js {:className (str "form-group " (omut/input-css-string-has? app))}
                   (dom/label #js {:className "control-label "} "Мера")
                   (apply
                    dom/select #js {:value (or ca_id 0)
                                    :className "form-control"
                                    :onChange
                                    (fn [e]
                                      (let [{:keys [id tip]} (-> e .-target .-value
                                                                 js/parseInt select-ca)]
                                        (om/transact! app
                                                      (fn [app]
                                                        (assoc app
                                                               :ca_id id
                                                               :tip tip
                                                               :base-ax (condp = tip
                                                                          :bigint  [:bigint  1]
                                                                          :numeric [:numeric 1]
                                                                          :money   [:money   1]
                                                                          :ratio   [:ratio [1 1]]
                                                                          [])
                                                               )))
                                        (when onChange-fn (onChange-fn))))}
                    (map (fn [{:keys [id title]}]
                           (dom/option #js {:value id} title))
                         (into [{:id 0 :title "Выбрать меру"}] (app :list))))
                   (om/build omut/helper-p app {})

                   (om/build input-ax (:base-ax app)
                             {:opts {:edit-base-measure? true}})
                   (dom/b nil (-> app :ca_id select-ca :title))) )))))

;; END AX input elements
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: swops
;; tag: <swops>
;; description: Элементы управления системой обмена
;;------------------------------------------------------------------------------


(def swops-edit-view-app-init
  {:list [] })


(defn swops-edit-view [app owner {:keys [chan-update]}]
  (reify
    om/IWillMount
    (will-mount [this]
      (when chan-update
        (go
          (while true
            (let [id (<! chan-update)]
              (ixnet/get-data
               "/tc/rb/product/swops"
               {:id id}
               (fn [webdoctag-edit-table]
                 (om/transact! app
                               (fn [app]
                                 (assoc app :list (->> webdoctag-edit-table
                                                       (map #(update-in % [:swops-set] vec))
                                                       vec)))))))))))
    om/IRender
    (render [_]
      (apply
       dom/div nil
       (->> app
            :list
            (map
             (fn [i opt-group]
               (dom/div #js {:className "panel panel-default"}
                        (dom/div #js {:className "panel-heading"}
                                 (dom/h4 nil
                                         (:title opt-group)
                                         " - "
                                         (dom/small nil (:description opt-group))))
                        (dom/div #js {:className "panel-body"}
                                 (dom/table #js {:className "table table-striped"}
                                            (dom/thead nil (dom/tr nil (dom/th nil "вид") (dom/th nil "варианты обмена")))
                                            (apply
                                             dom/tbody nil
                                             (->> opt-group
                                                  :swops-set
                                                  (map
                                                   (fn [j swop-row]
                                                     (dom/tr
                                                      nil
                                                      (dom/td
                                                       nil
                                                       (dom/label
                                                        nil
                                                        (dom/input
                                                         #js {:type"checkbox"
                                                              :checked (get-in @app [:list i :swops-set j :swopv :include])
                                                              :onChange (fn [e]
                                                                          (let [v (.. e -target -checked)]
                                                                            (om/update!
                                                                             app [:list i :swops-set j :swopv :include] v)))})
                                                        " " (:title swop-row))
                                                       (dom/div
                                                        nil
                                                        (dom/small nil (:description swop-row))))

                                                      (condp = (keyword (:swop swop-row))
                                                        :swop-1
                                                        (let [{:keys [variants]} swop-row]
                                                          (apply
                                                           dom/td nil
                                                           (map
                                                            (fn [[n {id :id tip :tip :as v}]]
                                                              (dom/div
                                                               nil
                                                               (dom/label
                                                                nil
                                                                (str "за " n)
                                                                (om/build input-ax
                                                                          (get-in app [:list i :swops-set j :variants n :val]))
                                                                )))
                                                            (seq variants))
                                                           ))

                                                        (dom/td nil (dom/label nil (str "нет элементов ввода для " swop-row))))
                                                      ))
                                                   (range))))))))
             (range)))))))

;; END swops
;;..............................................................................

