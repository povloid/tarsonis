(ns tarsonis.product-oms
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [put! chan <!]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]

            ;;[sablono.core :as html :refer-macros [html]]

            [ixinfestor.net :as ixnet]
            [ixinfestor.omut :as omut]

            [tarsonis.tarsonis-oms :as toms]

            ))



;; Отображение товара в таблице

(defn product-td-view [app _ {:keys [onClick-fn]}]
  (reify
    om/IRender
    (render [_]
      (dom/td
       #js{:style #js {:whiteSpace "normal"}
           :onClick (fn [_]
                      (when onClick-fn (onClick-fn))
                      1)}
       ;;(:id row)
       (dom/h3 nil
               (dom/span #js {:className "label label-default"}
                         (dom/span #js {:className "glyphicon glyphicon-barcode" :aria-hidden "true"})
                         " " (:scod app)))
       (dom/h4 nil (:keyname app))
       (dom/p nil (:description app))
       (dom/p nil (str (:swops app)))
       (dom/p nil (str (:accas app)))
       
       ))))




;;------------------------------------------------------------------------------
;; BEGIN: Product add|edit form
;; tag: <product add edit form>
;; description: Форма редактирования товара
;;------------------------------------------------------------------------------

(def product-edit-form-app-init
  {:tabs (-> omut/nav-tabs-app-state
             (assoc :tabs [{:text "Общ."}
                           {:text "Цены"}
                           {:text "Теги"}
                           {:text "Веб"}
                           {:text "Фото"}
                           {:text "Файлы"}
                           ]))

   :product-id 0

   :common {:scod (assoc omut/input-app-init :has-warning? true)
            :keyname (assoc omut/input-app-init :has-warning? true)
            :description omut/textarea-app-init
            :is_product omut/toggle-button-app-init
            :select-ca toms/select-ca-app-init
            }
   :swaps  toms/swops-edit-view-app-init
   :tags   toms/webdoctag-select-view-app-init
   :web    {:web_meta_subject omut/textarea-app-init
            :web_meta_keywords omut/textarea-app-init
            :web_meta_description omut/textarea-app-init
            :web_top_description omut/textarea-app-init
            :web_title_image omut/input-app-init
            :web_citems omut/textarea-app-init
            :web_description omut/textarea-app-init
            }
   :images omut/thumbnails-view-app-init
   :files  omut/files-view-app-init})

(defn product-edit-form [app owner {:keys [chan-id chan-save]}]

  (reify
    om/IInitState
    (init-state [_]
      {:chan-update (chan)
       :chan-save (or chan-save (chan))
       :chan-validate-select-ca (chan)
       :chan-webdoctag-select-view-update (chan)
       :chan-swops-edit-view-update (chan)
       :chan-images-update (chan)
       :chan-files-update (chan)})
    om/IWillMount
    (will-mount [this]
      (let [{:keys [chan-update
                    chan-validate-select-ca
                    chan-webdoctag-select-view-update
                    chan-swops-edit-view-update
                    chan-images-update
                    chan-files-update]} (om/get-state owner)]

        (go ;; Обработчик для подсветки валидатора выбора меры
          (loop []
            (let [_ (<! chan-validate-select-ca)
                  {{{is_product? :value} :is_product {ca_id :ca_id} :select-ca} :common} @app]
              (if (and is_product? (nil? ca_id))
                (om/transact! app [:common :select-ca]
                              #(assoc % :has-warning? true :text-warning "Укажите меру"))
                (om/transact! app [:common :select-ca]
                              #(dissoc % :has-warning? :text-warning )) ))
            (recur)))

        (when chan-id
          (go ;; Обработчик для скачивания сведений по товару по запросу с сервера
            (loop []
              (let [id (<! chan-id)]
                (ixnet/get-data "/tc/rb/product/edit" {:id (if (= 0 id) nil id)}
                                (fn [{{:keys [id
                                              ;; common
                                              scod
                                              keyname
                                              description
                                              is_product
                                              ca_id
                                              tip
                                              val
                                              ;; web
                                              web_meta_subject
                                              web_meta_keywords
                                              web_meta_description
                                              web_top_description
                                              web_title_image
                                              web_citems
                                              web_description

                                              ]
                                       :or {keyname ""
                                            description ""
                                            is_product true}} :row
                                            {:keys [ca-list]} :rb
                                            :as response}]

                                  (om/transact! app
                                                (fn [app]
                                                  (-> app
                                                      (assoc :product-id id)

                                                      ;; Заполнение общей формы
                                                      (assoc-in [:common :scod :value] scod)
                                                      (assoc-in [:common :keyname :value] keyname)
                                                      (assoc-in [:common :description :value] description)
                                                      (assoc-in [:common :is_product :value] is_product)

                                                      (assoc-in [:common :select-ca :ca_id] ca_id)
                                                      (assoc-in [:common :select-ca :tip] tip)
                                                      (assoc-in [:common :select-ca :base-ax] val)
                                                      (assoc-in [:common :select-ca :list] ca-list)

                                                      ;; Заполнение веб формы
                                                      (assoc-in [:web :web_meta_subject :value] web_meta_subject )
                                                      (assoc-in [:web :web_meta_keywords :value] web_meta_keywords )
                                                      (assoc-in [:web :web_meta_description :value] web_meta_description )
                                                      (assoc-in [:web :web_top_description :value] web_top_description )
                                                      (assoc-in [:web :web_title_image :value] web_title_image )
                                                      (assoc-in [:web :web_citems :value] web_citems )
                                                      (assoc-in [:web :web_description :value] web_description )
                                                      )))

                                  (put! chan-update (get-in @app [:tabs :active-tab] 0)))))
              (recur))))

        (go ;; Обработка отрисовки табулятороров и переключения на страници
          (loop []
            (let [i (<! chan-update)
                  id (:product-id @app)]
              (println "Переключение на " i " :product-id = " id)

              (if (nil? id)
                (do
                  (om/update! app [:tabs :active-tab] 0)
                  (om/transact! app [:tabs :tabs]
                                (fn [tabs]
                                  (->> tabs
                                       (map
                                        (fn [i t]
                                          (if (= 0 i) t (assoc t :disabled? true)))
                                        (range))
                                       vec))))

                (do
                  (om/transact! app [:tabs :tabs]
                                (fn [tabs]
                                  (->> tabs
                                       (map
                                        (fn [t]
                                          (dissoc t :disabled?)))
                                       vec)))

                  ;; Обновление нужных страниц по запросу
                  (condp = i
                    1 (put! chan-swops-edit-view-update id)
                    2 (put! chan-webdoctag-select-view-update id)
                    4 (put! chan-images-update {:id id})
                    5 (put! chan-files-update {:id id})
                    nil)))

              (put! chan-validate-select-ca 1)

              (recur))))

        (put! chan-update 0)

        ;; Сохранение
        (let [chan-save (om/get-state owner :chan-save)]
          (go
            (while true
              (let [_ (<! chan-save)
                    active-tab (get-in @app [:tabs :active-tab])]
                (println "Сохранение")

                (condp = active-tab

                  0 (ixnet/get-data
                     "/tc/rb/product/save"
                     (-> {}
                         (#(if-let [id (@app :product-id)]
                             (assoc % :id id) %))

                         (assoc :keyname     (get-in @app [:common :keyname :value])
                                :description (get-in @app [:common :description :value])
                                :is_product  (get-in @app [:common :is_product :value]))

                         (as-> row
                             (if (row :is_product)
                               (assoc row
                                      :scod  (get-in @app [:common :scod :value])
                                      :ca_id (get-in @app [:common :select-ca :ca_id])
                                      :tip   (get-in @app [:common :select-ca :tip])
                                      :val   (get-in @app [:common :select-ca :base-ax]))
                               (dissoc row :ca_id)))
                         )
                     (fn [row]
                       (println "COMMON SAVED:"  row)
                       (put! chan-id (:id row)) )
                     omut/show-in-message-modal-danger)


                  1 (ixnet/get-data
                     "/tc/rb/product/swops/save"
                     {:product-id (@app :product-id)
                      :swaps (get-in @app [:swaps :list]) }
                     (fn [row]
                       (println "SWOPS SAVED:"  row)
                       (put! chan-update 2)))

                  2 (ixnet/get-data
                     "/tc/rb/product/tags-edit-table/save"
                     {:product-id (@app :product-id)
                      :webdoctag-edit-table (get-in @app [:tags :webdoctag-edit-table])}
                     (fn [row]
                       (println "TAGS SAVED:" row)
                       (put! chan-update 2)))

                  3 (ixnet/get-data
                     "/tc/rb/product/save"
                     (-> {:id (@app :product-id)

                          :web_meta_subject     (get-in @app [:web :web_meta_subject :value])
                          :web_meta_keywords    (get-in @app [:web :web_meta_keywords :value])
                          :web_meta_description (get-in @app [:web :web_meta_description :value])
                          :web_top_description  (get-in @app [:web :web_top_description :value])
                          :web_title_image      (get-in @app [:web :web_title_image :value])
                          :web_citems           (get-in @app [:web :web_citems :value])
                          :web_description (get-in @app [:web :web_description :value])

                          })
                     (fn [row]
                       (println "WEB SAVED:"  row)
                       (put! chan-id (:id row)) ))


                  (println "Действие сохранения для " active-tab " еще не определено."))))))
        ))
    om/IWillUpdate
    (will-update [_ s _]
      )
    om/IRenderState
    (render-state [_ {:keys [chan-update
                             chan-save
                             chan-validate-select-ca
                             chan-webdoctag-select-view-update
                             chan-swops-edit-view-update
                             chan-images-update
                             chan-files-update]}]
      (dom/div #js {:className "row"}
               (om/build omut/nav-tabs (:tabs app) {:opts {:chan-update chan-update
                                                           :type :tabs
                                                           :justified? true}})

               (condp = (get-in app [:tabs :active-tab])

                 ;; Основная форма
                 0 (dom/form
                    #js {:className "form-horizontal col-sm-12 col-md-12 col-lg-12"}
                    (dom/fieldset
                     nil
                     (dom/legend nil "Основные данные документа")

                     (om/build omut/input-form-group
                               (get-in app [:common :scod])
                               {:opts {:label "Код товара"
                                       :spec-input {:onChange-valid?-fn
                                                    omut/input-vldfn-not-empty}}})

                     (om/build omut/input-form-group
                               (get-in app [:common :keyname])
                               {:opts {:label "Наименование"
                                       :spec-input {:onChange-valid?-fn
                                                    omut/input-vldfn-not-empty}}})

                     (om/build omut/textarea-form-group
                               (get-in app [:common :description])
                               {:opts {:label "Наименование"}})

                     (om/build omut/toggle-button-form-group (get-in app [:common :is_product])
                               {:opts {:label "Это товар"
                                       :spec-toggle-button
                                       {:onClick-fn #(put! chan-validate-select-ca 1)}}})

                     (dom/div #js {:className
                                   (str "form-group "
                                        (omut/input-css-string-has? (get-in app [:common :select-ca])))}
                              (dom/label #js {:className "control-label col-sm-4 col-md-4 col-lg-4"}
                                         "Единица измерения")
                              (dom/div #js {:className "col-sm-8 col-md-8 col-lg-8"}
                                       (om/build toms/select-ca (get-in app [:common :select-ca])
                                                 {:opts {:onChange-fn #(put! chan-validate-select-ca 1)}})))

                     (dom/button #js {:className "btn btn-primary"
                                      :type "button"
                                      :onClick (fn [_]
                                                 (put! chan-save 1)
                                                 1)
                                      :data-dismiss "modal"}
                                 "Сохранить")

                     ))

                 ;; Страница редактирования цен
                 1 (dom/div nil
                            (dom/button #js {:className "btn btn-primary"
                                             :type "button"
                                             :onClick (fn [_]
                                                        (put! chan-save 1)
                                                        1)
                                             :data-dismiss "modal"}
                                        "Сохранить")

                            (om/build toms/swops-edit-view (app :swaps)
                                      {:opts{:chan-update chan-swops-edit-view-update}}))

                 ;; Страница тегов
                 2 (dom/div nil
                            (dom/button #js {:className "btn btn-primary"
                                             :type "button"
                                             :onClick (fn [_]
                                                        (put! chan-save 1)
                                                        1)
                                             :data-dismiss "modal"}
                                        "Сохранить")
                            (om/build toms/webdoctag-select-view (app :tags)
                                      {:opts{:chan-update chan-webdoctag-select-view-update}}))

                 ;; Веб
                 3 (dom/form
                    #js {:className "form-horizontal col-sm-12 col-md-12 col-lg-12"}
                    (dom/fieldset
                     nil
                     (dom/legend nil "Данные для сайта")

                     (om/build omut/textarea-form-group
                               (get-in app [:web :web_meta_subject])
                               {:opts {:label "Метатег subject"
                                       :spec-textarea {:rows 2}}})

                     (om/build omut/textarea-form-group
                               (get-in app [:web :web_meta_keywords])
                               {:opts {:label "Метатег keywords"
                                       :spec-textarea {:rows 2}}})

                     (om/build omut/textarea-form-group
                               (get-in app [:web :web_meta_description])
                               {:opts {:label "Метатег description"
                                       :spec-textarea {:rows 2}}})

                     (om/build omut/textarea-form-group
                               (get-in app [:web :web_top_description])
                               {:opts {:label "Краткое описание"
                                       :spec-textarea {:rows 4}}})

                     (om/build omut/input-form-group
                               (get-in app [:web :web_title_image])
                               {:opts {:label "Ссылка на аватар"}})

                     (om/build omut/textarea-form-group
                               (get-in app [:web :web_citems])
                               {:opts {:label "Метки заглавия"
                                       :spec-textarea {:rows 4}}})

                     (om/build omut/textarea-form-group
                               (get-in app [:web :web_description])
                               {:opts {:label "Полное описание описание"
                                       :spec-textarea {:rows 10}}})


                     (dom/button #js {:className "btn btn-primary"
                                      :type "button"
                                      :onClick (fn [_]
                                                 (put! chan-save 1)
                                                 1)
                                      :data-dismiss "modal"}
                                 "Сохранить")
                     
                     ))

                 ;; Картинки
                 4 (om/build omut/thumbnails-view (:images app)
                             {:opts {:uri "/tc/rb/product/images-list"
                                     :uri-upload "/tc/rb/product/upload/"
                                     :uri-delete "/tc/rb/product/files_rel/delete"
                                     :chan-update chan-images-update
                                     }})

                 ;; Файлы
                 5 (om/build omut/files-view (:files app)
                             {:opts {:uri "/tc/rb/product/files-list"
                                     :uri-upload "/tc/rb/product/upload/"
                                     :uri-delete "/tc/rb/product/files_rel/delete"
                                     :chan-update chan-files-update
                                     }})



                 (dom/h1 nil (str "Панель под индексом "
                                  (get-in app [:tabs :active-tab])
                                  " не определена")))

               ))))

;; END product add|edit form
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: Product view panel
;; tag: <product panel search view>
;; description: Панель поиска продуктов в таблицей
;;------------------------------------------------------------------------------

(def product-search-view-app-init
  (merge omut/search-view-app-init
         {:product-tips nil
          :modal-add (assoc omut/modal-app-init
                            :product-edit-form product-edit-form-app-init)
          :modal-act omut/actions-modal-app-init
          :modal-yes-no (assoc omut/modal-yes-no-app-init :row {})
          }))

(defn product-search-view [app owner {:keys [selection-type editable?] :or {selection-type :one}}]
  (reify
    om/IInitState
    (init-state [_]
      {:chan-modal-add-id (chan)
       :chan-modal-act (chan)
       :chan-update (chan)})
    om/IRenderState
    (render-state [_ {:keys [chan-modal-add-id
                             chan-modal-act
                             chan-update]}]
      (dom/div
       nil
       (om/build omut/search-view app
                 {:opts
                  {:chan-update chan-update
                   :data-update-fn
                   (fn [app]
                     (ixnet/get-data "/tc/rb/product/search"
                                     {:fts-query (get-in @app [:fts-query :value])
                                      :page (get-in @app [:page])
                                      :product-tips (get-in @app [:product-tips])}
                                     (fn [response]
                                       (om/update! app :data (vec response)))))
                   :data-rendering-fn
                   (fn [app-2]
                     (omut/ui-table {:hover? true
                                     :bordered? true
                                     :striped? true
                                     :responsive? true
                                     :thead (omut/ui-thead-tr [
                                                               (dom/th nil "Товар")
                                                               ;; (dom/th nil "№")
                                                               ;; (dom/th nil "Код")
                                                               ;; (dom/th nil "Наименование")
                                                               ])
                                     :tbody
                                     (om/build omut/tbody-trs-sel (:data app-2)
                                               {:opts {:selection-type selection-type
                                                       :app-to-tds-seq-fn
                                                       (fn [
                                                            row
                                                            ;;{:keys [id scod keyname]}
                                                            ]
                                                         [
                                                          (om/build product-td-view row)
                                                          ;; (dom/td nil id)
                                                          ;; (dom/td nil scod)
                                                          ;; (dom/td #js{:style #js {:whiteSpace "normal"}}
                                                          ;;         keyname)
                                                          ])
                                                       :on-select-fn
                                                       (fn [{:keys [id] :as row}]
                                                         (println row)
                                                         (when editable?
                                                           (put! chan-modal-act
                                                                 {:label (str "Выбор действий над записью №" id)
                                                                  :acts
                                                                  [{:text "Редактировать" :btn-type :primary
                                                                    :act-fn (fn []
                                                                              (put! chan-modal-add-id id)
                                                                              (omut/modal-show (:modal-add app)))}
                                                                   {:text "Удалить" :btn-type :danger
                                                                    :act-fn #(do
                                                                               (om/update! app [:modal-yes-no :row] row)
                                                                               (omut/modal-show (:modal-yes-no app)))}]
                                                                  }))
                                                         )
                                                       }})

                                     }))
                   :add-button-fn
                   #(do (omut/modal-show (:modal-add app))
                        (put! chan-modal-add-id 0))

                   }})

       (when editable?
         (om/build omut/actions-modal (:modal-act app) {:opts {:chan-open chan-modal-act}}))

       (om/build omut/modal (:modal-add app )
                 {:opts {:label "Редактировать товар"
                         :body (om/build product-edit-form
                                         (-> app :modal-add :product-edit-form)
                                         {:opts {:chan-id chan-modal-add-id}})
                         :footer
                         (dom/div nil
                                  (dom/button #js {:className "btn btn-default"
                                                   :type "button"
                                                   :onClick (fn [_]
                                                              (put! chan-update 1)
                                                              (omut/modal-hide (:modal-add app ))
                                                              1)
                                                   :data-dismiss "modal"}
                                              "Закрыть")
                                  )
                         }})

       (when editable?
         (om/build omut/modal-yes-no (:modal-yes-no app)
                   {:opts {:modal-size :sm
                           :label "Желаете удалить запись?"
                           :body
                           (dom/div
                            #js{:className "row"}
                            (dom/h3 #js{:className "col-xs-12 col-sm-12 col-md-12 col-lg-12"}
                                    (get-in @app [:modal-yes-no :row :keyname])))
                           :act-yes-fn
                           (fn []
                             (ixnet/get-data
                              "/tc/rb/product/delete"
                              {:id (get-in @app [:modal-yes-no :row :id])}
                              (fn [_]
                                (when chan-update
                                  (put! chan-update 1)))))
                           }}))

       ))))



;; Product view panel
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: Product search-and-add input
;; tag: <product search add input>
;; description: Элемент поиска товра по штрихкоду + поиск при помощи диалога с
;; с панелью поиска товара
;;------------------------------------------------------------------------------

(def product-search-add-app-init
  {:scod-query omut/input-app-init
   :modal (assoc omut/modal-app-init
                 :product-search-view product-search-view-app-init

                 ;; !!!
                 :modal-2 omut/modal-app-init

                 )
   :product-for-scod nil
   })

(defn product-search-add [app owner {:keys [chan-ext
                                            class+]
                                     :or {class+ ""}}]
  (reify
    om/IInitState
    (init-state [_]
      {:chan-update (chan)})
    om/IWillMount
    (will-mount [this]
      (let [chan-update (om/get-state owner :chan-update)]
        (go (loop []
              (let [_ (<! chan-update)]
                (ixnet/get-data "/tc/rb/product/search/scod"
                                {:scod (get-in @app [:scod-query :value])
                                 :page (get-in @app [:page])}
                                (fn [response]
                                  (om/update! app :product-for-scod response)
                                  (when chan-ext (put! chan-ext response))
                                  (om/update! app [:scod-query :value] "")
                                  ))
                (recur))))))
    om/IRenderState
    (render-state [_ {:keys [chan-update data]}]
      (dom/div #js {:className (str "form-group " class+ " " (omut/input-css-string-has? app))}
               (dom/label #js {:className "control-label "}
                          (dom/span #js {:className "glyphicon glyphicon-barcode" :aria-hidden "true"})
                          " Добавить товар по штрихкоду")

               (dom/div #js {:className "input-group"}
                        (dom/span #js {:className "input-group-btn"}
                                  (dom/button #js {:className "btn btn-default" :type "button"
                                                   :onClick (fn [_]
                                                              (om/update! app [:scod-query :value] "")
                                                              1)}
                                              (dom/span #js {:className "glyphicon glyphicon-remove"
                                                             :aria-hidden "true"})))

                        (om/build omut/input (:scod-query app)
                                  {:opts {:placeholder "Введите штрихкод товара"
                                          :onKeyPress-fn #(do #_(println
                                                                 (.-type %)
                                                                 (.-which %)
                                                                 (.-timeStamp %))

                                                              (when (= 13 (.-which %))
                                                                (put! chan-update 1)
                                                                )
                                                              1)
                                          }})

                        (dom/span #js {:className "input-group-btn"}
                                  (dom/button #js {:className "btn btn-default" :type "button"
                                                   :onClick (fn [_]
                                                              (put! chan-update 1)
                                                              1)}
                                              (dom/span #js {:className "glyphicon glyphicon-search"
                                                             :aria-hidden "true"}))

                                  (dom/button #js {:className "btn btn-default" :type "button"
                                                   :onClick (fn [_]
                                                              (omut/modal-show (:modal app))
                                                              1)}
                                              (dom/span #js {:className "glyphicon glyphicon-list-alt"
                                                             :aria-hidden "true"}))
                                  )
                        )

               (om/build omut/helper-p app)

               (om/build omut/modal (:modal app)
                         {:opts {:body
                                 (om/build product-search-view (get-in app [:modal :product-search-view]))
                                 :footer
                                 (dom/div #js {:className "btn-toolbar  pull-right"}
                                          (dom/button #js {:className "btn btn-primary"
                                                           :onClick (fn [_]
                                                                      (->> app
                                                                           :modal
                                                                           :product-search-view
                                                                           :data
                                                                           (filter omut/omut-row-selected?)
                                                                           (map #(select-keys % [:id :scod]))
                                                                           first
                                                                           :scod
                                                                           (om/update! app [:scod-query :value]))
                                                                      (put! chan-update 1)
                                                                      (omut/modal-hide (:modal app))
                                                                      1)
                                                           :type "button"}
                                                      "Выбрать")

                                          (dom/button #js {:className "btn btn-default"
                                                           :onClick (fn [_] (omut/modal-hide (:modal app)) 1)
                                                           :type "button"}
                                                      "Закрыть")
                                          )
                                 }})
               ))))

;; END Product search-and-add input
;;..............................................................................
;; END product
;;..................................................................................................
