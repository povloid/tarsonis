(ns tarsonis.op-move-as-form
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [clojure.data :as data]
            [clojure.string :as string]
           
            [ixinfestor.omut :as omut]
            [tarsonis.tarsonis-oms :as toms]
            [tarsonis.own-oms :as own]

            [goog.net.EventType]
            [goog.events :as events])
  )


(enable-console-print!)


(defn app-init [meta-opt]
  (->> meta-opt
       seq
       (filter (comp (partial contains? #{:money+ :money-> :money-}) :opttype second))
       (reduce
        (fn [a [k opt]]
          (assoc a k (-> {:opt opt

                          :owns {:search-from own/own-search-add-app-init
                                 :search-to   own/own-search-add-app-init
                                 }

                          :modal-clear?-yes-no omut/modal-yes-no-app-init
                          :modal-enter?-yes-no omut/modal-yes-no-app-init
                          }

                         ;; Настройка компонентов в зависимости от метаданных
                         (assoc-in [:owns  :search-from :selected]
                                   (get-in opt [:owns :from :default]))
                         (assoc-in [:owns  :search-from :modal :own-search-view :arls]
                                   (get-in opt [:owns :from :alt-search-in]))


                         (assoc-in [:owns  :search-to :selected]
                                   (get-in opt [:owns :to :default]))
                         (assoc-in [:owns  :search-to :modal :own-search-view :arls]
                                   (get-in opt [:owns :to :alt-search-in]))

                         )))
        {})))







