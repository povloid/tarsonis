(ns tarsonis.op-move
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [clojure.data :as data]
            [clojure.string :as string]


            ;;[sablono.core :as html :refer-macros [html]]
            [ixinfestor.omut :as omut]
            [tarsonis.tarsonis-oms :as toms]
            [tarsonis.own-oms :as own]
            [tarsonis.product-oms :as product]

            [ixinfestor.net :as ixnet]


            [goog.net.EventType]
            [goog.events :as events])
  )



(enable-console-print!)

;;(def panel-main-height (->  js/window .-innerHeight (- 50)))



(defn app-init [meta-opt]
  (->> meta-opt
       seq
       (filter (comp (partial contains? #{:prod+ :prod-> :prod-}) :opttype second))
       (reduce
        (fn [a [k opt]]
          (assoc a k (-> {:opt opt

                          :product-search-add product/product-search-add-app-init
                          :product-list {:data []
                                         :selected nil}
                          :modal-select-row omut/modal-app-init

                          :owns {:search-from own/own-search-add-app-init
                                 :search-to   own/own-search-add-app-init}

                          :modal-clear?-yes-no omut/modal-yes-no-app-init
                          :modal-enter?-yes-no omut/modal-yes-no-app-init
                          }

                         ;; Настройка компонентов в зависимости от метаданных
                         (assoc-in [:owns  :search-from :selected]
                                   (get-in opt [:owns :from :default]))
                         (assoc-in [:owns  :search-from :modal :own-search-view :arls]
                                   (get-in opt [:owns :from :alt-search-in]))


                         (assoc-in [:owns  :search-to :selected]
                                   (get-in opt [:owns :to :default]))
                         (assoc-in [:owns  :search-to :modal :own-search-view :arls]
                                   (get-in opt [:owns :to :alt-search-in]))

                         (assoc-in [:product-search-add :modal :product-search-view :product-tips]
                                   (get-in opt [:products :search-for-tips]))

                         )))
        {})))


(defn panel [app owner {:keys [opt]}]
  (let [cursor-app-opt (app opt)

        cursor-product-search-add (cursor-app-opt :product-search-add)

        cursor-own-search         (cursor-app-opt :owns )
        cursor-own-search-from    (cursor-own-search  :search-from )
        cursor-own-search-to      (cursor-own-search  :search-to )

        cursor-product-list       (cursor-app-opt :product-list)
        cursor-product-list-data  (cursor-product-list :data)

        cursor-modal-select-row   (cursor-app-opt :modal-select-row)

        cursor-modal-clear?-yes-no  (cursor-app-opt :modal-clear?-yes-no)
        cursor-modal-enter?-yes-no  (cursor-app-opt :modal-enter?-yes-no)
        ]


    (letfn [;; Отчистка списка товаров
            (clear []
              (om/update! cursor-product-list-data []))

            ;; Функция проведения операции
            (enter []
              (println "OPERATIO START")
              (let [[from-zone-key to-zone-key] (get-in @cursor-app-opt [:opt :products :direction])
                    from-own-id (get-in @cursor-own-search-from [:selected :id])
                    to-own-id   (get-in @cursor-own-search-to   [:selected :id])
                    ]

                (println from-zone-key to-zone-key)
                
                (ixnet/get-data "/tc/opt/op/do-operatio"
                                {:operatio {:opt-key (get-in @cursor-app-opt [:opt :opt])
                                            :items (map

                                                    (fn [{:keys [id caval] :as row}]
                                                      {:product-id id
                                                       :caval (map #(select-keys % [:id
                                                                                    :root_id
                                                                                    :parent_id
                                                                                    :tip
                                                                                    :keyname
                                                                                    :val]) caval)
                                                       :from-zone-key from-zone-key
                                                       :from-own-id from-own-id

                                                       :to-zone-key to-zone-key
                                                       :to-own-id to-own-id
                                                       }
                                                      )

                                                    @cursor-product-list-data)

                                            }
                                 }
                                
                                (fn [response]
                                  (om/update! cursor-product-list-data [])
                                  (omut/show-in-message-modal-success {:message "Операция проведена успешно"}))
                                
                                omut/show-in-message-modal-danger)))

            ]

      (reify
        om/IInitState
        (init-state [_]
          {:chan-add-product (chan)
           :chan-do-product-row (chan)})
        om/IWillMount
        (will-mount [this]
          (let [chan-add-product (om/get-state owner :chan-add-product)
                chan-do-product-row (om/get-state owner :chan-do-product-row)]
            (go
              (while true
                (let [new-product-row (<! chan-add-product)]
                  (om/transact!
                   cursor-product-list-data
                   (fn [product-list]
                     (let [id (:id new-product-row)
                           new-product-row (if-let [old-product-row (->> product-list
                                                                         (filter #(= id (:id %)))
                                                                         first)]
                                             old-product-row new-product-row)
                           product-list (filter #(not (= id (:id %))) product-list)]
                       (-> product-list
                           (conj new-product-row)
                           vec)))))))
            (go
              (while true
                (let [product-row (<! chan-do-product-row)]
                  (om/update! cursor-product-list :selected product-row)
                  (omut/modal-show cursor-modal-select-row)
                  )))
            ))
        om/IRenderState
        (render-state [_ {:keys [chan-add-product chan-do-product-row]}]
          (dom/div #js {:className "container-fluid"}
                   (dom/div #js {:id "panel-main"
                                 :className "row"
                                 :style #js {:backgroundColor "#eee"
                                             ;;:height panel-main-height
                                             }
                                 }


                            (dom/div #js {:id "panel-top row"
                                          :className "col-md-12"
                                          :style #js {:backgroundColor "white"
                                                      ;;:height panel-top-height
                                                      :overflowX "hidden;"
                                                      :overflowY "auto;"
                                                      }
                                          }


                                     (dom/div ;; Кнопки операций
                                      #js {:className "col-xs-12 col-sm-2"
                                           :style #js {:marginTop 10}}

                                      (omut/ui-button {:text "Сбросить"
                                                       :lg? true :block? true
                                                       :on-click #(omut/modal-show cursor-modal-clear?-yes-no)
                                                       :disabled? (empty? @cursor-product-list-data)
                                                       })

                                      (omut/ui-button {:text "Провести"
                                                       :type :primary
                                                       :lg? true :block? true
                                                       :on-click #(omut/modal-show cursor-modal-enter?-yes-no)
                                                       :disabled? (empty? @cursor-product-list-data)
                                                       })

                                      )

                                     ;; Форма
                                     (dom/div
                                      #js {:className "col-xs-12 col-sm-10"}

                                      (dom/div
                                       #js {:className "row"}

                                       (om/build own/own-search-add cursor-own-search-from
                                                 {:opts {:label-text "Уйдет от"
                                                         :class+ "col-xs-12 col-sm-6"}})
                                       (om/build own/own-search-add cursor-own-search-to
                                                 {:opts {:label-text "Прийдет к"
                                                         :class+ "col-xs-12 col-sm-6"}}) )


                                      (dom/div
                                       #js {:className "row"}
                                       (om/build product/product-search-add cursor-product-search-add
                                                 {:opts {:chan-ext chan-add-product
                                                         :class+ "col-xs-12 col-sm-12"}}) )

                                      )


                                     )


                            ;; Диалог
                            (om/build omut/modal-yes-no cursor-modal-clear?-yes-no
                                      {:opts {:modal-size :sm
                                              :label "Желаете очистить список и начать заново?"
                                              :body
                                              (dom/p nil
                                                     "Список товаров будет сброшен.")
                                              :act-yes-fn clear
                                              }})
                            ;; Диалог
                            (om/build omut/modal-yes-no cursor-modal-enter?-yes-no
                                      {:opts {:modal-size :sm
                                              :label "Провести операцию?"
                                              :body
                                              (dom/p nil
                                                     "После нажатия кнопки OK операция будет проведена.")
                                              :act-yes-fn enter
                                              }})




                            (dom/div #js {:id "panel-list"
                                          :className "col-md-12"
                                          :style #js {:backgroundColor "white"
                                                      :overflowX "hidden;"
                                                      :overflowY "auto;"
                                                      }
                                          }

                                     (omut/ui-table {:hover? true
                                                     :bordered? true
                                                     :striped? true
                                                     :responsive? true
                                                     :responsive-class+ ""
                                                     ;; :thead (dom/thead nil
                                                     ;;                   (dom/tr nil
                                                     ;;                           (dom/th nil "111")))
                                                     :thead (omut/ui-thead-tr [(dom/th nil "Товар")
                                                                               (dom/th nil "Количество")
                                                                               ])
                                                     :tbody (apply dom/tbody nil
                                                                   (for [row cursor-product-list-data]
                                                                     (dom/tr nil
                                                                             (om/build product/product-td-view row
                                                                                       {:opts {:onClick-fn
                                                                                               #(put! chan-do-product-row row)}})
                                                                             ;;(dom/td nil (om/build cm/qpanel-caval-info (:caval row)))
                                                                             (dom/td nil (om/build toms/input-caval (:caval row)))
                                                                             )))
                                                     })

                                     )

                            (om/build omut/modal cursor-modal-select-row
                                      {:opts {:class+ "modal-sm"
                                              :header (dom/h4 nil (get-in cursor-product-list [:selected :keyname]));; [:seleted :keyname] "Запись не выбрана"))
                                              :body
                                              (dom/div nil
                                                       (dom/button #js {:className "btn btn-danger btn-lg btn-block" :type "button"
                                                                        :onClick (fn [_]
                                                                                   (let [id (get-in cursor-product-list [:selected :id])]
                                                                                     (om/transact! cursor-product-list-data
                                                                                                   (comp vec
                                                                                                         (partial filter #(-> % :id (= id) not))))
                                                                                     (omut/modal-hide cursor-modal-select-row)
                                                                                     1))}
                                                                   (dom/span #js {:className "glyphicon glyphicon-remove"
                                                                                  :aria-hidden "true"})
                                                                   "Удалить из списка")

                                                       )
                                              ;;:footer nil
                                              }})



                            )))))))





#_(println "1111>>>>"
           (cm/ax-calc-symbolic-expression (list :ax+ [:bigint 1] (list :ax+ [:bigint 4] [:bigint 4]))))








;; #_(defn main []
;;     (println "GO!!!")
;;     (om/root
;;      (fn [app _]
;;        (reify
;;          om/IRender
;;          (render [_]
;;            (dom/div #js {:className "container-fluid"}
;;                     (dom/div #js {:className "row"}
;;                              (dom/button #js {:className "btn btn-primary"
;;                                               :onClick (fn [_]
;;                                                          (cm/modal-show app :as111))
;;                                               :type "button"}
;;                                          "Открыть")

;;                              (om/build cm/modal app {:opts {:id :as111
;;                                                             :body (om/build cm/input (app :k) {:opts {}})
;;                                                             :footer (dom/button #js {:className "btn btn-primary"
;;                                                                                      :onClick (fn [_]
;;                                                                                                 (cm/modal-hide app :as111))
;;                                                                                      :type "button"}
;;                                                                                 "Закрыть")

;;                                                             }})

;;                              #_(om/build cm/input-0 app)
;;                              (om/build cm/input (app :k) {:opts {}})
;;                              (om/build cm/input (app :k)
;;                                        {:opts {:label "Поле ввода строки"
;;                                                :onChange-valid?-fn
;;                                                (fn [data value]
;;                                                  (cm/helper-p-clear data)
;;                                                  (cm/input-css-string-has?-clear data)
;;                                                  (condp = (count value)
;;                                                    0 (om/transact! data (fn [m]
;;                                                                           (assoc m
;;                                                                                  :text-danger "Пустая строка"
;;                                                                                  :has-error? true)))
;;                                                    1 (om/update! data :text-warning "строка 1")
;;                                                    2 (om/update! data :text-info "строка 2")

;;                                                    true))

;;                                                }})


;;                              (cm/ui-table {:hover? true
;;                                            :bordered? true
;;                                            :striped? true
;;                                            :responsive? true
;;                                            :responsive-class+ "col-md-5"
;;                                            ;; :thead (dom/thead nil
;;                                            ;;                   (dom/tr nil
;;                                            ;;                           (dom/th nil "111")))
;;                                            :thead (cm/ui-thead-tr [(dom/th nil "111")
;;                                                                    (dom/th nil "111")])
;;                                            :tbody (apply dom/tbody nil
;;                                                          (repeat 100
;;                                                                  (dom/tr nil
;;                                                                          (dom/td nil 1)
;;                                                                          (dom/td nil 2))))
;;                                            })
;;                              ;;(doall (map #(dom/h1 nil %) (range 100)))

;;                              )))))
;;      app-state {:target (. js/document (getElementById "main"))}))



;;(-> js/window (.addEventListener "keyup" println false))




;; (dom/div #js {:className "btn-toolbar"}
;;          (dom/button #js {:className "btn btn-primary btn-lg"
;;                           :onClick (fn [_]
;;                                      (println 1))
;;                           :type "button"}
;;                      "Новая операция")

;;          (dom/button #js {:className "btn btn-primary btn-lg"
;;                           :onClick (fn [_]
;;                                      (println 1))
;;                           :type "button"}
;;                      "Новая операция")

;;          (dom/button #js {:className "btn btn-primary btn-lg"
;;                           :onClick (fn [_]
;;                                      (println 1))
;;                           :type "button"}
;;                      "Новая операция")

;;          )


;; #_(om/build cm/search-view app {:opts
;;                                 {:data-update-fn
;;                                  #(ixnet/get-data "/tc/op/rest/product/search"
;;                                                   {:fts-query (-> @app :fts-query :value)
;;                                                    :page (@app :page)}
;;                                                   (fn [response]
;;                                                     (om/update! app :data (vec response))))
;;                                  :data-rendering-fn
;;                                  #(cm/ui-table {:hover? true
;;                                                 :bordered? true
;;                                                 :striped? true
;;                                                 :responsive? true
;;                                                 :thead (cm/ui-thead-tr [(dom/th nil "№")
;;                                                                         (dom/th nil "Наименование")
;;                                                                         (dom/th nil "Описание")])
;;                                                 :tbody
;;                                                 #_(apply dom/tbody nil
;;                                                          #_(for [r (:data app)]
;;                                                              (dom/tr nil
;;                                                                      (dom/td nil (r :id))
;;                                                                      (dom/td nil (r :keyname))))
;;                                                          (om/build-all cm/tr-sel (:data app))
;;                                                          )
;;                                                 (om/build cm/tbody-trs-sel (:data app)
;;                                                           {:opts {:selection-type :one}})
;;                                                 })
;;                                  }})
;;(om/build cm/product-search-view app)
