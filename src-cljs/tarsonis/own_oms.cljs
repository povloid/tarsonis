(ns tarsonis.own-oms
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [put! chan <!]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]

            ;;[sablono.core :as html :refer-macros [html]]

            [ixinfestor.net :as ixnet]
            [ixinfestor.omut :as omut]

            ))






;;**************************************************************************************************
;;* BEGIN Owners
;;* tag: <own owners>
;;*
;;* description: Работа с владельцами счетов
;;*
;;**************************************************************************************************


;;------------------------------------------------------------------------------
;; BEGIN: Owners edit form
;; tag: <own edit form>
;; description: Форма редактирования владельца счетов
;;------------------------------------------------------------------------------

(def own-edit-form-app-init
  (merge omut/edit-form-for-id-app-init
         {:keyname (assoc omut/input-app-init :has-warning? true)
          :description omut/textarea-app-init
          :arls []
          }))


(defn own-edit-form [app owner {:keys [arls]
                                :as opts}]
  (reify
    om/IRender
    (render [_]
      (println "edit only for arls:" arls)
      (om/build
       omut/edit-form-for-id app
       {:opts
        (merge opts
               {:uri "/tc/rb/own/edit"
                :fill-app-fn
                (fn [row] ;; !!! состав ответа непростой!!!
                  (om/transact!
                   app
                   (fn [app]
                     (-> app
                         ;; Заполнение формы
                         (assoc :id  (get-in row [:row :id])) ;; !!! cостав ответа непростой и ID в корне нет, поэтому берем его из [:row]
                         (assoc-in [:keyname :value]    (get-in row [:row :keyname] ""))
                         (assoc-in [:description :value] (get-in row [:row :description] ""))

                         (assoc :arls (vec
                                       (map
                                        (fn [r]
                                          (assoc r :inc? {:value (if (not (nil? arls))
                                                                   (contains? arls (r :keyname))
                                                                   (r :inc?))
                                                          }))
                                        (row :arls))))
                         ))))

                :uri-save "/tc/rb/own/save"
                :app-to-row-fn
                (fn []
                  {:row (-> (if-let [id (:id @app)] {:id id} {})
                            (assoc :keyname      (get-in @app [:keyname :value])
                                   :description  (get-in @app [:description :value])))
                   :arls (->> @app
                              :arls
                              (map #(assoc % :inc? (get-in % [:inc? :value]))))
                   })

                :form-body
                (dom/fieldset
                 nil
                 (dom/legend nil "Основные данные")

                 (om/build omut/input-form-group (get-in app [:keyname])
                           {:opts {:label "Наименование"
                                   :spec-input {:onChange-valid?-fn
                                                omut/input-vldfn-not-empty}}})

                 (om/build omut/textarea-form-group (get-in app [:description])
                           {:opts {:label "Описание"}})


                 (dom/div #js {:className "form-group"}
                          (dom/label #js {:className "control-label col-sm-4 col-md-4 col-lg-4"} "Учетные роли")
                          (apply dom/div #js {:className "col-sm-8 col-md-8 col-lg-8"}
                                 (map (fn [i {:keys [title]}]
                                        (dom/div nil
                                                 (om/build omut/toggle-button (get-in app [:arls i :inc?])
                                                           {:opts {:disabled? (not (nil? arls))
                                                                   }})
                                                 title))
                                      (range) (app :arls))))


                 )
                })
        }))))


(def own-modal-edit-form-app-init
  (merge omut/modal-edit-form-for-id--YN--app-init own-edit-form-app-init))

(defn own-modal-edit-form [app _ opts]
  (reify
    om/IRender
    (render [_]
      (om/build omut/modal-edit-form-for-id--YN- app
                {:opts (assoc opts :edit-form-for-id own-edit-form)}))))





;; END Owners edit form
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: Owners search form
;; tag: <own search view form>
;; description: Форма поиска владельцев счетов
;;------------------------------------------------------------------------------

(def own-search-view-app-init
  (merge
   omut/search-view-app-init
   {:modal-add own-modal-edit-form-app-init
    :modal-act omut/actions-modal-app-init
    :modal-yes-no (assoc omut/modal-yes-no-app-init :row {})
    }))

(defn own-search-view [app owner {:keys [selection-type
                                         editable?]
                                  :or {selection-type :one}}]
  (reify
    om/IInitState
    (init-state [_]
      {:chan-update (chan)
       :chan-modal-act (chan)
       :chan-modal-add-id (chan)})
    om/IRenderState
    (render-state [_ {:keys[chan-modal-act
                            chan-modal-add-id
                            chan-update]}]
      (dom/div nil
               (om/build omut/search-view app
                         {:opts
                          {:chan-update chan-update
                           :data-update-fn
                           (fn [app]
                             (ixnet/get-data "/tc/rb/own/search"
                                             {:fts-query (get-in @app [:fts-query :value])
                                              :page (get-in @app [:page])
                                              :arls (get-in @app [:arls] #{})}
                                             (fn [response]
                                               (om/update! app :data (vec response)))))
                           :data-rendering-fn
                           (fn [app-2]
                             (omut/ui-table {:hover? true
                                             :bordered? true
                                             :striped? true
                                             :responsive? true
                                             :thead (omut/ui-thead-tr [(dom/th nil "№")
                                                                       (dom/th nil "Наименование")
                                                                       (dom/th nil "Описание")])
                                             :tbody
                                             (om/build omut/tbody-trs-sel (:data app-2)
                                                       {:opts {:selection-type selection-type
                                                               :app-to-tds-seq-fn
                                                               (fn [{:keys [id keyname description]}]
                                                                 [(dom/td nil id)
                                                                  (dom/td nil keyname)
                                                                  (dom/td #js{:style #js {:whiteSpace "normal"}}
                                                                          description)])
                                                               :on-select-fn
                                                               (fn [{:keys [id] :as row}]
                                                                 (when editable?
                                                                   (put! chan-modal-act
                                                                         {:label (str "Выбор действий над записью №" id)
                                                                          :acts
                                                                          [{:text "Редактировать" :btn-type :primary
                                                                            :act-fn (fn []
                                                                                      (put! chan-modal-add-id id)
                                                                                      (omut/modal-show (:modal-add app)))}
                                                                           {:text "Удалить" :btn-type :danger
                                                                            :act-fn #(do
                                                                                       (om/update! app [:modal-yes-no :row] row)
                                                                                       (omut/modal-show (:modal-yes-no app)))}]
                                                                          }))
                                                                 )
                                                               }})
                                             }))
                           :add-button-fn
                           #(do (omut/modal-show (:modal-add app))
                                (put! chan-modal-add-id 0)
                                )
                           }})

               (when editable?
                 (om/build omut/actions-modal (:modal-act app) {:opts {:chan-open chan-modal-act}}))

               (om/build own-modal-edit-form (:modal-add app)
                         {:opts {
                                 :chan-load-for-id chan-modal-add-id
                                 :post-save-fn #(do
                                                  (when chan-update
                                                    (put! chan-update 1))
                                                  1)
                                 :arls (@app :arls)
                                 }})

               (when editable?
                 (om/build omut/modal-yes-no (:modal-yes-no app)
                           {:opts {:modal-size :sm
                                   :label "Желаете удалить запись?"
                                   :body
                                   (dom/div
                                    #js{:className "row"}
                                    (dom/h3 #js{:className "col-xs-12 col-sm-12 col-md-12 col-lg-12"}
                                            (get-in @app [:modal-yes-no :row :keyname])))
                                   :act-yes-fn
                                   (fn []
                                     (ixnet/get-data
                                      "/tc/rb/own/delete"
                                      {:id (get-in @app [:modal-yes-no :row :id])}
                                      (fn [_]
                                        (when chan-update
                                          (put! chan-update 1)))))
                                   }}))

               ))))

(def own-search-add-app-init
  {:modal (assoc omut/modal-app-init
                 :own-search-view own-search-view-app-init)
   :selected {}
   })

(defn own-search-add [app _ {:keys [label-text
                                    class+
                                    disabled?]
                             :or {class+ ""}}]
  (reify
    om/IRender
    (render [_]
      (let [arls? (not
                   (empty?
                    (get-in app [:modal :own-search-view :arls])))]
        (dom/div #js {:className (str "form-group " class+ " "(omut/input-css-string-has? app))}
                 (dom/label #js {:className "control-label "}
                            (dom/span #js {:className "glyphicon glyphicon-user" :aria-hidden "true"})
                            (str " " (or label-text "Владелец счета")))

                 (dom/div #js {:className "input-group"}

                          (dom/input #js {:value (get-in app [:selected :keyname])
                                          :placeholder "Выберите владельца счета"
                                          :className "form-control"})

                          (dom/span #js {:className "input-group-btn"}
                                    (dom/button #js {:className "btn btn-default" :type "button"
                                                     :disabled (if (and (not disabled?) arls?)
                                                                 "" "disabled")
                                                     :onClick (fn [_]
                                                                (omut/modal-show (:modal app))
                                                                1)}
                                                (dom/span #js {:className "glyphicon glyphicon-list-alt"
                                                               :aria-hidden "true"}))
                                    ))

                 (om/build omut/helper-p app)

                 (when (and (not disabled?) arls?)
                   (om/build omut/modal (:modal app)
                             {:opts {:body (om/build own-search-view (get-in app [:modal :own-search-view]))
                                     :footer (dom/div #js {:className "btn-toolbar  pull-right"}
                                                      (dom/button #js {:className "btn btn-primary"
                                                                       :onClick (fn [_]
                                                                                  (->> app
                                                                                       :modal
                                                                                       :own-search-view
                                                                                       :data
                                                                                       (filter omut/omut-row-selected?)
                                                                                       first
                                                                                       (om/update! app :selected))

                                                                                  (omut/modal-hide (:modal app))
                                                                                  1)
                                                                       :type "button"}
                                                                  "Выбрать")

                                                      (dom/button #js {:className "btn btn-default"
                                                                       :onClick (fn [_] (omut/modal-hide (:modal app)) 1)
                                                                       :type "button"}
                                                                  "Закрыть")
                                                      )
                                     }})
                   ))))))



(def input-form-search-view-app-init
  (omut/input-form-search-view-app-init own-search-view-app-init))

(def input-form-search-view-get-selected omut/input-form-search-view-get-selected)


(defn input-form-search-view [{:keys [label-one label-multi placeholder]
                               :or {label-one "Владелец счета"
                                    label-multi "Владельци счетов"
                                    placeholder "Выберите владельца счета"}}]
  (omut/input-from-search-view
   own-search-view
   {:label-one label-one
    :label-multi label-multi
    :placeholder placeholder
    ;;:ui-type--add-button--type :success
    :ui-type--add-button--text "Добавить..."
    :multiselect-row-render-fn (fn [app-row]
                                 (dom/td nil (str app-row)))
    }))





;; END Owners search form
;;..............................................................................


;; END Owners
;;..................................................................................................
