(ns tarsonis.core-test
  (:require [clojure.test :refer :all]
            [ixinfestor.core :as ix]
            [tarsonis.core :refer :all]))



(korma.db/defdb tarsonis (korma.db/postgres {:db "tarsonis"
                                             :user "tarsonis"
                                             :password "paradox"
                                             ;; optional keys
                                             :host "localhost"
                                             :port "5433"}))


(use-fixtures
  :once (fn [f]
          (println "Java version: "  (-> Runtime .getClass .getPackage .getImplementationVersion))
          (println "Установка типов")
          (install-tips)
          (f)))

(use-fixtures
  :each (fn [f]
          (print "Отчистка базы ->")

          (korma.core/delete swopva)
          (korma.core/delete swopv)

          (korma.core/delete opiacc)
          (korma.core/delete opi)
          (korma.core/delete op)
          (korma.core/delete opt)
          (korma.core/delete opttype)

          (korma.core/delete acc)

          (korma.core/delete ix/webdoctag)
          (korma.core/delete ix/tag)
          (korma.core/delete ix/files_rel)
          (korma.core/delete ix/files)

          (korma.core/delete webdoc)
          (korma.core/delete ca)
          ;;(korma.core/delete tip)

          (korma.core/delete ownarl)
          (korma.core/delete arl)
          (korma.core/delete own)

          (korma.core/delete ix/webuserwebrole)
          (korma.core/delete ix/webrole)
          (korma.core/delete ix/webuser)
          (korma.core/delete zone)
          (println " OK!")


          ;;(install-tips)

          (f)))


;;**************************************************************************************************
;;* BEGIN Test utils
;;* tag: <test utils>
;;*
;;* description: Утилиты для тестирования
;;*
;;**************************************************************************************************

(defmacro equal-vals [r field]
  `(= (~field (:row-1 ~r)) (~field (:row-2 ~r))))

(defn check-unique [fields result-list]
  (let [ss (reduce #(do ;;(println %2 "\n====>" (select-keys %2 fields))
                      (conj %1 (select-keys %2 fields))) #{} result-list)]
    (is (= (count result-list) (count ss))
        (str "Проверка уникальности " fields "\n=>" ss))))

(defn comm-test-save-and-delete [entity save-fn row-1 row-upd & [dissoc-fields]]
  (let [count-1 (ix/com-count entity)
        _ (println 1)
        row-2 (save-fn row-1)
        _ (println 2)
        {id-2 :id} row-2
        _ (println 3)
        count-2 (ix/com-count entity)
        _ (println 4)
        row-3 (save-fn (merge row-2 row-upd))
        _ (println 5 row-3)
        {id-3 :id} row-3
        _ (println 6)
        count-3 (ix/com-count entity)
        _ (println 7)
        _ (ix/com-delete-for-id entity id-3)
        _ (println 8)
        count-4 (ix/com-count entity)
        _ (println 9)]
    (testing "Плохое сохранение!"
      (is (not (nil? id-2)))
      (is (number? id-2))
      (if (nil? dissoc-fields)
        (is (= (dissoc row-2 :id) row-1))
        (is (= (apply dissoc (into [row-2] dissoc-fields)) row-1)))
      )
    (testing "Вставлено больше одного или невставлено!"
      (is (< 0 count-2))
      (is (< count-1 count-2))
      (is (= count-1 (dec count-2)))
      )
    (testing "Плохое обновление!"
      (is (= id-2 id-3))
      (is (= count-2 count-3))
      (is (not (= row-2 row-3)))
      (println ">>>" dissoc-fields)
      (if (nil? dissoc-fields)
        (is (= (dissoc row-3 :id) row-upd))
        (is (= (apply dissoc (into [row-3] dissoc-fields)) row-upd)))
      )
    (testing "Плохое удаление"
      (is (= count-1 count-4))
      )
    {
     :row-1 row-1
     :row-2 row-2
     :row-3 row-3
     :row-upd row-upd
     :count-1 count-1
     :count-2 count-2
     :count-3 count-3
     :count-4 count-4
     }
    ))

;; END Test utils
;;..................................................................................................


(declare t-init-webusers t-init-owns t-init-webdocs t-init-zones)

(defn t-init []
  (t-init-zones)
  (t-init-webusers)
  (t-init-owns)
  (t-init-webdocs))

;;**************************************************************************************************
;;* BEGIN Tus tests
;;* tag: <test webuser>
;;*
;;* description: Тестироване пользователей системы
;;*
;;**************************************************************************************************

(defn get-webusers
  "Создание тестового набора полтзователей"
  []
  (println "Создание тестовых пользователей системы")
  {
   :webuser-1 (ix/webuser-save {:username "тест-пользователь 1" :description "Тестовый пользователь"})
   :webuser-2 (ix/webuser-save {:username "тест-пользователь 2" :description "Тестовый пользователь"})
   :webuser-3 (ix/webuser-save {:username "тест-пользователь 3" :description "Тестовый пользователь"})
   })

;; REPL TESTS
(defn t-init-webusers []
  (def t-webuser-1 (:webuser-1 (get-webusers)))
  (def t-webuser-2 (:webuser-2 (get-webusers)))
  (def t-webuser-3 (:webuser-3 (get-webusers))))

(deftest test-webuser-logic
  (testing "Тестирование логики webuser"
    (let [_ (get-webusers)
          result-list (korma.core/select ix/webuser)]
      (check-unique [:id] result-list)
      (check-unique [:username] result-list)
      ))
  )

(deftest test-webuser-save
  (testing "Тестирование сохранения и удаления webuser"
    (comm-test-save-and-delete ix/webuser ix/webuser-save
                               {:username "Test webuser 1" :description "Test description 1" :password "123"}
                               {:username "Test webuser 2" :description "Test description 2" :password "123"})
    )
  )

;; END Owner tests
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN webrole tests and creating test objects
;;* tag: <test webrole>
;;*
;;* description: тестирование ролей пользователей
;;*
;;**************************************************************************************************

(defn get-webroles
  "Создание тестового набора ролей пользователей"
  []
  (println "Создание тестового набора ролей пользователя системы")
  {
   :webrole-1 (ix/webrole-init :webrole-1 "тест-роль пользователя 1" "Тестовая роль пользователя")
   :webrole-2 (ix/webrole-init :webrole-2 "тест-роль пользователя 2" "Тестовая роль пользователя")
   :webrole-3 (ix/webrole-init :webrole-3 "тест-роль пользователя 3" "Тестовая роль пользователя")
   })

;; REPL TESTS
(defn t-webrole-inits []
  (def t-webrole-1 (:webrole-1 (get-webroles)))
  (def t-webrole-2 (:webrole-2 (get-webroles)))
  (def t-webrole-3 (:webrole-3 (get-webroles))))


;; END webrole tests and creating test objects
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN test webuserwebrole
;;* tag: <test webuserwebrole>
;;*
;;* description: тестирование связей пользователей и ролей
;;*
;;**************************************************************************************************

(deftest test-webuserwebrole-webuser-has-a-role?
  (testing "тестирование пользовательских ролей"
    (let [t-webusers (get-webusers)
          t-webuser-1      (:webuser-1 t-webusers)
          t-webuser-2      (:webuser-2 t-webusers)
          t-webuser-3      (:webuser-3 t-webusers)

          t-webroles (get-webroles)
          t-webrole-1 (t-webroles :webrole-1)
          t-webrole-2 (t-webroles :webrole-2)
          t-webrole-3 (t-webroles :webrole-3)

          webuserwebrole-count (ix/com-count ix/webuserwebrole)]

      (ix/webuserwebrole-add-rel t-webuser-1 t-webrole-1)
      (is (= (+ 1 webuserwebrole-count) (ix/com-count ix/webuserwebrole)))
      (ix/webuserwebrole-add-rel t-webuser-1 t-webrole-1)
      (is (= (+ 1 webuserwebrole-count) (ix/com-count ix/webuserwebrole)))

      (ix/webuserwebrole-add-rel t-webuser-1 t-webrole-2)
      (is (= (+ 2 webuserwebrole-count) (ix/com-count ix/webuserwebrole)))

      (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-1)
      (is (= (+ 3 webuserwebrole-count) (ix/com-count ix/webuserwebrole)))
      (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-2)
      (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-2)
      (is (= (+ 4 webuserwebrole-count) (ix/com-count ix/webuserwebrole)))
      (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-3)
      (is (= (+ 5 webuserwebrole-count) (ix/com-count ix/webuserwebrole)))


      (is (true? (ix/webuserwebrole-webuser-has-a-role? t-webuser-1 :webrole-1)))
      (is (true? (ix/webuserwebrole-webuser-has-a-role? t-webuser-1 :webrole-2)))
      (is (false? (ix/webuserwebrole-webuser-has-a-role? t-webuser-1 :webrole-3)))

      (is (true? (ix/webuserwebrole-webuser-has-a-role? t-webuser-2 :webrole-1)))
      (is (true? (ix/webuserwebrole-webuser-has-a-role? t-webuser-2 :webrole-2)))
      (is (true? (ix/webuserwebrole-webuser-has-a-role? t-webuser-2 :webrole-3)))

      (is (false? (ix/webuserwebrole-webuser-has-a-role? t-webuser-3 :webrole-1)))
      (is (false? (ix/webuserwebrole-webuser-has-a-role? t-webuser-3 :webrole-2)))
      (is (false? (ix/webuserwebrole-webuser-has-a-role? t-webuser-3 :webrole-3)))
      )))

;; END test webuserwebrole
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Owner tests
;;* tag: <test own>
;;*
;;* description: Тестироване владельцев счетов
;;*
;;**************************************************************************************************


(defn get-owns
  "Создание тестового набора владельцев"
  []
  (println "Создание тестовых владельцев счетов")
  {
   :own-cash-1 (own-save {:keyname "тест-касса 1" :description "Тестовыя касса"})
   :own-cash-2 (own-save {:keyname "тест-касса 2" :description "Тестовыя касса"})
   :own-cash-3 (own-save {:keyname "тест-касса 3" :description "Тестовыя касса"})
   :own-warehouse-1 (own-save {:keyname "тест-склад 1" :description "Тестовый склад"})
   :own-warehouse-2 (own-save {:keyname "тест-склад 2" :description "Тестовый склад"})
   :own-chief (own-save {:keyname "тест-шеф" :description "Тестовый шеф"})
   :own-customer-1 (own-save {:keyname "тест-клиент1" :description "Тестовый клиент"})
   :own-customer-2 (own-save {:keyname "тест-клиент2" :description "Тестовый клиент"})
   })

;; REPL TESTS
(defn t-init-owns []
  (def t-own-cash-1 (:own-cash-1 (get-owns)))
  (def t-own-cash-2 (:own-cash-2 (get-owns)))
  (def t-own-cash-3 (:own-cash-3 (get-owns)))
  (def t-own-warehouse-1 (:own-warehouse-1 (get-owns)))
  (def t-own-warehouse-2 (:own-warehouse-2 (get-owns)))
  (def t-own-chief (:own-chief (get-owns)))
  (def t-own-customer-1 (:own-customer-1 (get-owns)))
  (def t-own-customer-2 (:own-customer-2 (get-owns))))


(deftest test-own-logic
  (testing "Тестирование логики own"
    (let [_ (get-owns)
          result-list (korma.core/select own)]
      (check-unique [:id] result-list)
      ))
  )


(deftest test-own-save
  (testing "Тестирование сохранения и удаления own"
    (comm-test-save-and-delete own own-save
                               {:keyname "Test own 1" :description "Test description 1"}
                               {:keyname "Test own 2" :description "Test description 2"})
    )
  )

;; END Owner tests
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN test arl
;;* tag: <test arl>
;;*
;;* description: тестирование ролей владельцев счетов
;;*
;;**************************************************************************************************

(defn get-arls
  "Создание тестового набора ролей владельцев"
  []
  (println "Создание тестовых ролей владельцев счетов")
  {
   :arl-1 (init-arl :arl-1 "тест-учетная-роль 1" "Тестовыя учетная роль")
   :arl-2 (init-arl :arl-2 "тест-учетная-роль 2" "Тестовыя учетная роль")
   :arl-3 (init-arl :arl-3 "тест-учетная-роль 3" "Тестовыя учетная роль")
   })

;; REPL TESTS
(defn t-init-arls []
  (def t-arl-1 (:arl-1 (get-arls)))
  (def t-arl-2 (:arl-2 (get-arls)))
  (def t-arl-3 (:arl-3 (get-arls))))

;; END test arl
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN test ownarl
;;* tag: <test ownarl>
;;*
;;* description: тестирование связей владельцев счетов с ролями
;;*
;;**************************************************************************************************

(deftest test-ownarl-own-has-a-role?
  (testing "тестирование счетныйх ролей "
    (let [t-owns (get-owns)
          t-own-cash-1      (:own-cash-1 t-owns)
          t-own-cash-2      (:own-cash-2 t-owns)
          t-own-cash-3      (:own-cash-3 t-owns)

          t-arls (get-arls)
          t-arl-1 (t-arls :arl-1)
          t-arl-2 (t-arls :arl-2)
          t-arl-3 (t-arls :arl-3)

          ownarl-count (ix/com-count ownarl)]

      (ownarl-add-rel t-own-cash-1 t-arl-1)
      (is (= (+ 1 ownarl-count) (ix/com-count ownarl)))
      (ownarl-add-rel t-own-cash-1 t-arl-1)
      (is (= (+ 1 ownarl-count) (ix/com-count ownarl)))

      (ownarl-add-rel t-own-cash-1 t-arl-2)
      (is (= (+ 2 ownarl-count) (ix/com-count ownarl)))


      (ownarl-add-rel t-own-cash-2 t-arl-1)
      (is (= (+ 3 ownarl-count) (ix/com-count ownarl)))
      (ownarl-add-rel t-own-cash-2 t-arl-2)
      (ownarl-add-rel t-own-cash-2 t-arl-2)
      (is (= (+ 4 ownarl-count) (ix/com-count ownarl)))
      (ownarl-add-rel t-own-cash-2 t-arl-3)
      (is (= (+ 5 ownarl-count) (ix/com-count ownarl)))


      (is (true? (ownarl-own-has-a-role? t-own-cash-1 :arl-1)))
      (is (true? (ownarl-own-has-a-role? t-own-cash-1 :arl-2)))
      (is (false? (ownarl-own-has-a-role? t-own-cash-1 :arl-3)))

      (is (true? (ownarl-own-has-a-role? t-own-cash-2 :arl-1)))
      (is (true? (ownarl-own-has-a-role? t-own-cash-2 :arl-2)))
      (is (true? (ownarl-own-has-a-role? t-own-cash-2 :arl-3)))

      (is (false? (ownarl-own-has-a-role? t-own-cash-3 :arl-1)))
      (is (false? (ownarl-own-has-a-role? t-own-cash-3 :arl-2)))
      (is (false? (ownarl-own-has-a-role? t-own-cash-3 :arl-3)))
      )))



;; END test ownarl
;;..................................................................................................




;;**************************************************************************************************
;;* BEGIN Zone test
;;* tag: <zone test>
;;*
;;* description: Тестовый блок для зон учета
;;*
;;**************************************************************************************************

(def zone-sufx-id (atom 0))

(defn get-zones
  "Создание тестового набора примитивных зонн"
  []
  (let [zf-id (swap! zone-sufx-id inc)]
    (println "Создание тестовых примитивных зонн")
    {
     :zone-1 (zone-save {:keyname (str "zone " zf-id)
                         :title (str "тест-зона " zf-id)
                         :description (str "Тестовыя зона " zf-id)})
     }))

;; REPL TESTS
(defn t-init-zones []
  (def t-zone-1 (:zone-1 (get-zones))))



;; END Zone test
;;..................................................................................................




;;**************************************************************************************************
;;* BEGIN Type tests
;;* tag: <test tip>
;;*
;;* description: Тестирование типов
;;*
;;**************************************************************************************************

(deftest test-tip-constructors-and-selectors
  (let [ax (ax-cr :bigint 10)]
    (testing "Тест конструктора"
      (is (thrown? Exception (ax-cr nil 1)))
      (is (thrown? Exception (ax-cr "key" 1)))
      (is (thrown? Exception (ax-cr :not-existed-tag 1)))
      (is (thrown? Exception (ax-cr 1 1)))
      ;; TODO: подумать над тестом исключения при отсутствии описания конструктора
      )
    (testing "Тест селекторов"
      (is (= :bigint (ax-tag ax)))
      (is (= 10 (ax-val ax)))
      )
    )
  )

(deftest test-constructors
  (testing "Тестирование конструктора bigint"
    (is (thrown? Exception (ax-cr :bigint nil)))
    (is (thrown? Exception (ax-cr :bigint [1 2 3])))
    (is (thrown? Exception (ax-cr :bigint :a)))
    (is (thrown? Exception (ax-cr :bigint "string")))
    (is (thrown? Exception (ax-cr :bigint {:a 1})))
    (is (thrown? Exception (ax-cr :bigint #{1 2})))
    (is (thrown? Exception (ax-cr :bigint 1.2)))
    (is (thrown? Exception (ax-cr :bigint 120.05M)))
    (is (thrown? Exception (ax-cr :bigint -12.05M)))
    (is (= :bigint (ax-tag (ax-cr :bigint 10))))
    (is (= 10 (ax-val (ax-cr :bigint 10))))
    (is (number? (ax-val (ax-cr :bigint 10))))
    (is (integer? (ax-val (ax-cr :bigint 100))))
    (is (integer? (ax-val (ax-cr :bigint 10N))))
    (is (integer? (ax-val (ax-cr :bigint 1000M))))
    )
  (testing "Тестирование конструктора money"
    (is (thrown? Exception (ax-cr :money nil)))
    (is (thrown? Exception (ax-cr :money [1 2 3])))
    (is (thrown? Exception (ax-cr :money :a)))
    (is (thrown? Exception (ax-cr :money "string")))
    (is (thrown? Exception (ax-cr :money {:a 1})))
    (is (thrown? Exception (ax-cr :money #{1 2})))
    (is (= :money (ax-tag (ax-cr :money 10))))
    (is (= 10M (ax-val (ax-cr :money 10))))
    (is (number? (ax-val (ax-cr :money 10))))
    (is (decimal? (ax-val (ax-cr :money 100))))
    (is (decimal? (ax-val (ax-cr :money 10N))))
    (is (decimal? (ax-val (ax-cr :money 1000M))))
    (is (decimal? (ax-val (ax-cr :money 10.5))))
    )
  (testing "Тестирование конструктора numeric"
    (is (thrown? Exception (ax-cr :numeric nil)))
    (is (thrown? Exception (ax-cr :numeric [1 2 3])))
    (is (thrown? Exception (ax-cr :numeric :a)))
    (is (thrown? Exception (ax-cr :numeric "string")))
    (is (thrown? Exception (ax-cr :numeric {:a 1})))
    (is (thrown? Exception (ax-cr :numeric #{1 2})))
    (is (= :numeric (ax-tag (ax-cr :numeric 10))))
    (is (= 10M (ax-val (ax-cr :numeric 10))))
    (is (number? (ax-val (ax-cr :numeric 10))))
    (is (decimal? (ax-val (ax-cr :numeric 100))))
    (is (decimal? (ax-val (ax-cr :numeric 10N))))
    (is (decimal? (ax-val (ax-cr :numeric 1000M))))
    (is (decimal? (ax-val (ax-cr :numeric 10.5))))
    )
  (testing "Тестирование конструктора ratio"
    (is (thrown? Exception (ax-cr :ratio nil)))
    (is (thrown? Exception (ax-cr :ratio 1)))
    (is (thrown? Exception (ax-cr :ratio 1 2 3)))
    (is (thrown? Exception (ax-cr :ratio nil nil)))
    (is (thrown? Exception (ax-cr :ratio 1 nil)))
    (is (thrown? Exception (ax-cr :ratio nil 2)))
    (is (thrown? Exception (ax-cr :ratio :a 2)))
    (is (thrown? Exception (ax-cr :ratio nil "string")))
    (is (thrown? Exception (ax-cr :ratio 1 -20)))
    (is (thrown? Exception (ax-cr :ratio 100 0)))
    (is (thrown? Exception (ax-cr :ratio 1.1 20)))
    (is (thrown? Exception (ax-cr :ratio 1 20.2)))
    (is (thrown? Exception (ax-cr :ratio 1.1 20.000004)))
    (is (coll? (ax-val (ax-cr :ratio 1 7))))
    (is (-> (ax-val (ax-cr :ratio 1 7)) first number?))
    (is (-> (ax-val (ax-cr :ratio 1 7)) second number?))
    (is (-> (ax-val (ax-cr :ratio 1 7)) first integer?))
    (is (-> (ax-val (ax-cr :ratio 1 7)) second integer?))
    (is (-> (ax-val (ax-cr :ratio 1.0 7)) first integer?))
    (is (-> (ax-val (ax-cr :ratio 1 7.0)) second integer?))
    (is (-> (ax-val (ax-cr :ratio 10.0 20.0)) first integer?))
    (is (-> (ax-val (ax-cr :ratio 10.0M 20.0M)) second integer?))
    (is (-> (ax-val (ax-cr :ratio -10.0 20.0)) first neg?))
    (is (-> (ax-val (ax-cr :ratio 125 75)) first (= 125)))
    (is (-> (ax-val (ax-cr :ratio -45 100)) second (= 100)))
    )
  )


(deftest test-ax-input-pair-arguments-x-y
  (testing "Тест проверки пары операндов"
    (is (thrown? Exception (ax-check-pair-and-prep (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax-check-pair-and-prep (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax-check-pair-and-prep (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax-check-pair-and-prep (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))
    )
  (testing "Тест проверки входных параметров операции сложения"
    (is (thrown? Exception (ax-add (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax-add (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax-add (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax-add (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))
    )
  (testing "Тест проверки входных параметров операции сравнения"
    (is (thrown? Exception (ax-cmp (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax-cmp (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax-cmp (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax-cmp (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))

    (is (thrown? Exception (ax< (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax< (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax< (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax< (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))

    (is (thrown? Exception (ax= (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax= (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax= (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax= (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))

    (is (thrown? Exception (ax> (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax> (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax> (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax> (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))

    (is (thrown? Exception (ax<= (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax<= (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax<= (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax<= (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))

    (is (thrown? Exception (ax>= (ax-cr :tip-1 1) (ax-cr :tip-2 1))))
    (is (thrown? Exception (ax>= (ax-cr :tip-1 nil) (ax-cr :tip-1 1))))
    (is (thrown? Exception (ax>= (ax-cr :tip-1 1) (ax-cr :tip-1 nil))))
    (is (thrown? Exception (ax>= (ax-cr :tip-1 nil) (ax-cr :tip-1 nil))))
    )
  )

(deftest test-ax-check
  (is (thrown? Exception (ax-check nil)))
  (is (thrown? Exception (ax-check [nil 1])))

  (is (thrown? Exception (ax-check [:bigint nil])))
  (is (thrown? Exception (ax-check [:ratio [1 nil]])))
  )

(deftest test-ax+
  (letfn [(test-loop [tip-tag x n ii inc-x-fn & [scale round-mode]]
            (letfn [(o+ [x y]
                      (if (or (nil? scale) (nil? round-mode)) (+ x y)
                          (-> (+ x y) bigdec (.setScale scale round-mode)))
                      )]

              (is (== 0 (ax-val (ax-add (ax-cr tip-tag 0) (ax-cr tip-tag 0)))))
              (is (= (ax-cr tip-tag 0) (ax-add (ax-cr tip-tag 0) (ax-cr tip-tag 0))))

              (loop [i 0 x x n n]
                (if (> i ii) (do (println "END x = " (ax-cr tip-tag x) " --> OK."))
                    (let [x- (* -1  x)
                          y  (*  2  x)
                          k  (bigint (/  n  2))]
                      (do

                        (is (== 0 (ax-val (ax-add (ax-cr tip-tag x) (ax-cr tip-tag x-)))))
                        (is (= (ax-cr tip-tag 0) (ax-add (ax-cr tip-tag x) (ax-cr tip-tag x-))))
                        (is (== 0 (ax-val (ax-add (ax-cr tip-tag x-) (ax-cr tip-tag x)))))
                        (is (= (ax-cr tip-tag 0) (ax-add (ax-cr tip-tag x-) (ax-cr tip-tag x))))

                        (is (== (+ x y) (ax-val (ax-add (ax-cr tip-tag x) (ax-cr tip-tag y)))))
                        (is (= (ax-cr tip-tag (+ x y)) (ax-add (ax-cr tip-tag x) (ax-cr tip-tag y))))
                        (is (== (+ x y) (ax-val (ax-add (ax-cr tip-tag y) (ax-cr tip-tag x)))))
                        (is (= (ax-cr tip-tag (+ x y)) (ax-add (ax-cr tip-tag y) (ax-cr tip-tag x))))

                        (is (== (+ x n) (ax-val (ax-add (ax-cr tip-tag x) (ax-cr tip-tag n)))))
                        (is (= (ax-cr tip-tag (+ x n)) (ax-add (ax-cr tip-tag x) (ax-cr tip-tag n))))
                        (is (== (+ x n) (ax-val (ax-add (ax-cr tip-tag n) (ax-cr tip-tag x)))))
                        (is (= (ax-cr tip-tag (+ x n)) (ax-add (ax-cr tip-tag n) (ax-cr tip-tag x))))

                        (is (== (+ y k) (ax-val (ax-add (ax-cr tip-tag y) (ax-cr tip-tag k)))))
                        (is (= (ax-cr tip-tag (+ y k)) (ax-add (ax-cr tip-tag y) (ax-cr tip-tag k))))
                        (is (== (+ y k) (ax-val (ax-add (ax-cr tip-tag k) (ax-cr tip-tag y)))))
                        (is (= (ax-cr tip-tag (+ y k)) (ax-add (ax-cr tip-tag k) (ax-cr tip-tag y))))

                        (is (== (+ 0 n) (ax-val (ax-add (ax-cr tip-tag 0) (ax-cr tip-tag n)))))
                        (is (= (ax-cr tip-tag (+ 0 n)) (ax-add (ax-cr tip-tag 0) (ax-cr tip-tag n))))
                        (is (== (+ 0 n) (ax-val (ax-add (ax-cr tip-tag n) (ax-cr tip-tag 0)))))
                        (is (= (ax-cr tip-tag (+ 0 n)) (ax-add (ax-cr tip-tag n) (ax-cr tip-tag 0))))

                        (recur (inc i) (inc-x-fn x) (dec n))))))))

          ]
    (testing "Тест сложения bigint"
      (test-loop :bigint  0    100 1000 inc)
      (test-loop :bigint -100  0   1000 inc)
      (test-loop :bigint -100 +100 1000 inc)
      (test-loop :bigint (long (/ -9223372036854775808 4)) 9223372036854775807 1000 inc)
      )

    (testing "Тест сложения money"
      (test-loop :money  0M   10M 2000 #(+ %1 0.01M) 2 java.math.RoundingMode/HALF_UP)
      (test-loop :money -10M  0M  2000 #(+ %1 1.11M) 2 java.math.RoundingMode/HALF_UP)
      (test-loop :money -10M +10M 2000 #(+ %1 1.21M) 2 java.math.RoundingMode/HALF_UP)
      (test-loop :money (bigdec (/ -9223372036854775808 4)) 9223372036854775807  1000 #(+ 1.51M %1) 2 java.math.RoundingMode/HALF_UP)
      )

    (testing "Тест сложения numeric"
      (test-loop :numeric  0M    100M 3000 #(+ %1 0.001M) 2 java.math.RoundingMode/HALF_UP)
      (test-loop :numeric -100M  0M   3000 #(+ %1 1.011M) 2 java.math.RoundingMode/HALF_UP)
      (test-loop :numeric -100M +100M 3000 #(+ %1 1.111M) 2 java.math.RoundingMode/HALF_UP)
      (test-loop :numeric (bigdec (/ -9223372036854775808 4)) 9223372036854775807  1000 #(+ 1.51M %1) 3 java.math.RoundingMode/HALF_UP)
      )

    (testing "Тест сложения ratio"
      (letfn [
              (test-loop-ratio [f to x y n d]
                (loop [i 0 x x y y n n]
                  (if (> i to) (do (println "END x = " (ax-cr :ratio x d) " --> OK."))
                      (let [rx  (ax-cr :ratio x d)
                            rx2 (ax-cr :ratio (*  2 x)  d)
                            rx- (ax-cr :ratio (* -1 x)  d)
                            ry  (ax-cr :ratio y d)
                            rn  (ax-cr :ratio n d)
                            rz  (ax-cr :ratio 0 d)]

                        (is (= rz (ax-add rz rz)))
                        (is (= [0 d] (ax-val (ax-add rz rz))))

                        (is (= rx (ax-add rx rz)))
                        (is (= [x d] (-> (ax-add rx rz) ax-val)))
                        (is (= (ax-add rx rz) (ax-add rz rx)))

                        (is (= rx2 (ax-add rx rx)))
                        (is (= [(* x 2) d] (-> (ax-add rx rx) ax-val)))

                        (is (= rz (ax-add rx rx-)))
                        (is (= [0 d] (-> (ax-add rx rx-) ax-val)))
                        (is (= (ax-add rx- rx) (ax-add rx rx-)))

                        (is (= [(+ x (* -1 x) (* -1 x)) d] (ax-val (ax-add rx- (ax-add rx rx-)))))
                        (is neg? (-> rx (ax-add rx-) (ax-add rx-) ax-val first))

                        (is (= [(+ x y) d] (ax-val (ax-add rx ry))))
                        (is (= [(+ x y) d] (ax-val (ax-add ry rx))))

                        (is (= [(+ x n) d] (ax-val (ax-add rx rn))))
                        (is (= [(+ x n) d] (ax-val (ax-add rn rx))))

                        (is (= [(+ y n) d] (ax-val (ax-add ry rn))))

                        (recur (f i) (inc x) (* y 2) (dec n)  )))))
              ]


        (test-loop-ratio inc 1000 0 0 111 1)
        (test-loop-ratio inc 1000 0 0 444 2)
        (test-loop-ratio inc 1000 0 0 323 9)
        (test-loop-ratio inc 10000 -1223372036854775808 0 1223372036854775807 57)
        )
      )
    ))

(deftest test-ax-cmp
  (testing "Тестирование сравнения bigint"
    (is (= -1 (ax-cmp (ax-cr :bigint 5) (ax-cr :bigint 10))))
    (is (= -1 (ax-cmp (ax-cr :bigint -5) (ax-cr :bigint 5))))
    (is (= -1 (ax-cmp (ax-cr :bigint -1) (ax-cr :bigint 0))))
    (is (=  0 (ax-cmp (ax-cr :bigint 0) (ax-cr :bigint 0))))
    (is (=  0 (ax-cmp (ax-cr :bigint 12) (ax-cr :bigint 12))))
    (is (=  0 (ax-cmp (ax-cr :bigint -5) (ax-cr :bigint -5))))
    (is (=  1 (ax-cmp (ax-cr :bigint 1) (ax-cr :bigint 0))))
    (is (=  1 (ax-cmp (ax-cr :bigint 50) (ax-cr :bigint 10))))
    (is (=  1 (ax-cmp (ax-cr :bigint 25) (ax-cr :bigint 5))))


    (is (true?  (ax< (ax-cr :bigint 5) (ax-cr :bigint 6))))
    (is (false? (ax< (ax-cr :bigint 6) (ax-cr :bigint 5))))
    (is (false? (ax< (ax-cr :bigint 5) (ax-cr :bigint 5))))

    (is (true?  (ax= (ax-cr :bigint 5) (ax-cr :bigint 5))))
    (is (false? (ax= (ax-cr :bigint 6) (ax-cr :bigint 5))))
    (is (false? (ax= (ax-cr :bigint 5) (ax-cr :bigint 6))))

    (is (true?  (ax> (ax-cr :bigint 6) (ax-cr :bigint 5))))
    (is (false? (ax> (ax-cr :bigint 5) (ax-cr :bigint 6))))
    (is (false? (ax> (ax-cr :bigint 5) (ax-cr :bigint 5))))


    (is (true?  (ax<= (ax-cr :bigint 5) (ax-cr :bigint 6))))
    (is (true?  (ax<= (ax-cr :bigint 6) (ax-cr :bigint 6))))
    (is (false? (ax<= (ax-cr :bigint 6) (ax-cr :bigint 5))))

    (is (true?  (ax>= (ax-cr :bigint 7) (ax-cr :bigint 6))))
    (is (true?  (ax>= (ax-cr :bigint 6) (ax-cr :bigint 6))))
    (is (false? (ax>= (ax-cr :bigint 6) (ax-cr :bigint 7))))
    )
  (testing "Тестирование сравнения money"
    (is (= -1 (ax-cmp (ax-cr :money 5.01) (ax-cr :money 10.64))))
    (is (= -1 (ax-cmp (ax-cr :money -5.01) (ax-cr :money 5.64))))
    (is (= -1 (ax-cmp (ax-cr :money -1.01) (ax-cr :money 0.64))))
    (is (=  0 (ax-cmp (ax-cr :money 0.01) (ax-cr :money 0.01))))
    (is (=  0 (ax-cmp (ax-cr :money 12.01) (ax-cr :money 12.01))))
    (is (=  0 (ax-cmp (ax-cr :money -5.01) (ax-cr :money -5.01))))
    (is (=  1 (ax-cmp (ax-cr :money 1.01) (ax-cr :money 0.64))))
    (is (=  1 (ax-cmp (ax-cr :money 50.01) (ax-cr :money 10.64))))
    (is (=  1 (ax-cmp (ax-cr :money 25.01) (ax-cr :money 5.64))))

    (is (true?  (ax< (ax-cr :money 5.01) (ax-cr :money 6.64))))
    (is (false? (ax< (ax-cr :money 6.01) (ax-cr :money 5.64))))
    (is (false? (ax< (ax-cr :money 5.13) (ax-cr :money 5.13))))

    (is (true?  (ax= (ax-cr :money 5.66) (ax-cr :money 5.66))))
    (is (false? (ax= (ax-cr :money 6.01) (ax-cr :money 5.64))))
    (is (false? (ax= (ax-cr :money 5.01) (ax-cr :money 6.64))))

    (is (true?  (ax> (ax-cr :money 6.01) (ax-cr :money 5.64))))
    (is (false? (ax> (ax-cr :money 5.01) (ax-cr :money 6.64))))
    (is (false? (ax> (ax-cr :money 5.01) (ax-cr :money 5.64))))


    (is (true?  (ax<= (ax-cr :money 5.01) (ax-cr :money 6.64))))
    (is (true?  (ax<= (ax-cr :money 6.01) (ax-cr :money 6.64))))
    (is (false? (ax<= (ax-cr :money 6.01) (ax-cr :money 5.64))))

    (is (true?  (ax>= (ax-cr :money 7.01) (ax-cr :money 6.64))))
    (is (true?  (ax>= (ax-cr :money 6.64) (ax-cr :money 6.64))))
    (is (false? (ax>= (ax-cr :money 6.01) (ax-cr :money 7.64))))
    )
  (testing "Тестирование сравнения numeric"
    (is (= -1 (ax-cmp (ax-cr :numeric 5.001) (ax-cr :numeric 10.648))))
    (is (= -1 (ax-cmp (ax-cr :numeric -5.001) (ax-cr :numeric 5.648))))
    (is (= -1 (ax-cmp (ax-cr :numeric -1.001) (ax-cr :numeric 0.648))))
    (is (=  0 (ax-cmp (ax-cr :numeric 0.001) (ax-cr :numeric 0.001))))
    (is (=  0 (ax-cmp (ax-cr :numeric 12.021) (ax-cr :numeric 12.021))))
    (is (=  0 (ax-cmp (ax-cr :numeric -5.001) (ax-cr :numeric -5.001))))
    (is (=  1 (ax-cmp (ax-cr :numeric 0.002) (ax-cr :numeric 0.001))))
    (is (=  1 (ax-cmp (ax-cr :numeric -0.002) (ax-cr :numeric -0.003))))
    (is (=  1 (ax-cmp (ax-cr :numeric 1.001) (ax-cr :numeric 0.648))))
    (is (=  1 (ax-cmp (ax-cr :numeric 50.001) (ax-cr :numeric 10.648))))
    (is (=  1 (ax-cmp (ax-cr :numeric 25.001) (ax-cr :numeric 5.648))))

    (is (true?  (ax< (ax-cr :numeric 5.001) (ax-cr :numeric 6.648))))
    (is (false? (ax< (ax-cr :numeric 6.001) (ax-cr :numeric 5.648))))
    (is (false? (ax< (ax-cr :numeric 5.648) (ax-cr :numeric 5.648))))

    (is (true?  (ax= (ax-cr :numeric 5.011) (ax-cr :numeric 5.011))))
    (is (false? (ax= (ax-cr :numeric 6.001) (ax-cr :numeric 5.648))))
    (is (false? (ax= (ax-cr :numeric 5.001) (ax-cr :numeric 6.648))))

    (is (true?  (ax> (ax-cr :numeric 6.001) (ax-cr :numeric 5.648))))
    (is (false? (ax> (ax-cr :numeric 5.001) (ax-cr :numeric 6.648))))
    (is (false? (ax> (ax-cr :numeric 5.001) (ax-cr :numeric 5.648))))


    (is (true?  (ax<= (ax-cr :numeric 5.001) (ax-cr :numeric 6.648))))
    (is (true?  (ax<= (ax-cr :numeric 6.001) (ax-cr :numeric 6.648))))
    (is (false? (ax<= (ax-cr :numeric 6.001) (ax-cr :numeric 5.648))))

    (is (true?  (ax>= (ax-cr :numeric 7.001) (ax-cr :numeric 6.648))))
    (is (true?  (ax>= (ax-cr :numeric 6.101) (ax-cr :numeric 6.101))))
    (is (false? (ax>= (ax-cr :numeric 6.001) (ax-cr :numeric 7.648))))
    )
  (testing "Тестирование сравнения ratio"
    (is (= -1 (ax-cmp (ax-cr :ratio 5 8) (ax-cr :ratio 10 8))))
    (is (= -1 (ax-cmp (ax-cr :ratio -5 8) (ax-cr :ratio 5 8))))
    (is (= -1 (ax-cmp (ax-cr :ratio -1 8) (ax-cr :ratio 0 8))))
    (is (=  0 (ax-cmp (ax-cr :ratio 0 8) (ax-cr :ratio 0 8))))
    (is (=  0 (ax-cmp (ax-cr :ratio 12 8) (ax-cr :ratio 12 8))))
    (is (=  0 (ax-cmp (ax-cr :ratio -5 8) (ax-cr :ratio -5 8))))
    (is (=  1 (ax-cmp (ax-cr :ratio 1 8) (ax-cr :ratio 0 8))))
    (is (=  1 (ax-cmp (ax-cr :ratio 50 8) (ax-cr :ratio 10 8))))
    (is (=  1 (ax-cmp (ax-cr :ratio 25 8) (ax-cr :ratio 5 8))))

    (is (true?  (ax< (ax-cr :ratio 5 8) (ax-cr :ratio 6 8))))
    (is (false? (ax< (ax-cr :ratio 6 8) (ax-cr :ratio 5 8))))
    (is (false? (ax< (ax-cr :ratio 5 8) (ax-cr :ratio 5 8))))

    (is (true?  (ax= (ax-cr :ratio 5 8) (ax-cr :ratio 5 8))))
    (is (false? (ax= (ax-cr :ratio 6 8) (ax-cr :ratio 5 8))))
    (is (false? (ax= (ax-cr :ratio 5 8) (ax-cr :ratio 6 8))))

    (is (true?  (ax> (ax-cr :ratio 6 8) (ax-cr :ratio 5 8))))
    (is (false? (ax> (ax-cr :ratio 5 8) (ax-cr :ratio 6 8))))
    (is (false? (ax> (ax-cr :ratio 5 8) (ax-cr :ratio 5 8))))


    (is (true?  (ax<= (ax-cr :ratio 5 8) (ax-cr :ratio 6 8))))
    (is (true?  (ax<= (ax-cr :ratio 6 8) (ax-cr :ratio 6 8))))
    (is (false? (ax<= (ax-cr :ratio 6 8) (ax-cr :ratio 5 8))))

    (is (true?  (ax>= (ax-cr :ratio 7 8) (ax-cr :ratio 6 8))))
    (is (true?  (ax>= (ax-cr :ratio 6 8) (ax-cr :ratio 6 8))))
    (is (false? (ax>= (ax-cr :ratio 6 8) (ax-cr :ratio 7 8))))

    (is (thrown? Exception (ax>= (ax-cr :bigint 6 8) (ax-cr :ratio 7 8))))


    (is (thrown? Exception (ax>= (ax-cr :ratio 6 8) (ax-cr :ratio 7 9))))

    (is (thrown? Exception (ax>= (ax-cr :ratio nil 8) (ax-cr :ratio 7 8))))
    (is (thrown? Exception (ax>= (ax-cr :ratio 1 nil) (ax-cr :ratio 7 8))))
    (is (thrown? Exception (ax>= (ax-cr :ratio 1 8) (ax-cr :ratio nil 8))))
    (is (thrown? Exception (ax>= (ax-cr :ratio 1 8) (ax-cr :ratio 7 nil))))
    (is (thrown? Exception (ax>= (ax-cr :ratio nil nil) (ax-cr :ratio nil nil))))
    )
  )

(deftest test-ax-neg
  (testing "Тестирование инверсии знака bigint"
    (is (= (ax-cr :bigint -100) (ax-neg (ax-cr :bigint 100))))
    (is (= (ax-cr :bigint 100) (ax-neg (ax-cr :bigint -100))))
    (is (= (ax-cr :bigint 0) (ax-neg (ax-cr :bigint 0))))
    )

  (testing "Тестирование инверсии знака money"
    (is (= (ax-cr :money -100.01) (ax-neg (ax-cr :money 100.01))))
    (is (= (ax-cr :money 100.02) (ax-neg (ax-cr :money -100.02))))
    (is (= (ax-cr :money 0) (ax-neg (ax-cr :money 0))))
    )

  (testing "Тестирование инверсии знака numeric"
    (is (= (ax-cr :numeric -100.011) (ax-neg (ax-cr :numeric 100.011))))
    (is (= (ax-cr :numeric 100.012) (ax-neg (ax-cr :numeric -100.012))))
    (is (= (ax-cr :numeric 0) (ax-neg (ax-cr :numeric 0))))
    )

  (testing "Тестирование инверсии знака ratio"
    (is (= (ax-cr :ratio -1 8) (ax-neg (ax-cr :ratio  1 8))))
    (is (= (ax-cr :ratio  1 8) (ax-neg (ax-cr :ratio -1 8))))
    (is (= (ax-cr :ratio 0 4) (ax-neg (ax-cr :ratio 0 4))))
    )
  )


(deftest test-ax-zero
  (testing "Тестирование 0 для bigint"
    (is (= (ax-cr :bigint 0) (ax-zero (ax-cr :bigint  10))))
    (is (= (ax-cr :bigint 0) (ax-zero (ax-cr :bigint  0 ))))
    (is (= (ax-cr :bigint 0) (ax-zero (ax-cr :bigint -10))))
    )
  (testing "Тестирование 0 для money"
    (is (= (ax-cr :money 0) (ax-zero (ax-cr :money  10))))
    (is (= (ax-cr :money 0) (ax-zero (ax-cr :money  0.01))))
    (is (= (ax-cr :money 0) (ax-zero (ax-cr :money  0 ))))
    (is (= (ax-cr :money 0) (ax-zero (ax-cr :money -0.01))))
    (is (= (ax-cr :money 0) (ax-zero (ax-cr :money -10))))
    )
  (testing "Тестирование 0 для numeric"
    (is (= (ax-cr :numeric 0) (ax-zero (ax-cr :numeric  10))))
    (is (= (ax-cr :numeric 0) (ax-zero (ax-cr :numeric  0.001))))
    (is (= (ax-cr :numeric 0) (ax-zero (ax-cr :numeric  0 ))))
    (is (= (ax-cr :numeric 0) (ax-zero (ax-cr :numeric -0.001))))
    (is (= (ax-cr :numeric 0) (ax-zero (ax-cr :numeric -10))))
    )
  (testing "Тестирование 0 для ratio"
    (is (= (ax-cr :ratio 0 8) (ax-zero (ax-cr :ratio  20 8))))
    (is (= (ax-cr :ratio 0 8) (ax-zero (ax-cr :ratio  1  8))))
    (is (= (ax-cr :ratio 0 8) (ax-zero (ax-cr :ratio  0  8))))
    (is (= (ax-cr :ratio 0 8) (ax-zero (ax-cr :ratio -1  8))))
    (is (= (ax-cr :ratio 0 8) (ax-zero (ax-cr :ratio -21 8))))
    )
  )

(deftest test-ax-
  (testing "Тестирование test-ax- для bigint"
    (is (= (ax-cr :bigint 0) (ax-sub (ax-cr :bigint  10) (ax-cr :bigint  10))))
    (is (= (ax-cr :bigint 0) (ax-sub (ax-cr :bigint  0 ) (ax-cr :bigint  0 ))))
    (is (= (ax-cr :bigint 0) (ax-sub (ax-cr :bigint -10) (ax-cr :bigint -10))))

    (is (= (ax-cr :bigint   4) (ax-sub (ax-cr :bigint  10) (ax-cr :bigint   6))))
    (is (= (ax-cr :bigint  16) (ax-sub (ax-cr :bigint  10) (ax-cr :bigint  -6))))
    (is (= (ax-cr :bigint  -5) (ax-sub (ax-cr :bigint  10) (ax-cr :bigint  15))))
    (is (= (ax-cr :bigint   0) (ax-sub (ax-cr :bigint  44) (ax-cr :bigint  44))))
    (is (= (ax-cr :bigint -20) (ax-sub (ax-cr :bigint -10) (ax-cr :bigint  10))))
    (is (= (ax-cr :bigint   0) (ax-sub (ax-cr :bigint -10) (ax-cr :bigint -10))))
    (is (= (ax-cr :bigint   0) (ax-sub (ax-cr :bigint   0) (ax-cr :bigint   0))))
    )
  (testing "Тестирование test-ax- для money"
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money  10) (ax-cr :money  10))))
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money  0 ) (ax-cr :money  0 ))))
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money -10) (ax-cr :money -10))))

    (is (= (ax-cr :money   4) (ax-sub (ax-cr :money  10) (ax-cr :money   6))))
    (is (= (ax-cr :money  16) (ax-sub (ax-cr :money  10) (ax-cr :money  -6))))
    (is (= (ax-cr :money  -5) (ax-sub (ax-cr :money  10) (ax-cr :money  15))))
    (is (= (ax-cr :money   0) (ax-sub (ax-cr :money  44) (ax-cr :money  44))))
    (is (= (ax-cr :money -20) (ax-sub (ax-cr :money -10) (ax-cr :money  10))))
    (is (= (ax-cr :money   0) (ax-sub (ax-cr :money -10) (ax-cr :money -10))))
    (is (= (ax-cr :money   0) (ax-sub (ax-cr :money   0) (ax-cr :money   0))))

    (is (= (ax-cr :money   9.4) (ax-sub (ax-cr :money     10) (ax-cr :money    0.6))))
    (is (= (ax-cr :money  10.6) (ax-sub (ax-cr :money     10) (ax-cr :money   -0.6))))
    (is (= (ax-cr :money  -0.5) (ax-sub (ax-cr :money     10) (ax-cr :money   10.5))))
    (is (= (ax-cr :money     0) (ax-sub (ax-cr :money  44.01) (ax-cr :money  44.01))))
    (is (= (ax-cr :money   -20) (ax-sub (ax-cr :money  -9.95) (ax-cr :money  10.05))))
    (is (= (ax-cr :money     0) (ax-sub (ax-cr :money  -0.01) (ax-cr :money  -0.01))))

    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money  10) (ax-cr :money  10))))
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money  0.01) (ax-cr :money  0.01))))
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money  0 ) (ax-cr :money  0 ))))
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money -0.01) (ax-cr :money -0.01))))
    (is (= (ax-cr :money 0) (ax-sub (ax-cr :money -10) (ax-cr :money -10))))
    )
  (testing "Тестирование test-ax- для numeric"
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric  10) (ax-cr :numeric  10))))
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric  0 ) (ax-cr :numeric  0 ))))
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric -10) (ax-cr :numeric -10))))

    (is (= (ax-cr :numeric   4) (ax-sub (ax-cr :numeric  10) (ax-cr :numeric   6))))
    (is (= (ax-cr :numeric  16) (ax-sub (ax-cr :numeric  10) (ax-cr :numeric  -6))))
    (is (= (ax-cr :numeric  -5) (ax-sub (ax-cr :numeric  10) (ax-cr :numeric  15))))
    (is (= (ax-cr :numeric   0) (ax-sub (ax-cr :numeric  44) (ax-cr :numeric  44))))
    (is (= (ax-cr :numeric -20) (ax-sub (ax-cr :numeric -10) (ax-cr :numeric  10))))
    (is (= (ax-cr :numeric   0) (ax-sub (ax-cr :numeric -10) (ax-cr :numeric -10))))
    (is (= (ax-cr :numeric   0) (ax-sub (ax-cr :numeric   0) (ax-cr :numeric   0))))

    (is (= (ax-cr :numeric  9.4) (ax-sub (ax-cr :numeric    10) (ax-cr :numeric   0.6))))
    (is (= (ax-cr :numeric 10.6) (ax-sub (ax-cr :numeric    10) (ax-cr :numeric  -0.6))))
    (is (= (ax-cr :numeric -0.5) (ax-sub (ax-cr :numeric    10) (ax-cr :numeric  10.5))))
    (is (= (ax-cr :numeric    0) (ax-sub (ax-cr :numeric 44.01) (ax-cr :numeric 44.01))))
    (is (= (ax-cr :numeric  -20) (ax-sub (ax-cr :numeric -9.95) (ax-cr :numeric 10.05))))
    (is (= (ax-cr :numeric    0) (ax-sub (ax-cr :numeric -0.01) (ax-cr :numeric -0.01))))

    (is (= (ax-cr :numeric  9.334) (ax-sub (ax-cr :numeric     10) (ax-cr :numeric  0.666))))
    (is (= (ax-cr :numeric 10.666) (ax-sub (ax-cr :numeric     10) (ax-cr :numeric -0.666))))
    (is (= (ax-cr :numeric -0.005) (ax-sub (ax-cr :numeric     10) (ax-cr :numeric 10.005))))
    (is (= (ax-cr :numeric      0) (ax-sub (ax-cr :numeric 44.001) (ax-cr :numeric 44.001))))
    (is (= (ax-cr :numeric    -20) (ax-sub (ax-cr :numeric -9.955) (ax-cr :numeric 10.045))))
    (is (= (ax-cr :numeric      0) (ax-sub (ax-cr :numeric -0.001) (ax-cr :numeric -0.001))))

    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric  10) (ax-cr :numeric  10))))
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric  0.001) (ax-cr :numeric  0.001))))
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric  0 ) (ax-cr :numeric  0 ))))
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric -0.001) (ax-cr :numeric -0.001))))
    (is (= (ax-cr :numeric 0) (ax-sub (ax-cr :numeric -10) (ax-cr :numeric -10))))
    )
  (testing "Тестирование test-ax- для ratio"
    (is (= (ax-cr :ratio   4 7) (ax-sub (ax-cr :ratio  10 7) (ax-cr :ratio   6 7))))
    (is (= (ax-cr :ratio  16 7) (ax-sub (ax-cr :ratio  10 7) (ax-cr :ratio  -6 7))))
    (is (= (ax-cr :ratio  -5 7) (ax-sub (ax-cr :ratio  10 7) (ax-cr :ratio  15 7))))
    (is (= (ax-cr :ratio   0 7) (ax-sub (ax-cr :ratio  44 7) (ax-cr :ratio  44 7))))
    (is (= (ax-cr :ratio -20 7) (ax-sub (ax-cr :ratio -10 7) (ax-cr :ratio  10 7))))
    (is (= (ax-cr :ratio   0 7) (ax-sub (ax-cr :ratio -10 7) (ax-cr :ratio -10 7))))

    (is (= (ax-cr :ratio 0 8) (ax-sub (ax-cr :ratio  20 8) (ax-cr :ratio  20 8))))
    (is (= (ax-cr :ratio 0 8) (ax-sub (ax-cr :ratio  1  8) (ax-cr :ratio  1  8))))
    (is (= (ax-cr :ratio 0 8) (ax-sub (ax-cr :ratio  0  8) (ax-cr :ratio  0  8))))
    (is (= (ax-cr :ratio 0 8) (ax-sub (ax-cr :ratio -1  8) (ax-cr :ratio -1  8))))
    (is (= (ax-cr :ratio 0 8) (ax-sub (ax-cr :ratio -21 8) (ax-cr :ratio -21 8))))
    )
  )



(deftest test-ax*
  (testing "Тестирование test-ax* для bigint"
    (is (= (ax-cr :bigint 0) (ax-mul (ax-cr :bigint  10) 0)))
    (is (= (ax-cr :bigint 0) (ax-mul (ax-cr :bigint  0 ) 0)))
    (is (= (ax-cr :bigint 0) (ax-mul (ax-cr :bigint -10) 0)))
    (is (= (ax-cr :bigint -10) (ax-mul (ax-cr :bigint 10) -1)))
    (is (= (ax-cr :bigint 10) (ax-mul (ax-cr :bigint -10) -1)))

    (is (= (ax-cr :bigint  40) (ax-mul (ax-cr :bigint  10) 4)))
    (is (= (ax-cr :bigint  16) (ax-mul (ax-cr :bigint   4) 4)))
    (is (= (ax-cr :bigint  -5) (ax-mul (ax-cr :bigint  10) -0.5))) ;; !!! подумать можно ли так делать
    (is (= (ax-cr :bigint   0) (ax-mul (ax-cr :bigint  44) 0)))
    (is (= (ax-cr :bigint -500) (ax-mul (ax-cr :bigint -10) 50)))
    (is (= (ax-cr :bigint 500) (ax-mul (ax-cr :bigint -10) -50)))
    (is (= (ax-cr :bigint   0) (ax-mul (ax-cr :bigint -10) 0)))
    (is (= (ax-cr :bigint   0) (ax-mul (ax-cr :bigint   0) 0)))
    )
  (testing "Тестирование test-ax* для money"
    (is (= (ax-cr :money 0) (ax-mul (ax-cr :money  10) 0)))
    (is (= (ax-cr :money 0) (ax-mul (ax-cr :money  0 ) 0)))
    (is (= (ax-cr :money 0) (ax-mul (ax-cr :money -10) 0)))
    (is (= (ax-cr :money -10) (ax-mul (ax-cr :money 10) -1)))
    (is (= (ax-cr :money 10) (ax-mul (ax-cr :money -10) -1)))

    (is (= (ax-cr :money  40) (ax-mul (ax-cr :money  10) 4)))
    (is (= (ax-cr :money  16) (ax-mul (ax-cr :money   4) 4)))
    (is (= (ax-cr :money  -5) (ax-mul (ax-cr :money  10) -0.5))) ;; !!! подумать можно ли так делать
    (is (= (ax-cr :money   0) (ax-mul (ax-cr :money  44) 0)))
    (is (= (ax-cr :money -500) (ax-mul (ax-cr :money -10) 50)))
    (is (= (ax-cr :money 500) (ax-mul (ax-cr :money -10) -50)))
    (is (= (ax-cr :money   0) (ax-mul (ax-cr :money -10) 0)))
    (is (= (ax-cr :money   0) (ax-mul (ax-cr :money   0) 0)))

    (is (= (ax-cr :money   0.0) (ax-mul (ax-cr :money   0.0) 0.0)))
    (is (= (ax-cr :money   0.1) (ax-mul (ax-cr :money   10) 0.01)))
    (is (= (ax-cr :money   100.35) (ax-mul (ax-cr :money   2.23) 45)))
    (is (= (ax-cr :money   0.84) (ax-mul (ax-cr :money   28.01) 0.03)))
    (is (= (ax-cr :money   534.0) (ax-mul (ax-cr :money   44.5) 12)))
    )
  (testing "Тестирование test-ax* для numeric"
    (is (= (ax-cr :numeric 0) (ax-mul (ax-cr :numeric  10) 0)))
    (is (= (ax-cr :numeric 0) (ax-mul (ax-cr :numeric  0 ) 0)))
    (is (= (ax-cr :numeric 0) (ax-mul (ax-cr :numeric -10) 0)))
    (is (= (ax-cr :numeric -10) (ax-mul (ax-cr :numeric 10) -1)))
    (is (= (ax-cr :numeric 10) (ax-mul (ax-cr :numeric -10) -1)))

    (is (= (ax-cr :numeric  40) (ax-mul (ax-cr :numeric  10) 4)))
    (is (= (ax-cr :numeric  16) (ax-mul (ax-cr :numeric   4) 4)))
    (is (= (ax-cr :numeric  -5) (ax-mul (ax-cr :numeric  10) -0.5))) ;; !!! подумать можно ли так делать
    (is (= (ax-cr :numeric   0) (ax-mul (ax-cr :numeric  44) 0)))
    (is (= (ax-cr :numeric -500) (ax-mul (ax-cr :numeric -10) 50)))
    (is (= (ax-cr :numeric 500) (ax-mul (ax-cr :numeric -10) -50)))
    (is (= (ax-cr :numeric   0) (ax-mul (ax-cr :numeric -10) 0)))
    (is (= (ax-cr :numeric   0) (ax-mul (ax-cr :numeric   0) 0)))

    (is (= (ax-cr :numeric   0.0) (ax-mul (ax-cr :numeric   0.0) 0.0)))
    (is (= (ax-cr :numeric   0.1) (ax-mul (ax-cr :numeric   10) 0.01)))
    (is (= (ax-cr :numeric   100.35) (ax-mul (ax-cr :numeric   2.23) 45)))
    (is (= (ax-cr :numeric   0.84) (ax-mul (ax-cr :numeric   28.01) 0.03)))
    (is (= (ax-cr :numeric   0.847) (ax-mul (ax-cr :numeric   28.23) 0.03)))
    (is (= (ax-cr :numeric   534.0) (ax-mul (ax-cr :numeric   44.5) 12)))
    )
  (testing "Тестирование test-ax* для ratio"
    (is (= (ax-cr :ratio   0 7) (ax-mul (ax-cr :ratio  10 7) 0)))
    (is (= (ax-cr :ratio  16 7) (ax-mul (ax-cr :ratio  10 7) 1.6)))
    (is (= (ax-cr :ratio  -5 7) (ax-mul (ax-cr :ratio  10 7) -0.5)))
    (is (= (ax-cr :ratio   0 7) (ax-mul (ax-cr :ratio  44 7) 0)))
    (is (= (ax-cr :ratio -20 7) (ax-mul (ax-cr :ratio -10 7) 2)))
    (is (= (ax-cr :ratio 20 7) (ax-mul (ax-cr :ratio 10 7) 2)))
    (is (= (ax-cr :ratio   0 7) (ax-mul (ax-cr :ratio -10 7) 0)))

    (is (= (ax-cr :ratio 0 8) (ax-mul (ax-cr :ratio  20 8) 0)))
    (is (= (ax-cr :ratio 0 8) (ax-mul (ax-cr :ratio  1  8) 0)))
    (is (= (ax-cr :ratio 0 8) (ax-mul (ax-cr :ratio  0  8) 0)))
    (is (= (ax-cr :ratio 0 8) (ax-mul (ax-cr :ratio -1  8) 0)))
    (is (= (ax-cr :ratio 0 8) (ax-mul (ax-cr :ratio -21 8) 0)))
    )
  )


(deftest test-ax_
  (testing "Тестирование test-ax-div для bigint"
    (is (thrown? Exception (ax-div (ax-cr :bigint  0 ) (ax-cr :bigint  0 ))))
    (is (thrown? Exception (ax-div (ax-cr :bigint  30 ) (ax-cr :bigint  0 ))))

    (is (= 1 (ax-div (ax-cr :bigint  10) (ax-cr :bigint  10))))
    (is (= 1 (ax-div (ax-cr :bigint -10) (ax-cr :bigint -10))))
    (is (= 10 (ax-div (ax-cr :bigint  10) (ax-cr :bigint   1))))
    (is (= -5 (ax-div (ax-cr :bigint  10) (ax-cr :bigint  -2))))
    (is (= -4 (ax-div (ax-cr :bigint  -24) (ax-cr :bigint  6))))
    (is (= 10 (ax-div (ax-cr :bigint  440) (ax-cr :bigint  44))))
    (is (= 10 (ax-div (ax-cr :bigint 10) (ax-cr :bigint  1))))
    (is (= 10 (ax-div (ax-cr :bigint -600) (ax-cr :bigint -60))))
    )
  (testing "Тестирование test-ax-div для money"
    (is (thrown? Exception (ax-div (ax-cr :money  0 ) (ax-cr :money  0 ))))
    (is (thrown? Exception (ax-div (ax-cr :money  0.0 ) (ax-cr :money  0 ))))
    (is (thrown? Exception (ax-div (ax-cr :money  30 ) (ax-cr :money  0.0 ))))
    (is (thrown? Exception (ax-div (ax-cr :money  30.0 ) (ax-cr :money  0.0 ))))

    (is (= 1M (ax-div (ax-cr :money  10) (ax-cr :money  10))))
    (is (= 1M (ax-div (ax-cr :money -10) (ax-cr :money -10))))
    (is (= 10M (ax-div (ax-cr :money  10) (ax-cr :money   1))))
    (is (= -5M (ax-div (ax-cr :money  10) (ax-cr :money  -2))))
    (is (= -4M (ax-div (ax-cr :money  -24) (ax-cr :money  6))))
    (is (= 10M (ax-div (ax-cr :money  440) (ax-cr :money  44))))
    (is (= 10M (ax-div (ax-cr :money 10) (ax-cr :money  1))))
    (is (= 10M (ax-div (ax-cr :money -600) (ax-cr :money -60))))
    (is (= 7.42M (ax-div (ax-cr :money 44.5) (ax-cr :money 6))))
    )
  (testing "Тестирование test-ax-div для numeric"
    (is (thrown? Exception (ax-div (ax-cr :numeric  0 ) (ax-cr :numeric  0 ))))
    (is (thrown? Exception (ax-div (ax-cr :numeric  0.0 ) (ax-cr :numeric  0 ))))
    (is (thrown? Exception (ax-div (ax-cr :numeric  30 ) (ax-cr :numeric  0.0 ))))
    (is (thrown? Exception (ax-div (ax-cr :numeric  30.0 ) (ax-cr :numeric  0.0 ))))

    (is (= 1M (ax-div (ax-cr :numeric  10) (ax-cr :numeric  10))))
    (is (= 1M (ax-div (ax-cr :numeric -10) (ax-cr :numeric -10))))
    (is (= 10M (ax-div (ax-cr :numeric  10) (ax-cr :numeric   1))))
    (is (= -5M (ax-div (ax-cr :numeric  10) (ax-cr :numeric  -2))))
    (is (= -4M (ax-div (ax-cr :numeric  -24) (ax-cr :numeric  6))))
    (is (= 10M (ax-div (ax-cr :numeric  440) (ax-cr :numeric  44))))
    (is (= 10M (ax-div (ax-cr :numeric 10) (ax-cr :numeric  1))))
    (is (= 10M (ax-div (ax-cr :numeric -600) (ax-cr :numeric -60))))
    (is (= 7.417M (ax-div (ax-cr :numeric 44.5) (ax-cr :numeric 6))))
    )
  (testing "Тестирование test-ax-div для ratio"
    (is (thrown? Exception (ax-div (ax-cr :ratio  0 11) (ax-cr :ratio  0 11))))
    (is (thrown? Exception (ax-div (ax-cr :ratio  30 23) (ax-cr :ratio  0 23))))
    (is (thrown? Exception (ax-div (ax-cr :ratio  30 23) (ax-cr :ratio  4 33))))

    (is (= 1 (ax-div (ax-cr :ratio  10 7) (ax-cr :ratio   10 7))))
    (is (= -1 (ax-div (ax-cr :ratio  10 7) (ax-cr :ratio  -10 7))))
    (is (= -1 (ax-div (ax-cr :ratio  -10 7) (ax-cr :ratio  10 7))))
    (is (= 0 (ax-div (ax-cr :ratio  10 7) (ax-cr :ratio  15 7)))) ;; Подумать можно ли так!!!
    (is (= 1 (ax-div (ax-cr :ratio -10 7) (ax-cr :ratio -10 7))))
    (is (= 10 (ax-div (ax-cr :ratio  20 8) (ax-cr :ratio  2 8))))
    (is (= 0 (ax-div (ax-cr :ratio  1  8) (ax-cr :ratio  2 8)))) ;; Подумать можно ли так!!!
    (is (= 3 (ax-div (ax-cr :ratio 21 8) (ax-cr :ratio 7 8))))
    )
  )




;; END Type tests
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Test counted attributes
;;* tag: <test ca>
;;*
;;* description: Тестирование счетных атрибутов
;;*
;;**************************************************************************************************

(defn ca-standart-coll+test []
  "Создание дополнительного тестового набора"
  (->> ca-standart-coll
       (merge
        {
         :shoe-size {
                     :title "Размеры обуви"
                     :tip :bigint
                     :description "Размеры обуви..."
                     :sub {
                           :29 {:title "29 размер"}
                           :30 {:title "30 размер"}
                           :31 {:title "31 размер"}
                           :32 {:title "32 размер"}
                           :33 {:title "33 размер"}
                           }
                     }

         :colored-wire {
                        :title "цвета провода"
                        :tip :numeric
                        :description "цвета провода..."
                        :sub {
                              :black {:title "черный"}
                              :white {:title "белый"}
                              :brown {:title "коричневый"}
                              }
                        }

         :colored-pills {
                         :title "цвета таблеток"
                         :tip :ratio
                         :description "цвета таблеток..."
                         :sub {
                               :red {:title "красный"}
                               :green {:title "зеленый"}
                               :blue {:title "голубой"}
                               }
                         }

         :colored-size {
                        :title "цвета предметов"
                        :tip :ratio
                        :description "цвета предметов..."
                        :sub {
                              :red {
                                    :title "красный"
                                    :sub {
                                          :17 {:title "17 размер"}
                                          :18 {:title "18 размер"}
                                          :19 {:title "19 размер"}
                                          }
                                    }

                              :green {
                                      :title "зеленый"
                                      :sub {
                                            :17 {:title "17 размер"}
                                            :18 {:title "18 размер"}
                                            :19 {:title "19 размер"}
                                            }
                                      }

                              :blue {
                                     :title "голубой"
                                     :sub {
                                           :17 {:title "17 размер"}
                                           :18 {:title "18 размер"}
                                           :19 {:title "19 размер"}
                                           }
                                     }
                              }
                        }
         })

       create-ca-as-list

       ))


(deftest test-ca-save
  (let [row {;;:parent_id
             :tip :bigint
             :keyname :test-ca
             :title "Тестовый атрибут"
             :description "Описание...."}]
    (testing "Тестирование неправильных значений"
      (is (thrown? Exception (ca-save (assoc row :tip :not-exist-tag))))
      (is (thrown? Exception (ca-save (assoc row :tip nil))))
      (is (thrown? Exception (ca-save (assoc row :tip "bigint"))))
      (is (thrown? Exception (ca-save (assoc row :keyname nil))))
      (is (thrown? Exception (ca-save (assoc row :tip nil))))
      )
    (testing "Тестирование сохранения, обновления и удаления"
      ;; TODO: Тут требуется особое тестирование
      (comm-test-save-and-delete
       ca ca-save
       row (assoc row :keyname :test-bigint-2 :title "Test ca 2" :description "Test description 2")
       [:id :root_id :parent_id])
      )
    )
  )



(deftest test-ca-main-list
  (ca-standart-coll+test)
  (testing "Тестирование списка"
    (let [l (ca-main-list)
          c (count l)]
      (is (->> l (filter #(-> % :parent_id nil? not)) count (= 0)))
      (is (->> l (filter #(-> % :parent_id nil? )) count (= c)))
      (is (->> l (filter #(= (% :root_id) (% :id))) count (= c)))
      )
    )
  )


(defn check-flat-hierarchy-> [get-id-fn get-parent-id-fn [p & r]]
  (let [id (get-id-fn p)]
    (reduce (fn [[pre-parents last-id] row]
              (let [parent-id (get-parent-id-fn row)
                    id        (get-id-fn row)]
                (println parent-id id [pre-parents last-id])
                (cond (nil? pre-parents) []
                      (contains? pre-parents parent-id) [pre-parents id]
                      (= parent-id last-id) [(conj pre-parents last-id) id]
                      :else (throw (Exception. "Неупорядоченная структура счетных атрибутов"))
                      )
                ))
            [#{id} id]
            ;;(reverse r) ; Test for test
            r
            )))


(deftest test-sort-tree-as-flat
  (let [tree-list-1 [{:id 1 :parent_id nil}

                     {:id 2 :parent_id 1}

                     {:id 3 :parent_id 2}
                     {:id 4 :parent_id 3}
                     {:id 5 :parent_id 3}

                     {:id 6 :parent_id 2}
                     {:id 7 :parent_id 6}
                     {:id 8 :parent_id 6}

                     {:id 9 :parent_id 1}

                     {:id 10 :parent_id 9}
                     {:id 11 :parent_id 10}
                     {:id 12 :parent_id 10}

                     {:id 13 :parent_id 9}
                     {:id 14 :parent_id 13}
                     {:id 15 :parent_id 13}

                     {:id 16 :parent_id 1}

                     {:id 17 :parent_id 16}
                     {:id 18 :parent_id 17}
                     {:id 19 :parent_id 17}

                     {:id 20 :parent_id 16}
                     {:id 21 :parent_id 20}
                     {:id 22 :parent_id 20}
                     ]
        tree-list-1-count (count tree-list-1)
        ]

    (testing "тест сортировки базовой древовидной структуры"
      (check-flat-hierarchy-> :id :parent_id tree-list-1)
      (is (thrown? Exception (check-flat-hierarchy-> :id :parent_id (reverse tree-list-1))))
      (is (= (count (ix/sort-tree-as-flat :id :parent_id (reverse tree-list-1))) (count tree-list-1)))
      (is (= (count (ix/sort-tree-as-flat :id :parent_id tree-list-1)) (count tree-list-1)))
      (check-flat-hierarchy-> :id :parent_id (ix/sort-tree-as-flat :id :parent_id (reverse tree-list-1)))
      (loop [tree-list tree-list-1 n 0]
        (if (> n 100) (is true)
            (let [band (dec tree-list-1-count)
                  i-1 (-> band rand-int int)
                  i-2 (-> band rand-int int)
                  x-1 (nth tree-list i-1)
                  x-2 (nth tree-list i-2)
                  tree-list-next (-> tree-list
                                     (assoc i-2 x-1)
                                     (assoc i-1 x-2))
                  tree-list-next-sorted-1 (ix/sort-tree-as-flat :id :parent_id tree-list-next)
                  tree-list-next-sorted-2 (ix/sort-tree-as-flat :id :parent_id (reverse tree-list-next))
                  tree-list-next-sorted-1-count (count tree-list-next-sorted-1)
                  tree-list-next-sorted-2-count (count tree-list-next-sorted-2)
                  ]
              (println "\nnext" "(" i-1 ":=><=:" i-2 "):" tree-list-next "\n")
              (is (= tree-list-1-count tree-list-next-sorted-1-count))
              (is (= tree-list-1-count tree-list-next-sorted-2-count))
              (is (check-flat-hierarchy-> :id :parent_id tree-list-next-sorted-1))
              (is (check-flat-hierarchy-> :id :parent_id tree-list-next-sorted-2))
              (is (= tree-list-next-sorted-1 tree-list-next-sorted-2))
              (is (= tree-list-1 tree-list-next-sorted-2))
              (recur tree-list-next (inc n)))))
      )
    )
  )


(deftest test-ca-get-for-id
  (ca-standart-coll+test)
  (testing "Тестирование получения структуры атрибутов ао id"
    (let [count-sql (ix/com-count ca)
          l (ca-main-list)
          c (count l)
          ll (map #(-> % :id ca-get-for-id) l)
          lll (reduce into ll)
          count-lll (count lll)]
      (testing "проверка на уникальность ключей"
        (is (= count-sql count-lll))

        (check-unique [:id] lll)
        (check-unique [:id, :tip] lll)
        (check-unique [:parent_id, :keyname] lll)
        (check-unique [:parent_id, :title] lll)
        (check-unique [:id, :root_id, :parent_id, :tip] lll)

        )
      (testing "проверка родительских атрибутов"
        (doall
         (map #(is (-> % first :parent_id nil?)) ll))
        )
      (testing "проверка родительских атрибутов на принадлежность к самим себе по :root_id"
        (doall
         (map (fn [[h]]
                (let [{id :id root-id :root_id} h]
                  (is (= root-id id))))
              ll))
        )
      (testing "проверка дочерних атрибутов на отношение к родительским"
        (doall
         (map (fn [[h & r]]
                (let [{root-root-id :root_id root-t :tip root-k :keyname} h]
                  (is (not (nil? root-t)))
                  (is (keyword? root-t))
                  (is (not (nil? root-k)))
                  (is (keyword? root-k))
                  (when-not (empty? r)
                    (map (fn [{root-id :root_id t :tip k :keyname}]
                           (is (= root-root-id root-id))
                           (is (not (nil? t)))
                           (is (keyword? t))
                           (is (= root-t t))
                           (is (not (nil? k)))
                           (is (keyword? k)))
                         r))))
              ll))
        )
      (testing "проверка списка атрибутов по возрвзтанию принадлежности к родительской иерархии"
        (doall
         (map (partial check-flat-hierarchy-> :id :parent_id) ll)
         )
        )
      )
    )
  )






;; END
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Test counted objects
;;* tag: <test webdoc>
;;*
;;* description: Тестирование объектов учета
;;*
;;**************************************************************************************************

(deftest test-webdoc-save
  (ca-standart-coll+test)
  (let [ca-id (-> (korma.core/select ca (korma.core/where (= :keyname (name :standart-bigint)))) first :id)]
    (testing "Тестирование сохранения и удаления webdoc"
      (comm-test-save-and-delete webdoc webdoc-save
                                 {:keyname "test webdoc 1",
                                  :description "Test description 1",
                                  :ca_id ca-id,
                                  :scod nil,
                                  :tip :bigint,
                                  :val (ax-cr :bigint 1),

                                  :is_product  true
                                  }
                                 {:keyname "test webdoc 2",
                                  :description "Test description 2",
                                  :ca_id ca-id,
                                  :scod nil,
                                  :tip :bigint,
                                  :val (ax-cr :bigint 1),
                                  :is_product  true
                                  }

                                 [:id

                                  :udate
                                  :cdate
                                  :plan_text
                                  :ttitle
                                  :url1
                                  :url1flag
                                  :web_citems
                                  :web_description
                                  :web_meta_description
                                  :web_meta_keywords
                                  :web_meta_subject
                                  :web_title
                                  :web_title_image
                                  :web_top_description
                                  :web_color_0
                                  :web_color_1
                                  :web_color_2
                                  :web_color_3
                                  :web_color_4
                                  :web_color_5
                                  ]
                                 )
      )
    (testing "Тестирование неправильных значений"
      (let [row {:keyname "test webdoc 0" :description "Test description 0"
                 :tip :bigint :val_ratio_denum nil :ca_id ca-id}]
        (is (thrown? Exception (webdoc-save (assoc row :tip :not-exist-tag))))
        ;; Документ пока универсален !!!!!!!!!!!!!!! Надо подумать!!!
        ;;(is (thrown? Exception (webdoc-save (assoc row :tip nil))))
        (is (thrown? Exception (webdoc-save (assoc row :tip "bigint"))))
        (is (thrown? Exception (webdoc-save (assoc row :keyname nil))))
        )
      )
    )
  )




(defn get-webdocs []
  (letfn [(get-ca-id [keyname]
            (-> (korma.core/select ca (korma.core/where (= :keyname (name keyname)))) first :id))]
    (println "Создание тестовых счетных объектов")
    (ca-standart-coll+test)
    {
     :webdoc-radiolamp-1 (webdoc-save {:keyname "тест-радиолампа" :tip :bigint :val (ax-cr :bigint 1) :ca_id (get-ca-id :standart-bigint) :description "Тестовый товар"})
     :webdoc-currency-kzt (webdoc-save {:keyname "тест-KZT" :tip :money :val (ax-cr :money 1) :ca_id (get-ca-id :standart-money) :description  "Тестовые деньги KZT"})
     :webdoc-plumbum-1 (webdoc-save {:keyname "тест-олово" :tip :numeric :val (ax-cr :numeric 1) :ca_id (get-ca-id :standart-numeric) :description "Тестовый товар"})
     :webdoc-trasistors-mp38 (webdoc-save {:keyname "тест-транзисторы МП38 (пачка)" :tip :ratio :val (ax-cr :ratio 1 15) :ca_id (get-ca-id :standart-ratio) :description "Тестовый товар" })

     :webdoc-shoe (webdoc-save {:keyname "тест-туфли" :tip :bigint :val (ax-cr :bigint 1) :ca_id (get-ca-id :shoe-size) :description "Тестовые туфли"})
     :webdoc-wire (webdoc-save {:keyname "тест-провода цветные" :tip :numeric :val (ax-cr :numeric 1) :ca_id (get-ca-id :colored-wire) :description "Тестовый провод"})
     :webdoc-pills (webdoc-save {:keyname "тест-таблетки" :tip :ratio :val (ax-cr :ratio 1 20) :ca_id (get-ca-id :colored-pills) :description "Тестовые таблетки"})
     :webdoc-bezdelushki (webdoc-save {:keyname "тест-цветоразмерные-безделушки" :tip :ratio :val (ax-cr :ratio 1 20) :ca_id (get-ca-id :colored-size) :description "Тестовый безделушки" })
     }))


(defn t-init-webdocs []
  (def t-webdoc-radiolamp-1  (:webdoc-radiolamp-1  (get-webdocs)))
  (def t-webdoc-currency-kzt  (:webdoc-currency-kzt (get-webdocs)))
  (def t-webdoc-plumbum-1  (:webdoc-plumbum-1 (get-webdocs)))
  (def t-webdoc-trasistors-mp38  (:webdoc-trasistors-mp38 (get-webdocs)))
  (def t-webdoc-shoe  (:webdoc-shoe (get-webdocs)))
  (def t-webdoc-wire  (:webdoc-wire (get-webdocs)))
  (def t-webdoc-pills  (:webdoc-pills (get-webdocs)))
  (def t-webdoc-bezdelushki  (:webdoc-bezdelushki (get-webdocs))))

;; END Test counted objects
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Accounts test
;;* tag: <test acc>
;;*
;;* description: Тест счетов
;;*
;;**************************************************************************************************


(deftest test-acccas-find
  (testing "тестирование счетов"
    (let [zones (get-zones)
          owns  (get-owns)
          webdocs (get-webdocs)
          c-sql-count (ix/com-count ca)]
      (doseq [z (vals zones) w (vals owns) o (vals webdocs)]
        (let [c (-> o :ca_id ca-get-for-id)
              _ (println ">>" o)
              c-count (count c)
              a-sql-count-1 (ix/com-count acc)
              acccas (acccas-find z w o)
              acccas-count (count acccas)
              a-sql-count-2 (ix/com-count acc)]
          (println "TEST FOR ACC:\nown:\t" w
                   "\nwebdoc:\t" o
                   "\nca-acc-count:\t" acccas-count
                   ;;"\nca-acc:\t" acccas
                   )
          (print-cas acccas)
          (print-accs acccas)

          (is (= c-count acccas-count))
          (is (= c-sql-count (ix/com-count ca)))
          (is (= a-sql-count-2 (+ a-sql-count-1 c-count)))
          (is (-> c first :parent_id nil?))

          (doall ;; !!! в doall нельзя помещать более одной конструкции !!!
           (->> acccas
                ;;ix/print-debug->>>
                (map accca-get-ca)
                ;;ix/print-debug->>>
                (check-flat-hierarchy-> :id :parent_id)))

          (doall ;; !!! в doall нельзя помещать более одной конструкции !!!
           (->> acccas
                ;;ix/print-debug->>>
                (map accca-get-acc)
                ;;ix/print-debug->>>
                (check-flat-hierarchy-> :ca_id :ca_parent_id))
           )

          (check-flat-hierarchy->
           #(-> % accca-get-ca :id)
           #(-> % accca-get-ca :parent_id)
           acccas)

          (check-flat-hierarchy->
           #(-> % accca-get-acc :ca_id)
           #(-> % accca-get-acc :ca_parent_id)
           acccas)


          (doseq [acccas acccas
                  :let [c (accca-get-ca acccas)
                        a (accca-get-acc acccas)]]

            (is (not (empty? c)))
            (is (not (empty? a)))
            (is (= (:tip c) (:tip a)))
            (is (= (:tip c) (-> a :val ax-tag)))
            (is (= (:id c) (:ca_id a)))
            (is (= (:root_id c) (:ca_root_id a)))
            (is (= (:parent_id c) (:ca_parent_id a)))
            )

          ;; Повторный запрос
          (let [acccas-2 (acccas-find z w o)]
            (is (= c-count acccas-count))
            (is (= c-sql-count (ix/com-count ca)))
            (is (= a-sql-count-2 (+ a-sql-count-1 c-count)))

            (doall (map #(is (= %1 %2)) acccas acccas-2))

            )
          )
        )
      )
    )
  )



(deftest test-acccas-check
  (testing "тестирование проверки счетов с циклом"
    (let [zones (get-zones)
          owns  (get-owns)
          webdocs (get-webdocs)
          c-sql-count (ix/com-count ca)]
      (doseq [z (vals zones) w (vals owns) o (vals webdocs)
              :let  [c (-> o :ca_id ca-get-for-id)
                     acccas (acccas-find z w o)
                     acccas-count (count acccas)]]
        (is (acccas-check acccas))
        (loop [i 0 acccas acccas]
          (if (> i 100) nil
              (let [band (dec acccas-count)
                    i-1 (-> band rand-int int)
                    i-2 (-> band rand-int int)
                    x-1 (nth acccas i-1)
                    x-2 (nth acccas i-2)
                    acccas (-> acccas
                               vec
                               (assoc i-2 x-1)
                               (assoc i-1 x-2))

                    v (-> acccas first accca-get-acc-val)
                    t (ax-tag v)
                    v-1 (cond (= t :ratio) (let [[_ d] (ax-val v)] (ax-cr t 1 d))
                              :else (ax-cr t 1))
                    ]
                (is (acccas-check acccas))

                (let [acccas-1 (map #(accca-ax-add-val % v-1) acccas)]
                  (if (< 1 (count acccas-1))
                    (is (thrown? Exception (acccas-check acccas-1)))
                    (is (acccas-check acccas-1)))
                  (doseq [v-2 acccas-1] (is (ax= v-1 (accca-get-acc-val v-2))))
                  )

                (let [v-1-neg (ax-neg v-1)
                      acccas-1 (map #(accca-ax-add-val % v-1-neg) acccas)]
                  (if (< 1 (count acccas-1))
                    (is (thrown? Exception (acccas-check acccas-1)))
                    (is (acccas-check acccas-1)))
                  (doseq [v-2 acccas-1] (is (ax= v-1-neg (accca-get-acc-val v-2))))
                  )
                (recur (inc i) acccas))
              )
          )
        )
      )
    )

  (testing "тестирование проверки по значениям"
    (letfn [(cr-test-accca [t id root-id parent-id & val]
              [{:id (+ 100 id) :tip t :ca_root_id root-id :ca_id id
                :ca_parent_id parent-id :val (apply ax-cr (into [t] val))}
               {:tip t :root_id root-id :id id :parent_id parent-id}]
              )

            (subt [acccas index accca]
              (-> acccas vec (assoc index accca) list*))

            ]

      (testing "тестирование проверки валидаторов"
        (is (thrown? Exception (acccas-check nil)))
        (is (thrown? Exception (acccas-check [])))
        (is (thrown? Exception (acccas-check '())))

        ;; Not described tip
        (is (thrown? Exception (acccas-check (list (cr-test-accca :tip-1 0  0 nil 0)))))
        (is (thrown? Exception (acccas-check (list (cr-test-accca :tip-1 0  0 nil 0)
                                                   (cr-test-accca :tip-1 1  0 0   0)
                                                   (cr-test-accca :tip-1 2  0 1   0)
                                                   (cr-test-accca :tip-1 3  0 1   0)))))
        ;; Различные теги
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  0 nil 0)
                                                   (cr-test-accca :money  1  0 0   0)
                                                   (cr-test-accca :bigint 2  0 1   0)
                                                   (cr-test-accca :bigint 3  0 1   0)))))
        ;; корневых элементов больше одного
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  0 nil 0)
                                                   (cr-test-accca :bigint 1  0 0   0)
                                                   (cr-test-accca :bigint 2  0 nil 0)
                                                   (cr-test-accca :bigint 3  0 1   0)))))
        ;; нет корневых элементов
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  0 0   0)
                                                   (cr-test-accca :bigint 1  0 0   0)
                                                   (cr-test-accca :bigint 2  0 1   0)
                                                   (cr-test-accca :bigint 3  0 1   0)))))
        ;; сбитый root
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  1 nil 0)))))
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  0 nil 0)
                                                   (cr-test-accca :bigint 1  1 0   0)
                                                   (cr-test-accca :bigint 2  1 1   0)
                                                   (cr-test-accca :bigint 3  1 1   0)))))
        ;; сбитый root
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  2 nil 0)
                                                   (cr-test-accca :bigint 1  2 0   0)
                                                   (cr-test-accca :bigint 2  2 1   0)
                                                   (cr-test-accca :bigint 3  2 1   0)))))
        ;; сбитый root
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  0 nil 0)
                                                   (cr-test-accca :bigint 1  0 0   0)
                                                   (cr-test-accca :bigint 2  0 1   0)
                                                   (cr-test-accca :bigint 3  1 1   0)))))
        ;; сбитый root
        (is (thrown? Exception (acccas-check (list (cr-test-accca :bigint 0  0 nil 0)
                                                   (cr-test-accca :bigint 1  1 0   0)
                                                   (cr-test-accca :bigint 2  2 1   0)
                                                   (cr-test-accca :bigint 3  3 1   0)))))

        (testing "тест валидации согласованности с ca"
          (letfn [(test-accca [t1 t2
                               ca-id-1 ca-id-2
                               ca-root-id-1 ca-root-id-2
                               ca-parent-id-1 ca-parent-id-2
                               val]
                    [{:id 100 :tip t1 :ca_root_id ca-root-id-1
                      :ca_id ca-id-1 :ca_parent_id ca-parent-id-1 :val val}
                     {:id ca-id-2 :tip t2 :root_id ca-root-id-2  :parent_id ca-parent-id-2}])]

            (is (acccas-check (list (test-accca :bigint :bigint 0 0 0 0 nil nil (ax-cr :bigint 10)))))

            (is (thrown? Exception (acccas-check (list (test-accca :bigint :money  0 0 0 0 nil nil (ax-cr :bigint 10))))))
            (is (thrown? Exception (acccas-check (list (test-accca :bigint :bigint 0 0 0 0 nil nil (ax-cr :money  10))))))

            (is (thrown? Exception (acccas-check (list (test-accca :bigint :bigint 0 0 1 0 nil nil (ax-cr :bigint 10))))))
            (is (thrown? Exception (acccas-check (list (test-accca :bigint :bigint 0 0 0 1 nil nil (ax-cr :bigint 10))))))
            (is (thrown? Exception (acccas-check (list (test-accca :bigint :bigint 0 0 0 0 1   nil (ax-cr :bigint 10))))))
            (is (thrown? Exception (acccas-check (list (test-accca :bigint :bigint 0 0 0 0 nil 1   (ax-cr :bigint 10))))))
            ;; TODO Сделать тесты для списков с несколькими элементами
            )
          )
        )
      (testing "тестирование проверки счетов с циклом для (:bigint :money :numeric)"
        (doseq [t [:bigint :money :numeric]]
          (testing (str "Тестирование " t)
            (let [acccas (list (cr-test-accca t 0  0 nil 0)

                               (cr-test-accca t 1  0 0   0)
                               (cr-test-accca t 2  0 1   0)
                               (cr-test-accca t 3  0 1   0)

                               (cr-test-accca t 4  0 0   0)
                               (cr-test-accca t 5  0 4   0)
                               (cr-test-accca t 6  0 4   0)

                               (cr-test-accca t 7  0 0   0)
                               (cr-test-accca t 8  0 7   0)
                               (cr-test-accca t 9  0 7   0))]
              (is (acccas-check acccas))

              (letfn [(rm [col i] (remove nil? (-> col vec (assoc i nil))))]
                (is (thrown? Exception (-> acccas (rm 0) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 1) acccas-check)))
                (is (-> acccas (rm 2) acccas-check))
                (is (-> acccas (rm 3) acccas-check))
                (is (thrown? Exception (-> acccas (rm 4) acccas-check)))
                (is (-> acccas (rm 5) acccas-check))
                (is (-> acccas (rm 6) acccas-check))
                (is (thrown? Exception (-> acccas (rm 7) acccas-check)))
                (is (-> acccas (rm 8) acccas-check))
                (is (-> acccas (rm 9) acccas-check))
                )

              )

            (let [acccas (list (cr-test-accca t 0  0 nil 10)

                               (cr-test-accca t 1  0 0   5)
                               (cr-test-accca t 2  0 1   1)
                               (cr-test-accca t 3  0 1   4)

                               (cr-test-accca t 4  0 0   1)
                               (cr-test-accca t 5  0 4   1)
                               (cr-test-accca t 6  0 4   0)

                               (cr-test-accca t 7  0 0   4)
                               (cr-test-accca t 8  0 7   1)
                               (cr-test-accca t 9  0 7   3))
                  acccas-count (count acccas)]

              (is (acccas-check acccas))
              (is (acccas-check (reverse acccas)))
              (is (->> acccas (map #(accca-do-val % ax-neg)) acccas-check))

              (letfn [(rm [col i] (remove nil? (-> col vec (assoc i nil))))]
                (is (thrown? Exception (-> acccas (rm 0) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 1) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 2) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 3) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 4) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 5) acccas-check)))
                (is (-> acccas (rm 6) acccas-check))
                (is (thrown? Exception (-> acccas (rm 7) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 8) acccas-check)))
                (is (thrown? Exception (-> acccas (rm 9) acccas-check)))
                )

              (loop [i 0 acccas acccas]
                (if (> i 50) nil
                    (let [band (dec acccas-count)
                          i-1 (-> band rand-int int)
                          i-2 (-> band rand-int int)
                          x-1 (nth acccas i-1)
                          x-2 (nth acccas i-2)
                          acccas-i (-> acccas vec (assoc i-2 x-1) (assoc i-1 x-2))]

                      (is (acccas-check acccas-i))

                      (doseq [i (range band)
                              :let [accca-i (nth acccas-i i)
                                    v (-> accca-i accca-get-acc-val ax-val)
                                    {ca-id :ca_id ca-p-id :ca_parent_id ca-r-id :ca_root_id} (-> accca-i accca-get-acc) ]]

                        #_(do (println ca-id ca-r-id ca-p-id v)
                              (-> acccas-i print-accs)
                              (-> acccas-i (subt i (cr-test-accca t id  r-id p-id (+ v 1))) print-accs))

                        ;; ---------------------------------------------------------------------------------------
                        (is (-> acccas-i (subt i (cr-test-accca t ca-id ca-r-id ca-p-id v)) acccas-check))

                        (when-not (== 0 v)
                          (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id 0)) acccas-check))))

                        (is (-> acccas-i (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ v 0))) acccas-check))
                        (is (-> acccas-i (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- v 0))) acccas-check))
                        (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ v 1))) acccas-check)))
                        (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- v 1))) acccas-check)))

                        (when (= t :money)
                          (is (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ v 0.00))) acccas-check))
                          (is (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- v 0.00))) acccas-check))
                          (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ v 0.01))) acccas-check)))
                          (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- v 0.01))) acccas-check))))

                        (when (= t :numeric)
                          (is (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ v 0.000))) acccas-check))
                          (is (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- v 0.000))) acccas-check))
                          (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ v 0.001))) acccas-check)))
                          (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- v 0.001))) acccas-check))))
                        )

                      (recur (inc i) acccas-i))))
              )
            )
          )
        )
      (testing "тестирование проверки счетов с циклом для :ratio"
        (let [t :ratio]
          (let [acccas (list (cr-test-accca t 0  0 nil 0 2)

                             (cr-test-accca t 1  0 0   0 2)
                             (cr-test-accca t 2  0 1   0 2)
                             (cr-test-accca t 3  0 1   0 2)

                             (cr-test-accca t 4  0 0   0 2)
                             (cr-test-accca t 5  0 4   0 2)
                             (cr-test-accca t 6  0 4   0 2)

                             (cr-test-accca t 7  0 0   0 2)
                             (cr-test-accca t 8  0 7   0 2)
                             (cr-test-accca t 9  0 7   0 2))]
            (is (acccas-check acccas)))

          (let [acccas (list (cr-test-accca t 0  0 nil 10 5)

                             (cr-test-accca t 1  0 0   5 5)
                             (cr-test-accca t 2  0 1   1 5)
                             (cr-test-accca t 3  0 1   4 5)

                             (cr-test-accca t 4  0 0   1 5)
                             (cr-test-accca t 5  0 4   1 5)
                             (cr-test-accca t 6  0 4   0 5)

                             (cr-test-accca t 7  0 0   4 5)
                             (cr-test-accca t 8  0 7   1 5)
                             (cr-test-accca t 9  0 7   3 5))
                acccas-count (count acccas)]

            (is (acccas-check acccas))
            (is (acccas-check (reverse acccas)))
            (is (->> acccas (map #(accca-do-val % ax-neg)) acccas-check))

            (loop [i 0 acccas acccas]
              (if (> i 50) nil
                  (let [band (dec acccas-count)
                        i-1 (-> band rand-int int)
                        i-2 (-> band rand-int int)
                        x-1 (nth acccas i-1)
                        x-2 (nth acccas i-2)
                        acccas-i (-> acccas vec (assoc i-2 x-1) (assoc i-1 x-2))]

                    (is (acccas-check acccas-i))

                    (doseq [i (range band)
                            :let [accca-i (nth acccas-i i)
                                  [n d] (-> accca-i accca-get-acc-val ax-val)
                                  {ca-id :ca_id ca-p-id :ca_parent_id ca-r-id :ca_root_id} (-> accca-i accca-get-acc) ]]

                      ;; ---------------------------------------------------------------------------------------
                      (is (-> acccas-i (subt i (cr-test-accca t ca-id ca-r-id ca-p-id n d)) acccas-check))

                      (when-not (== 0 n)
                        (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id 0 d)) acccas-check))))

                      (is (-> acccas-i (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ n 0) d)) acccas-check))
                      (is (-> acccas-i (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- n 0) d)) acccas-check))
                      (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (+ n 1) d)) acccas-check)))
                      (is (thrown? Exception (-> acccas-i  (subt i (cr-test-accca t ca-id ca-r-id ca-p-id (- n 1) d)) acccas-check)))
                      )
                    (recur (inc i) acccas-i))))
            )
          )
        )
      )
    )
  )


(defmacro transaction-isolation-serializable [& body]
  `(korma.db/transaction
    (korma.core/exec-raw "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;")
    ~@body))


(deftest test-acccas-save
  (testing "тестирование сохранения счетов"
    (let [zones (get-zones)
          owns  (get-owns)
          webdocs (get-webdocs)
          c-sql-count (ix/com-count ca)]
      (doseq [z (vals zones) w (vals owns) o (vals webdocs)
              :let  [c (-> o :ca_id ca-get-for-id)
                     acccas (acccas-find z w o)
                     acccas-count (count acccas)]]
        (transaction-isolation-serializable
         (acccas-save acccas))

        (loop [i 0]
          (if (> i 10) nil
              (let [count-1 (ix/com-count acc)
                    acccas-i (acccas-find z w o)
                    band (-> acccas-i count dec)
                    i-1 (-> band rand-int int)
                    i-2 (-> band rand-int int)
                    x-1 (nth acccas-i i-1)
                    x-2 (nth acccas-i i-2)
                    acccas-i-2 (-> acccas-i vec (assoc i-2 x-1) (assoc i-1 x-2))
                    acccas-2 (transaction-isolation-serializable (acccas-save acccas-i-2))
                    ]

                (is (= count-1 (ix/com-count acc)))
                ;;;;;;(is (= (count acccas-i) (count acccas-2)))
                                        ;TODO: проработать тесты с длинами
                (doall
                 (map (fn [x y z]
                        (let [a-x (accca-get-acc x)
                              c-x (accca-get-ca x)

                              a-y (accca-get-acc y)
                              c-y (accca-get-ca y)

                              a-z (accca-get-acc z)
                              c-z (accca-get-ca z)
                              ]

                          (is (= c-x c-y c-z))
                          (is (= a-y a-z))
                          (is (= (dissoc a-x :version_id) (dissoc a-z :version_id)))
                          (is (= (a-y :version_id) (a-z :version_id)))

                          ))
                      acccas-i acccas-2 (acccas-find z w o)))

                (is (->> [acccas-i (acccas-find z w o)] (map count) (apply =)))

                (recur (inc i)))))
        )
      )
    )
  )





;; END Accounts test
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN test opt
;;* tag: <test opt>
;;*
;;* description: тестирование opt
;;*
;;**************************************************************************************************

(deftest test-opt-save
  (testing "Тестирование сохранения и удаления opt"
    (opttype-describe {:opttype :test-type-0 :title "перемещение test 0"})
    (comm-test-save-and-delete opt opt-save
                               {:opt :opt-0 :opttype :test-type-0 :title "Test opt 1" :description "Test description 1"}
                               {:opt :opt-0 :opttype :test-type-0 :title "Test opt 2" :description "Test description 2"})
    )
  )

;; TODO: дописать еще тесты на ключи и ограничения

;; END test opt
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN test op
;;* tag: <test op>
;;*
;;* description: тестирование op
;;*
;;**************************************************************************************************

(deftest test-op-cr
  (let [_ (opttype-save {:opttype :opttype-1 :title "Opttype 1" :description "Test opt type description 1"})
        {op-tip :opt :as opt-row}  (opt-save {:opt :opt-1 :opttype :opttype-1
                                              :title "Test opt 1" :description "Test description 1"})
        {webuser-id :id :as webuser-row} ((get-webusers) :webuser-1)]
    (testing "Тестирование op-cr"
      (let [count-0 (ix/com-count op)
            op-row (op-cr webuser-row op-tip)
            count-1 (ix/com-count op)]
        (is (= count-0 (dec count-1)))

        (is (-> op-row :id     nil? not))
        (is (-> op-row :opt    nil? not))
        (is (-> op-row :cdate  nil? not))
        (is (-> op-row :webuser_id nil? not))

        (is (-> op-row :id     number?))
        (is (-> op-row :opt    keyword?))
        (is (-> op-row :cdate  type (= org.joda.time.DateTime)))
        (is (-> op-row :webuser_id number?))

        (is (-> op-row :id     (= 0) not))
        (is (-> op-row :opt    (= op-tip)))
        (is (-> op-row :webuser_id (= webuser-id)))

        (doseq [_ (range 100)] (op-cr webuser-row op-tip))

        (is (= (+ count-0 1 100) (ix/com-count op)))

        ))))


;; TODO: дописать еще тесты на ключи и ограничения

;; END test op
;;..................................................................................................

(defmacro test-complect [& body]
  `(let [~'t-webusers (get-webusers)
         ~'t-webuser-1 (:webuser-1 ~'t-webusers)
         ~'t-webuser-2 (:webuser-2 ~'t-webusers)
         ~'t-webuser-3 (:webuser-3 ~'t-webusers)

         ~'t-zones  (get-zones)
         ~'t-zone-1 (:zone-1 ~'t-zones)

         ~'t-owns (get-owns)
         ~'t-own-cash-1      (:own-cash-1      ~'t-owns)
         ~'t-own-cash-2      (:own-cash-2      ~'t-owns)
         ~'t-own-cash-3      (:own-cash-3      ~'t-owns)
         ~'t-own-warehouse-1 (:own-warehouse-1 ~'t-owns)
         ~'t-own-warehouse-2 (:own-warehouse-2 ~'t-owns)
         ~'t-own-chief       (:own-chief       ~'t-owns)
         ~'t-own-customer-1  (:own-customer-1  ~'t-owns)
         ~'t-own-customer-2  (:own-customer-2  ~'t-owns)

         ~'t-webdocs (get-webdocs)
         ~'t-webdoc-radiolamp-1      (:webdoc-radiolamp-1     ~'t-webdocs)
         ~'t-webdoc-currency-kzt     (:webdoc-currency-kzt    ~'t-webdocs)
         ~'t-webdoc-plumbum-1        (:webdoc-plumbum-1       ~'t-webdocs)
         ~'t-webdoc-trasistors-mp38  (:webdoc-trasistors-mp38 ~'t-webdocs)
         ~'t-webdoc-shoe             (:webdoc-shoe            ~'t-webdocs)
         ~'t-webdoc-wire             (:webdoc-wire            ~'t-webdocs)
         ~'t-webdoc-pills            (:webdoc-pills           ~'t-webdocs)
         ~'t-webdoc-bezdelushki      (:webdoc-bezdelushki     ~'t-webdocs)
         ]
     ~@body
     ))

;;**************************************************************************************************
;;* BEGIN test opi
;;* tag: <test opi>
;;*
;;* description: тестирование opi - записей операций
;;*
;;**************************************************************************************************

(deftest test-save-opi-description
  (test-complect
   (opttype-describe {:opttype :test-type-1 :title "перемещение test 1"})
   (opt-describe {:opt :test-1 :opttype :test-type-1 :title "операция перемещения 1" :description "..."})
   (testing "Тестирование op-cr"
     (let [op-row (op-cr t-webuser-1 :test-1)
           acccas (acccas-find t-zone-1 t-own-cash-1 t-webdoc-currency-kzt)
           opi-row-1 (korma.core/insert opi
                                        (korma.core/values {:acc_id (-> acccas first accca-get-acc :id)
                                                            :acc_version_id (-> acccas first accca-get-acc :version_id)
                                                            :op_id (op-row :id)
                                                            :opt :test-1
                                                            :opttype :test-type-1
                                                            :zone_id (t-zone-1 :id)
                                                            :own_id (t-own-cash-1 :id)
                                                            :webdoc_id (t-webdoc-currency-kzt :id)
                                                            :ca_id (-> acccas first accca-get-acc :ca_id)
                                                            :ca_root_id (-> acccas first accca-get-acc :ca_root_id)
                                                            :ca_parent_id (-> acccas first accca-get-acc :ca_parent_id)
                                                            :tip :money
                                                            :description "1"
                                                            :val (ax-cr :money 1000)}))

           opi-row-2 (save-opi-description (assoc opi-row-1 :description "2" :acc_version_id 2))
           ]

       (is (->> [opi-row-1 opi-row-2] (map #(dissoc % :description)) (apply =)))
       (is (= "2" (opi-row-2 :description)))
       ))))

;; END test opi
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN TEST AZONES
;;* tag: <tests azones>
;;*
;;* description: тестирование зон
;;*
;;**************************************************************************************************


;; Организация - торговая сеть ПИЖОН
;; +-Отделение - склад 1
;;   +-Зона учета 1
;; +-Отделение - магазин
;;   +-Зона учета 1
;;     +-Зона учета 1.1 - не продано
;;     +-Зона учета 1.2 - продано


;; ввести 2 типа:
;;   1) Физическая зона учета - зона в которой никогда товара не может быть меньше нуля
;;   2) Логическая зона учета - зона в которой количество может быть меньше нуля

(defn t-init-main-scheme []


  ;; ARLS ------------------------------------------------


  (def arole-client   (arole :client   "Клиент"    "Клиенты, которые совершают покупки"))
  (def arole-supplier (arole :supplier "Поставщик" "Поставщик продукции"))
  (def arole-company  (arole :company  "Компания" "Представляет собственность компании"))

  ;; STATIC OWNS -----------------------------------------

  (def own-company (static-own {:keyname "компания"}
                               [:company]))

  (def own-supplier-noname (static-own {:keyname "Неизвесный поставщик"}
                                       [:supplier]))

  (def own-client-noname (static-own {:keyname "неизвесный клиент"}
                                     [:client]))


  ;; SCHEME ----------------------------------------------

  (main-scheme-new)


  (def azone-main
    (azone {:keyname :main
            :title "Полная зона"
            :path-set #{}
            :description "Описание"}))

  (def azone-external
    (azone {:keyname :extermal
            :title "Внешняя зона"
            :path-set (has-in azone-main)
            :description "Описание"}))

  (def azone-suppliers
    (azone {:keyname :suppliers
            :title "поставщики"
            :path-set (has-in azone-external)
            :aroles-set #{arole-supplier}
            :validators []
            :description "поставщики товаров"}))

  (def azone-clients
    (azone {:keyname :clients
            :title "клиенты"
            :path-set  (has-in azone-external)
            :aroles-set #{arole-client}
            :rules []
            :description "наши дорогие клиенты"}))

  ;; Предприятие

  (def azone-internal
    (azone {:keyname :internal
            :title "Внутренняя зона предприятия"
            :path-set (has-in azone-main)
            :description "Описание"}))

  ;; Баланс клиентов

  (def azone-client-balance
    (azone {:keyname :client-balance
            :title "Баланс клинта"
            :path-set (has-in azone-internal)
            :aroles-set #{arole-company}
            :rules []
            :description "Денежный баланс клиентов"}))

  ;; Склад
  (def azone-warehouse-1
    (azone {:keyname :warehouse-1
            :title "Склад 1"
            :path-set (has-in azone-internal)
            :aroles-set #{arole-company}
            :rules [rule-zone-vals>0
                    (rule-zone-tips #{:bigint :ratio :numeric})]
            :description "Склад предприятия"}))

  ;; Магазин

  (def azone-store-1
    (azone {:keyname :store-1
            :title "Магазин 1"
            :path-set (has-in azone-internal)
            :aroles-set #{}
            :description "Магазин предприятия"}))

  (def azone-store-1-phisical
    (azone {:keyname :store-1-phisical
            :title "Физические объекты учета"
            :path-set (has-in azone-store-1)
            :description "Все что имеются в магазине"}))

  (def azone-store-1-phisical-products
    (azone {:keyname :store-1-phisical-products
            :title  "Товары"
            :path-set (has-in azone-store-1-phisical)
            :rules [rule-zone-vals>0]
            :description "Товары в магазине"}))

  (def azone-store-1-phisical-products-for-sale
    (azone {:keyname :store-1-phisical-products-for-sale
            :title "Товары на продажу"
            :path-set (has-in azone-store-1-phisical-products)
            :aroles-set #{arole-company}
            :rules [(rule-zone-tips #{:bigint :numeric :ratio})]
            :description "Товары которые можно продать"}))

  (def azone-store-1-phisical-products-saled
    (azone {:keyname :store-1-phisical-products-saled
            :title "Товары проданные"
            :path-set (has-in azone-store-1-phisical-products)
            :aroles-set #{arole-client}
            :rules [(rule-zone-tips #{:bigint :numeric :ratio})]
            :description "Товары которые ждут клинта"}))

  (def azone-store-1-phisical-cash
    (azone {:keyname :store-1-phisical-cash
            :title   "Касса"
            :path-set (has-in azone-store-1-phisical)
            :aroles-set #{arole-company}
            :rules [(rule-zone-tips #{:money})]
            :description "Деньги в кассе"}))

  (def azone-store-1-client-balance
    (azone {:keyname :store-1-client-balance
            :title "Баланс клиентов магадзина 1"
            :path-set (has-in azone-store-1 azone-client-balance)
            :aroles-set #{arole-client}
            :rules [(rule-zone-tips #{:money})]
            :description "Денежный баланс клинта в магазине 1"}))
  )




;; END TEST AZONES
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN TEST OPERATIONS
;;* tag: <test operations>
;;*
;;* description: тестирование операций
;;*
;;**************************************************************************************************

(defn t-init-meta-opts []

  ;; типы операций
  (def opttype-move (opttype-describe {:opttype :move :title "перемещение"}))
  ;; операции
  (def opt-move-produsts-from-warehouse-1-to-store-1
    (opt-describe {:opt :move-produsts-from-warehouse-1-to-store-1
                   :title "перемещение товара со склада 1 в магазин 1"
                   :opttype opttype-move
                   :description "простая операция перемещения....."
                   :directions-set #{(direction-> azone-suppliers azone-warehouse-1)}
                   ;; задаем роли, кому разрешена операция
                   :troles-set #{:warehouser}
                   ;; функции-валидаторы для проверки перед выполнением
                   ;;:validators  []
                   }))
  )



(deftest test-do-operatio-2
  (letfn [(set-box [zone-1 own-1 zone-2 own-2 cbj set-list]
            (box-fill-op-item-vals-for-idxs (box-cr zone-1 own-1 zone-2 own-2 cbj) set-list))]
    (let [
          _ (t-init-main-scheme)
          _ (t-init-meta-opts)
          ;; пользователи
          t-webusers (get-webusers)
          t-webuser-1 (:webuser-1 t-webusers)
          t-webuser-2 (:webuser-2 t-webusers)
          t-webuser-3 (:webuser-3 t-webusers)

          ;; проработка ролей
          t-webroles (get-webroles)
          t-webrole-1 (:webrole-1 t-webroles)
          t-webrole-2 (:webrole-2 t-webroles)
          t-webrole-3 (:webrole-3 t-webroles)

          _ (ix/webuserwebrole-add-rel t-webuser-1 t-webrole-1)
          _ (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-1)
          _ (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-2)

          ;; поставщики
          own-supplier-1 (static-own {:keyname "поставщик 1"} [:supplier])
          own-supplier-2 (static-own {:keyname "поставщик 2"} [:supplier])
          own-supplier-3 (static-own {:keyname "поставщик 3"} [:supplier])

          ;; клиенты
          own-client-1 (static-own {:keyname "клиент 1"} [:client])
          own-client-2 (static-own {:keyname "клиент 2"} [:client])
          own-client-3 (static-own {:keyname "клиент 3"} [:client])

          ;; объекты
          t-webdocs (get-webdocs)
          t-webdoc-radiolamp-1      (:webdoc-radiolamp-1     t-webdocs)
          t-webdoc-currency-kzt     (:webdoc-currency-kzt    t-webdocs)
          t-webdoc-plumbum-1        (:webdoc-plumbum-1       t-webdocs)
          t-webdoc-trasistors-mp38  (:webdoc-trasistors-mp38 t-webdocs)
          t-webdoc-shoe             (:webdoc-shoe            t-webdocs)
          t-webdoc-wire             (:webdoc-wire            t-webdocs)
          t-webdoc-pills            (:webdoc-pills           t-webdocs)
          t-webdoc-bezdelushki      (:webdoc-bezdelushki     t-webdocs)
          ]



      (testing "тестирование валидации плохих аргументов описания"

        (is (thrown? Exception (opt-describe nil)))
        (is (thrown? Exception (opt-describe {})))
        (is (thrown? Exception (opt-describe #{})))
        (is (thrown? Exception (opt-describe [])))
        (is (thrown? Exception (opt-describe (list ))))

        (is (thrown? Exception (opt-describe #{1 2 3})))
        (is (thrown? Exception (opt-describe 1)))
        (is (thrown? Exception (opt-describe [1 2 3])))
        (is (thrown? Exception (opt-describe (list 1 2 3))))

        (is (thrown? Exception (opt-describe {;;;;;;;; :opt :test-opt-1 ;; nil
                                              :title "тестовая операция 1"
                                              :description "тестовая операция операция..."
                                              :opttype opttype-move
                                              :directions-set #{(direction-> azone-suppliers azone-warehouse-1)}
                                              ;; задаем роли, кому разрешена операция
                                              :troles-set #{:warehouser}
                                              ;; функции-валидаторы для проверки перед выполнением
                                              :rules-for-operatio []
                                              :validators  []
                                              })))

        (is (thrown? Exception (opt-describe {:opt :test-opt-1 ;; nil
                                            ;;;;;;;:title "тестовая операция 1"
                                              :opttype opttype-move
                                              :description "тестовая операция операция..."
                                              :directions-set #{(direction-> azone-suppliers azone-warehouse-1)}
                                              ;; задаем роли, кому разрешена операция
                                              :troles-set #{:warehouser}
                                              ;; функции-валидаторы для проверки перед выполнением
                                              :rules-for-operatio []
                                              :validators  []
                                              })))

        (is (thrown? Exception (opt-describe {:opt :test-opt-1 ;; nil
                                              :title "тестовая операция 1"
                                              ;;;;;;;:opttype opttype-move
                                              :description "тестовая операция операция..."
                                              :directions-set #{(direction-> azone-suppliers azone-warehouse-1)}
                                              ;; задаем роли, кому разрешена операция
                                              :troles-set #{:warehouser}
                                              ;; функции-валидаторы для проверки перед выполнением
                                              :rules-for-operatio []
                                              :validators  []
                                              })))

        )


      (println "******************************************" 1)
      (testing "Тестирование правильности заполнения описания"
        (let [opt-key (opt-describe {:opt :test-opt-1 ;; nil
                                     :title "тестовая операция 1"
                                     :opttype opttype-move
                                     :description "тестовая операция операция..."
                                     :directions-set #{(direction-> azone-suppliers azone-warehouse-1)}
                                     ;; задаем роли, кому разрешена операция
                                     :troles-set #{:warehouser}
                                     ;; функции-валидаторы для проверки перед выполнением
                                     :rules-for-operatio []
                                     :validators [validator-provocator!]
                                     })
              opt-row (first (korma.core/select opt (korma.core/where (= :opt (name :test-opt-1)))))]

          (is (= opt-key :test-opt-1))
          (is (= opt-key (:opt opt-row)))
          (is (= (-> opt-key meta-opts :title) "тестовая операция 1"))
          (is (= (-> opt-key meta-opts :title) (:title opt-row)))

          (is (= (-> opt-key meta-opts :description) "тестовая операция операция..."))
          (is (= (-> opt-key meta-opts :description) (:description opt-row)))

          (is (= (-> opt-key meta-opts :directions-set)  #{(direction-> azone-suppliers azone-warehouse-1)}))
          (is (= (-> opt-key meta-opts :troles-set)  #{:warehouser}))
          (is (= (-> opt-key meta-opts :validators)  [validator-provocator!]))


          ))

      (println "******************************************" 2)
      (testing "тестирование провокатором плохой валидации"
        (let [test-opt-1 (opt-describe {:opt :test-opt-1
                                        :title "тестовая операция 1"
                                        :opttype opttype-move
                                        :description "тестовая операция операция..."
                                        :directions-set #{(direction-> azone-suppliers azone-warehouse-1)}
                                        ;; задаем роли, кому разрешена операция
                                        :troles-set #{:warehouser}
                                        ;; функции-валидаторы для проверки перед выполнением
                                        :rules-for-operatio [validator-provocator!]
                                        :validators []
                                        })]

          (is (thrown? Exception
                       (do-operatio
                        (operatio-cr t-webuser-1 test-opt-1
                                     [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                               (azone-row azone-warehouse-1) own-company
                                               t-webdoc-radiolamp-1 [[0 (ax-cr :bigint 10)]])])
                        )))
          ))

      (println "******************************************" 3)
      (testing "тестирование операции перемещения 1"
        (let [test-opt-move-1 (opt-describe {:opt :test-move-1
                                             :title "тестовая операция перемещения 1"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{
                                                               (direction-> azone-suppliers azone-warehouse-1)
                                                               ;;(direction-> azone-warehouse-1 azone-suppliers)
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:ratio :numeric :bigint})

                                                                  ]
                                             :validators []
                                             })]

          (do-operatio
           (operatio-cr t-webuser-1 test-opt-move-1
                        [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                  (azone-row azone-warehouse-1) own-company
                                  t-webdoc-radiolamp-1 [[0 (ax-cr :bigint 55)]])]))
          ))

      (println "******************************************" 4)
      (testing "тестирование проверки прав пользователя"
        (let [test-opt-move-1 (opt-describe {:opt :test-move-1
                                             :title "тестовая операция перемещения 1"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-2 :keyname)} ;;<-!!
                                             :directions-set #{
                                                               (direction-> azone-suppliers azone-warehouse-1)
                                                               ;;(direction-> azone-warehouse-1 azone-suppliers)
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:ratio :numeric :bigint})

                                                                  ]
                                             :validators []
                                             })]

          (is (thrown? Exception
                       (do-operatio
                        (operatio-cr t-webuser-1 test-opt-move-1
                                     [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                               (azone-row azone-warehouse-1) own-company
                                               t-webdoc-radiolamp-1 [[0 (ax-cr :bigint 55)]])]))
                       ))))

      (println "******************************************" 5)
      (testing "тестирование проверки допустимых направлений в одну сторону"
        (let [test-opt-move-1 (opt-describe {:opt :test-move-1
                                             :title "тестовая операция перемещения 1"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{
                                                               ;;(direction-> azone-suppliers azone-warehouse-1)
                                                               (direction-> azone-warehouse-1 azone-suppliers) ;;<-!!
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:ratio :numeric :bigint})

                                                                  ]
                                             :validators []
                                             })]

          (is (thrown? Exception
                       (do-operatio
                        (operatio-cr t-webuser-1 test-opt-move-1
                                     [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                               (azone-row azone-warehouse-1) own-company
                                               t-webdoc-radiolamp-1 [[0 (ax-cr :bigint 55)]])]))
                       ))))


      (println "******************************************" 6)
      (testing "тестирование проверки допустимых направлений в одну сторону по типам"
        (let [test-opt-move-1 (opt-describe {:opt :test-move-1
                                             :title "тестовая операция перемещения 1"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{
                                                               (direction-> azone-suppliers azone-warehouse-1)
                                                               ;;(direction-> azone-warehouse-1 azone-suppliers)
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:ratio :numeric}) ;;<-!!

                                                                  ]
                                             :validators []
                                             })]

          (is (thrown? Exception
                       (do-operatio
                        (operatio-cr t-webuser-1 test-opt-move-1
                                     [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                               (azone-row azone-warehouse-1) own-company
                                               t-webdoc-radiolamp-1 [[0 (ax-cr :bigint 55)]])]))
                       ))))


      (println "******************************************" 7)
      (testing "тестирование проверки допустимых типов в зоне"
        (let [test-opt-move-1 (opt-describe {:opt :test-move-1
                                             :title "тестовая операция перемещения 1"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{
                                                               (direction-> azone-suppliers azone-warehouse-1)
                                                               ;;(direction-> azone-warehouse-1 azone-suppliers)
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:ratio :numeric :bigint})

                                                                  ]
                                             :validators []
                                             })]

          (is (thrown? Exception
                       (do-operatio
                        (operatio-cr t-webuser-1 test-opt-move-1
                                     [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                               (azone-row azone-warehouse-1) own-company
                                               t-webdoc-radiolamp-1 [[0 (ax-cr :money 55)]])])) ;; <-!!
                       ))))
      )))






(deftest test-do-operatio
  (letfn [(set-box [zone-1 own-1 zone-2 own-2 cbj set-list]
            (box-fill-op-item-vals-for-idxs (box-cr zone-1 own-1 zone-2 own-2 cbj) set-list))]
    (let [
          _ (t-init-main-scheme)
          _ (t-init-meta-opts)
          ;; пользователи
          t-webusers (get-webusers)
          t-webuser-1 (:webuser-1 t-webusers)
          t-webuser-2 (:webuser-2 t-webusers)
          t-webuser-3 (:webuser-3 t-webusers)

          ;; проработка ролей
          t-webroles (get-webroles)
          t-webrole-1 (:webrole-1 t-webroles)
          t-webrole-2 (:webrole-2 t-webroles)
          t-webrole-3 (:webrole-3 t-webroles)

          _ (ix/webuserwebrole-add-rel t-webuser-1 t-webrole-1)
          _ (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-1)
          _ (ix/webuserwebrole-add-rel t-webuser-2 t-webrole-2)

          ;; поставщики
          own-supplier-1 (static-own {:keyname "поставщик 1"} [:supplier])
          own-supplier-2 (static-own {:keyname "поставщик 2"} [:supplier])
          own-supplier-3 (static-own {:keyname "поставщик 3"} [:supplier])

          ;; клиенты
          own-client-1 (static-own {:keyname "клиент 1"} [:client])
          own-client-2 (static-own {:keyname "клиент 2"} [:client])
          own-client-3 (static-own {:keyname "клиент 3"} [:client])

          ;; объекты
          t-webdocs (get-webdocs)
          t-webdoc-radiolamp-1      (:webdoc-radiolamp-1     t-webdocs)
          t-webdoc-currency-kzt     (:webdoc-currency-kzt    t-webdocs)
          t-webdoc-plumbum-1        (:webdoc-plumbum-1       t-webdocs)
          t-webdoc-trasistors-mp38  (:webdoc-trasistors-mp38 t-webdocs)
          t-webdoc-shoe             (:webdoc-shoe            t-webdocs)
          t-webdoc-wire             (:webdoc-wire            t-webdocs)
          t-webdoc-pills            (:webdoc-pills           t-webdocs)
          t-webdoc-bezdelushki      (:webdoc-bezdelushki     t-webdocs)
          ]

      (testing "тестирование операции перемещения money"
        (let [test-opt-move-1 (opt-describe {:opt :test-move-1
                                             :title "тестовая операция перемещения 1"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{
                                                               (direction-> azone-suppliers azone-warehouse-1)
                                                               ;;(direction-> azone-warehouse-1 azone-suppliers)
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:ratio :numeric :bigint})

                                                                  ]
                                             :validators []
                                             })

              test-opt-move-2 (opt-describe {:opt :test-move-2
                                             :title "тестовая операция перемещения 2"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{
                                                               (direction-> azone-warehouse-1 azone-suppliers)
                                                               ;;(direction-> azone-warehouse-1 azone-suppliers)
                                                               }
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-warehouse-1 azone-suppliers)
                                                                   #{:ratio :numeric :bigint})

                                                                  ]
                                             :validators []
                                             })

              test-opt-move-3 (opt-describe {:opt :test-move-3
                                             :title "тестовая операция перемещения 3"
                                             :opttype opttype-move
                                             :description "тестовая операция операция..."
                                             :troles-set #{(t-webrole-1 :keyname)}
                                             :directions-set #{(direction-> azone-warehouse-1 azone-suppliers)
                                                               (direction-> azone-suppliers azone-warehouse-1)}
                                             :rules-for-operatio [(rule-o-boxes-directions-tips
                                                                   (direction-> azone-warehouse-1 azone-suppliers)
                                                                   #{:ratio})
                                                                  (rule-o-boxes-directions-tips
                                                                   (direction-> azone-suppliers azone-warehouse-1)
                                                                   #{:bigint})]
                                             :validators []
                                             })
              ]
          (letfn [(do-fn-1 [x]
                    (do-operatio
                     (operatio-cr t-webuser-1 test-opt-move-1
                                  [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                            (azone-row azone-warehouse-1) own-company
                                            t-webdoc-radiolamp-1
                                            [[0 (ax-cr :bigint 55)]])
                                   (set-box (azone-row azone-suppliers)   own-supplier-noname
                                            (azone-row azone-warehouse-1) own-company
                                            t-webdoc-bezdelushki
                                            [[0 (ax-cr :ratio 5 20)]
                                             [1 (ax-cr :ratio 5 20)]
                                             [2 (ax-cr :ratio 1 20)]
                                             [3 (ax-cr :ratio 0 20)]
                                             [4 (ax-cr :ratio 4 20)]])
                                   ])))
                  (do-fn-2 [x]
                    (do-operatio
                     (operatio-cr t-webuser-1 test-opt-move-2
                                  [(set-box (azone-row azone-warehouse-1) own-company
                                            (azone-row azone-suppliers)   own-supplier-noname
                                            t-webdoc-radiolamp-1
                                            [[0 (ax-cr :bigint 55)]])
                                   (set-box (azone-row azone-warehouse-1) own-company
                                            (azone-row azone-suppliers)   own-supplier-noname
                                            t-webdoc-bezdelushki
                                            [[0 (ax-cr :ratio 5 20)]
                                             [1 (ax-cr :ratio 5 20)]
                                             [2 (ax-cr :ratio 1 20)]
                                             [3 (ax-cr :ratio 0 20)]
                                             [4 (ax-cr :ratio 4 20)]])
                                   ])))

                  (do-fn-3 [x]
                    (do-operatio
                     (operatio-cr t-webuser-1 test-opt-move-3
                                  [(set-box (azone-row azone-suppliers)   own-supplier-noname
                                            (azone-row azone-warehouse-1) own-company
                                            t-webdoc-radiolamp-1
                                            [[0 (ax-cr :bigint 55)]])
                                   (set-box (azone-row azone-warehouse-1) own-company
                                            (azone-row azone-suppliers)   own-supplier-noname
                                            t-webdoc-bezdelushki
                                            [[0 (ax-cr :ratio 5 20)]
                                             [1 (ax-cr :ratio 5 20)]
                                             [2 (ax-cr :ratio 1 20)]
                                             [3 (ax-cr :ratio 0 20)]
                                             [4 (ax-cr :ratio 4 20)]])
                                   ])))



                  (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1 []
                    (acccas-find (azone-row azone-suppliers) own-supplier-noname t-webdoc-radiolamp-1))
                  (azone-warehouse-own-company-t-webdoc-radiolamp-1 []
                    (acccas-find (azone-row azone-warehouse-1) own-company t-webdoc-radiolamp-1))

                  (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki []
                    (acccas-find (azone-row azone-suppliers) own-supplier-noname t-webdoc-bezdelushki))
                  (azone-warehouse-own-company-t-webdoc-bezdelushki []
                    (acccas-find (azone-row azone-warehouse-1) own-company t-webdoc-bezdelushki))

                  (test-compare-acccas [as1 as2]
                    (doall (map (fn [a1 a2]
                                  (is (= (accca-get-ca a1) (accca-get-ca a2)))
                                  (is (= (accca-get-acc-val a1) (accca-get-acc-val a2))))
                                as1 as2)))



                  (test-compare-acccas=zero [as1]
                    (doseq [a1 as1]
                      (is (ax= (accca-get-acc-val a1) (ax-zero (accca-get-acc-val a1))))))

                  (test-compare-acccas=axval [as1 axval]
                    (doseq [a1 as1
                            :when (= nil (-> a1 accca-get-ca :parent_id))]
                      (is (ax= (accca-get-acc-val a1) axval))))
                  ]

            (println ">>>>" (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1))
            (println ">>>>" (azone-warehouse-own-company-t-webdoc-radiolamp-1))


            (test-compare-acccas
             (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1)
             (azone-warehouse-own-company-t-webdoc-radiolamp-1))

            (test-compare-acccas
             (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki)
             (azone-warehouse-own-company-t-webdoc-bezdelushki))

            (test-compare-acccas=zero (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1))
            (test-compare-acccas=zero (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki))

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint 0))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint 0))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio  0 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio  0 20))


            (println "TEST OPS 1")
            (->> (range 100)
                 (pmap do-fn-1)
                 doall
                 ix/print-debug->>>)

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint -5500))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint  5500))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio  -500 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio   500 20))


            (->> (range 100)
                 (pmap do-fn-1)
                 doall
                 ix/print-debug->>>)

            (println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint -11000))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint  11000))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio  -1000 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio   1000 20))

            (println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            (->> (range 100)
                 (pmap do-fn-2)
                 doall
                 ix/print-debug->>>)

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint -5500))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint  5500))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio  -500 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio   500 20))

            (println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            (is (thrown? Exception  (->> (range 200)
                                         (pmap do-fn-2)
                                         doall
                                         ix/print-debug->>>)))

            (test-compare-acccas
             (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1)
             (azone-warehouse-own-company-t-webdoc-radiolamp-1))

            (test-compare-acccas
             (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki)
             (azone-warehouse-own-company-t-webdoc-bezdelushki))

            (test-compare-acccas=zero (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1))
            (test-compare-acccas=zero (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki))

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint 0))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint 0))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio  0 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio  0 20))


            (Thread/sleep 5000)

            (println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            (->> (range 100)
                 (pmap do-fn-1)
                 doall
                 ix/print-debug->>>)

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint -5500))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint  5500))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio  -500 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio   500 20))

            (println ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            (->> (range 100)
                 (pmap do-fn-3)
                 doall
                 ix/print-debug->>>)

            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-radiolamp-1) (ax-cr :bigint -11000))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-radiolamp-1)         (ax-cr :bigint  11000))
            (test-compare-acccas=axval (azone-suppliers-own-supplier-noname-t-webdoc-bezdelushki) (ax-cr :ratio   0 20))
            (test-compare-acccas=axval (azone-warehouse-own-company-t-webdoc-bezdelushki)         (ax-cr :ratio   0 20))

            (println "\n\n\nTEST POINT 1\n\n\n")

            )))



      #_(testing "тестирование проведения операций money"
          (doseq [x (range 100)]
            (-> (operatio-cr t-webuser-1 :move-1
                             [(set-box t-zone-1 t-own-warehouse-1 t-zone-1 t-own-customer-2 t-webdoc-currency-kzt
                                       [[0 (ax-cr :money 2.50)]])])
                do-operatio)))

      #_(testing "тестирование проведения операций c паралельным вызовом money"
          (->> (range 100)
               (pmap
                (fn [x]
                  (-> (operatio-cr t-webuser-1 :move-1
                                   [(set-box t-zone-1 t-own-warehouse-1 t-zone-1 t-own-warehouse-2 t-webdoc-currency-kzt
                                             [[0 (ax-cr :money 50.64)]])])
                      do-operatio
                      )))
               doall
               ix/print-debug->>>
               ))


      #_(testing "тестирование проведения операций ratio"
          (doseq [x (range 100)]
            (-> (operatio-cr t-webuser-1 :move-1
                             [
                              (set-box t-zone-1 t-own-warehouse-1 t-zone-1 t-own-warehouse-2 t-webdoc-bezdelushki
                                       [[0 (ax-cr :ratio 1 20)]
                                        [1 (ax-cr :ratio 1 20)]
                                        [2 (ax-cr :ratio 1 20)]])
                              ])
                do-operatio)))
      #_(testing "тестирование проведения операций c паралельным вызовом ratio"
          (->> (range 100)
               (pmap
                (fn [x]
                  (-> (operatio-cr t-webuser-1 :move-1
                                   [
                                    (set-box t-zone-1 t-own-warehouse-1 t-zone-1 t-own-warehouse-2 t-webdoc-bezdelushki
                                             [[0 (ax-cr :ratio 5 20)]
                                              [1 (ax-cr :ratio 5 20)]
                                              [2 (ax-cr :ratio 1 20)]
                                              [3 (ax-cr :ratio 0 20)]
                                              [4 (ax-cr :ratio 4 20)]])
                                    ])
                      do-operatio
                      )))
               doall
               ix/print-debug->>>
               ))
      )))


;; END TEST OPERATIONS
;;..................................................................................................
