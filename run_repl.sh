#!/bin/sh

JAVA_HOME=/opt/jdk8

export JAVA_HOME=$JAVA_HOME
export JAVA_CMD=$JAVA_HOME/bin/java
export JDK_HOME=$JAVA_HOME
export LEIN_JAVA_CMD=$JAVA_HOME/bin/java

env | grep JAVA


lein repl