#/sbin/sh

export PATH=$PATH:/opt/PostgreSQL/9.3/bin/
export PGPASSWORD="paradox"

dropdb -e -h localhost -U tarsonis -p 5433 tarsonis
createdb -h localhost -U tarsonis -p 5433 -e -E UTF8 -T template0 tarsonis
psql -h localhost -U tarsonis -p 5433 -f schema.sql tarsonis

