(ns tarsonis.core
  (:use korma.db)
  (:use korma.core)
  (:use clojure.pprint)

  (:require [ixinfestor.core :as ix]
            [clj-time.core :as tco]
            [clj-time.format :as tf]
            [clj-time.coerce :as tc]
            [clj-time.local :as tl]
            )
  )


;;**************************************************************************************************
;;* BEGIN common keys
;;* tag: <common keys>
;;*
;;* description: определение ключей
;;*
;;**************************************************************************************************

(def key-keyname :keyname)
(def key-title :title)
(def key-description :description)
(def key-type :type)
(def key-sql-prefix-k :sql-prefix)


;; cheking!
(defn check-req-keys-in-map [req-keys m]
  (cond (empty? m) (throw (Exception. "Пустое описание"))
        (not (map? m)) (throw (Exception. "Описание не имеет тип map"))
        :else (do
                (doseq  [k req-keys
                         :when (-> m k nil?)]
                  (throw (Exception. (str "не указан необходимый параметр с ключем " k))))
                true)))

(defn on-map-if-nil?-set-default-vals [m m-defaults]
  (reduce (fn [m k] (if (-> m k nil?) (assoc m k (m-defaults k)) m))
          m (keys m-defaults)))



;; END common keys
;;..................................................................................................



;;To get java version
;;Runtime.class.getPackage().getImplementationVersion();
;;(-> Runtime .getClass .getPackage .getImplementationVersion)
;;or
;;(System/getProperty "java.version")


;;**************************************************************************************************
;;* BEGIN database
;;* tag: <database>
;;*
;;* description: База данных
;;*
;;**************************************************************************************************

;; (defdb tarsonis (postgres {:db "tarsonis"
;;                            :user "tarsonis"
;;                            :password "paradox"
;;                            ;; optional keys
;;                            :host "localhost"
;;                            :port "5433"}))

;; END database
;;..................................................................................................

(defn check-transaction-isolation [level]
  (let [r (exec-raw "SELECT current_setting('transaction_isolation')" :results)]
    (cond (empty? r) (throw (Exception. "Невозможно получить сведения о текущем уровне изоляции транзакции"))
          (= level (-> r first :current_setting)) true
          :else (throw (Exception. (str "неправильный уровень изоляции транзакции "
                                        (-> r first :current_setting) " тогда как ожидался " level))))))

(defn check-transaction-isolation-serializable [] (check-transaction-isolation "serializable"))

;; ---------------------------------------

(defn prepare-tip [row]
  (ix/if-empty?-row-or-nil?-val-then-row-else-do :tip
                                                 (fn [t]
                                                   (when (nil? t) (throw (Exception. "tip is nil.")))
                                                   (when-not (keyword? t) (throw (Exception. "tip is not keyword.")))
                                                   (name t))
                                                 row))

(defn transform-tip [row] (ix/if-empty?-row-or-nil?-val-then-row-else-do :tip keyword row))





(declare acc webdoc own ownarl arl ca tip webuser zone op opi opiacc)

;; -------------------------------------------------------------------------------------------------

;;**************************************************************************************************
;;* BEGIN entity owner
;;* tag: <entity own>
;;*
;;* description: Владельци счетов
;;*
;;**************************************************************************************************

(defentity own
  (pk :id)
  (many-to-many arl :ownarl)
  (transform (fn [row] (-> row
                           (dissoc :fts)
                           )))
  )


(defn own-save
  "Сохранить владельца счетов"
  [own-row]
  ;; SAVE
  (ix/com-save-for-id own own-row))

(def own-select* (select* own))


(defn own-pred-search? [select*-1 fts-query]
  (ix/com-pred-full-text-search* select*-1 :fts fts-query))

                                        ;TODO: написать тесты
(defn own-search [query]
  (-> own-select*
      (ix/com-pred-full-text-search* :fts query)
      exec))



;; END entity owner
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN entity arl
;;* tag: <entity arl>
;;*
;;* description: Учетные роли владельцев счетов
;;*
;;**************************************************************************************************

(defentity arl
  (pk :id)
  (many-to-many own :ownarl)

  (prepare (fn [row] (->> row
                          (ix/prepare-as-string :keyname))))

  (transform (fn [row] (->> row
                            (ix/transform-as-keyword :keyname))))
  )


(defn init-arl [keyname title description]
  (ix/com-save-for-field arl :keyname
                         {:keyname (name keyname)
                          :title title
                          :description description}))

(defn arl-find-for-keyname [keyname]
  (first (select arl (where (= :keyname (name keyname))))))

;; В принципе можно закэшировать !!!
(defn arl-list [] (select arl))



;; END entity arl
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN ownarl
;;* tag: <entity ownarl>
;;*
;;* description: связь владельца счета с ролями
;;*
;;**************************************************************************************************

(defentity ownarl
  (belongs-to own)
  (belongs-to arl))

(defn ownarl-add-rel [own-row arl-row]
  ((ix/com-defn-add-rel-many-to-many :id :id ownarl :own_id :arl_id) own-row arl-row))

                                        ;TODO: Надо написать тесты
(defn ownarl-add-rels [{id :id :as own-row} arl-rows]
  (transaction
   (delete ownarl (where (= :own_id id)))
   (doall (map (partial ownarl-add-rel own-row) arl-rows))))

                                        ;TODO: Надо написать тесты
(defn ownarl-add-rels-for-keynames [own-row arl-rows-keynames]
  (->> arl-rows-keynames
       (map #(first (select arl (where (= :keyname (name %))))))
       ix/print-debug->>>
       (ownarl-add-rels own-row)
       transaction))

(defn ownarl-own-has-a-role? [own-row arl-keyname]
  ((ix/com-defn-has-a-rel? arl :id :keyname ownarl :own_id :arl_id) own-row arl-keyname))

(defn ownarl-own-get-rels-set [own-row]
  ((ix/com-defn-get-rels-set arl :id :keyname ownarl :own_id :arl_id) own-row))



;; TODO: написать тесты
(defn own-pred-by-arl?
  [select*-1 arl-row & [not?]]
  ((ix/com-defn-pred-rows-by-rel? :id :id ownarl :own_id :arl_id not?)
   select*-1  arl-row))

;; TODO: написать тесты
(defn own-pred-by-arls?
  [select*-1 arls-rows & [not?]]
  ((ix/com-defn-pred-rows-by-rels? :id :id ownarl :own_id :arl_id not?)
   select*-1 arls-rows))

(defn own-search? [query]
  (-> own-select*
      (ix/com-pred-full-text-search* :fts query)
      exec))




;; (defn arls-for-own [{own-id :id}]
;;   (-> (select* ownarl)
;;       (with arl)
;;       (where (= :own_id own-id))
;;       exec))



;; END ownarl
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN entity type
;;* tag: <entity tip>
;;*
;;* description: Типы
;;*
;;**************************************************************************************************

;;------------------------------------------------------------------------------
;; BEGIN: PROTOCOL AND TYPES
;; tag: <protocol types>
;; description: Протокол калькулируемых типов и их реализаци
;;------------------------------------------------------------------------------

;; ЭТО ВАРИАНТ ТИПОВ С ООП

;; (defprotocol AccType
;;   (add-impl [this this])
;;   (tip [this])
;;   (title [this])
;;   (description [this])
;;   (sql-field [this])
;;   (tip [this])
;;   )

;; (defprotocol AccOp
;;   (add [this this]))

;; (def acc-types-list
;;   [(defrecord BigintAccType [^Long val]
;;      AccType
;;      (tip [this] :bigint)
;;      (title [this] "Целочисленный")
;;      (description [this] "Целочисленный")
;;      (sql-field [this] :val_bigint)

;;      (add-impl [this b]
;;        (let [x (:val this)
;;              y (:val b)]
;;          (BigintAccType. (+ x y))))
;;      )

;;    (defrecord MoneyAccType [^BigDecimal val]
;;      AccType
;;      (tip [this] :money)
;;      (title [this] "Денежный")
;;      (description [this] "Денежный тип")
;;      (sql-field [this] :val_money)

;;      (add-impl [this b]
;;        (let [x (:val this)
;;              y (:val b)]
;;          (MoneyAccType. (+ x y))))
;;      )

;;    (defrecord NumericAccTypeP3 [^BigDecimal val]
;;      AccType
;;      (tip [this] :numeric)
;;      (title [this] "Десятичный (*.3)")
;;      (description [this] "Десятичный с точностью до 3 знака")
;;      (sql-field [this] :val_numeric)

;;      (add-impl [this b]
;;        (let [x (:val this)
;;              y (:val b)]
;;          (NumericAccTypeP3. (+ x y))))
;;      )

;;    (defrecord RatioAccType [^Long num ^Long denum]
;;      AccType
;;      (tip [this] :ratio)
;;      (title [this] "Дробный")
;;      (description [this] "Дробный")
;;      (sql-field [this] :val_ratio)

;;      (add-impl [this b]
;;        (let [{num1 :num } this
;;              {num2 :num denum2 :denum tip :tip} b]
;;          (if (= denum denum2)
;;            (RatioAccType. (+ num1 num2) denum)
;;            (throw (Exception. (str "Not equal denums: " denum " != " denum2 ))))))
;;      )
;;    ])

;; (doall
;;  (map #(extend % AccOp
;;                {:add (fn [a b]
;;                        (println "+")
;;                        (add-impl a b ))}
;;                ) acc-types-list))


;; END PROTOCOL AND TYPES
;;..............................................................................

(declare meta-tips)

;;------------------------------------------------------------------------------
;; BEGIN: constructor and selectors for types
;; tag: <tip constructor selectors>
;; description:
;;------------------------------------------------------------------------------

;; CONSTRUCTORS

(defn ax-cr [tip-tag & val]

  (when (nil? tip-tag)
    (throw
     (Exception. "tip-tag is nil")))

  (when-not (keyword? tip-tag)
    (throw
     (Exception. "tip-tag is not keyword")))

  (let [{{constructor :constructor
          validators :validators} tip-tag :as tip-descr} meta-tips]
    (when (nil? tip-descr)
      (throw
       (Exception. (str "This tip-tag: " tip-tag " is not described."))))

    (when-not (nil? validators)
      (doseq [[f m] validators]
        (when (if (-> val count (= 1)) (-> val first f) (f val))
          (throw (Exception. (str m " val: " val " tip-tag: " tip-tag))))))

    (when (nil? constructor)
      (throw
       (Exception. (str "Constructor is not described for this tag: " tip-tag ))))

    [tip-tag (apply constructor val)]))

;; SELECTORS

(defn ax-tag [[tip-tag _]] tip-tag)
(defn ax-val [[_ val]] val)

;; END constructors and selectors for types
;;..............................................................................


;; OPERATIONS ------------------------------------------------------------------


(defn ax-check [x]

  (when (nil? x) (throw (Exception. "Arg is nil!")))

  (let [tip-tag (ax-tag x)]
    (when (nil? tip-tag)
      (throw (Exception. "tip-tag is nil")))

    (let [{{validators :validators} tip-tag} meta-tips]
      (when-not (nil? validators)
        (let [val (ax-val x)]
          (doseq [[f m] validators]
            (when (f val)
              (throw (Exception. (str m " val: " val " tip-tag: " tip-tag))))))
        true))))


(defn ax-check-pair-and-prep [x y]
  (when (and (ax-check x) (ax-check y))
    (let [x-tip-tag (ax-tag x)
          y-tip-tag (ax-tag y)
          x-val (ax-val x)
          y-val (ax-val y)]
      (when-not (= x-tip-tag y-tip-tag)
        (throw (Exception. (str "Tip is not equals: x-tip-tag: " x-tip-tag ", y-tip-tag: " y-tip-tag ))))
      [x-tip-tag x-val y-val])))


;;------------------------------------------------------------------------------
;; BEGIN: Arifmetic operations of types
;; tag: <tip op math>
;; description: Арифметические операции над типами
;;------------------------------------------------------------------------------

(defn ax-cr-<-rez- [tip-tag r]
  (if (coll? r)
    (apply ax-cr (reduce conj [tip-tag] r))
    (ax-cr tip-tag r)))

(defn ax-add [x y]
  (let [[x-tip-tag x-val y-val] (ax-check-pair-and-prep x y)
        fn-add (-> meta-tips x-tip-tag :op+)]
    ;; Calculation
    (ax-cr-<-rez- x-tip-tag (fn-add x-val y-val))))

                                        ;TODO: make tests

(defn ax-mul [x k]
  (when (ax-check x)
    (let [[x-tip-tag x-val] x
          fn-mul (-> meta-tips x-tip-tag :op*)]
      ;; Calculation
      (ax-cr-<-rez- x-tip-tag (fn-mul x-val k)))))

                                        ;TODO: make tests

(defn ax-div [x y]
  (let [[x-tip-tag x-val y-val] (ax-check-pair-and-prep x y)
        fn-div (-> meta-tips x-tip-tag :op_)]
    ;; Calculation
    (fn-div x-val y-val)))

(defn ax-neg [x]
  (when (ax-check x)
    (let [tip-tag (ax-tag x)]
      (->> (ax-val x)
           ((-> meta-tips tip-tag :op-neg)) ;; Обязательно с двойными скобками! ((-> .... ))
           (ax-cr-<-rez- tip-tag)))))

(defn ax-zero [x]
  (when (ax-check x)
    (ax-add x (ax-neg x))))

(defn ax-sub [x y]
  (ax-add x (ax-neg y)))


(def ax-opk->op {:ax-cr-<-rez- ax-cr-<-rez-
                 :ax-add ax-add
                 :ax-sub ax-sub
                 :ax-mul ax-mul
                 :ax-div ax-div
                 :ax-neg ax-neg
                 :ax-zero ax-zero
                 })


                                        ;TODO: make tests
(defn ax-calc-symbolic-expression [expr]
  (if (or (vector? expr) (not (seq? expr)))
    expr
    (apply
     (-> expr first ax-opk->op)
     (map ax-calc-symbolic-expression (rest expr)))))



;; END Arifmetic operations of types
;;..............................................................................


;;------------------------------------------------------------------------------
;; BEGIN: Compare types
;; tag: <cmp tip>
;; description: Сравнение типов
;;------------------------------------------------------------------------------

(defn ax-cmp [x y]
  (let [[x-tip-tag x-val y-val] (ax-check-pair-and-prep x y)
        {fn-< :< fn-= := fn-> :>} (-> meta-tips x-tip-tag :op-cmps)]

    ;; Comparation
    (cond (fn-< x-val y-val) -1
          (fn-= x-val y-val)  0
          (fn-> x-val y-val)  1)
    ))

(defn ax< [x y] (= (ax-cmp x y) -1))
(defn ax= [x y] (= (ax-cmp x y)  0))
(defn ax> [x y] (= (ax-cmp x y)  1))

(defn ax<= [x y] (or (ax< x y) (ax= x y)))
(defn ax>= [x y] (or (ax> x y) (ax= x y)))


;; END Compare types
;;..............................................................................

(defentity tip
  (pk :id))

;; ----------------------
(defn smpl-trans [sql-field]
  [;; to-row
   (fn [{val sql-field t :tip :as row}]
     (assoc row :val (ax-cr t val)) )
   ;; to-sql
   (fn [{val :val :as row}]
     (assoc row sql-field (ax-val val)))
   ])

;; selectors
(defn get-to-val-fn [[f _]] f)
(defn get-to-sql-fn [[_ f]] f)

;; ----------------------

(defn is-not-number? [n]
  (not (number? n)))

(defn is-has-a-fractional-part? [n]
  (let [fp (rem n 1)]
    (not (== 0 fp))))

;;-----------------------

(def meta-tips {})
(def meta-tips-sql-fields [])
(def meta-tips-trans-functions-map {})

(defn deftip [{tip-k :tip
               fns-trans :fns-trans
               [sql-field] :sql-field
               :as tip-descr-map}]

  (def meta-tips
    (-> meta-tips
        (assoc tip-k
               (assoc tip-descr-map :fns-trans (or fns-trans (smpl-trans sql-field))))))

  (def meta-tips-sql-fields
    (reduce #(-> %2 :sql-field (into %)) [] (vals meta-tips)))

  (def meta-tips-trans-functions-map
    (reduce #(assoc %1 %2 (-> meta-tips %2 :fns-trans)) {} (keys meta-tips))))



(deftip {:tip :bigint
         :title "Целочисленный"
         :sql-field [:val_bigint]
         :validators [[nil? "Пустое значение"]
                      [is-not-number? "Нечисловой тип"]
                      [is-has-a-fractional-part? "Нецелочисленный тип, возможно потеять копейки"]]
         :constructor long
         :op+ #(long (+ %1 %2))
         :op* #(long (* %1 %2))
         :op_ #(long (/ %1 %2))
         :op-cmps {:< < := = :> >}
         :op-neg #(* % -1)
         :description "Целочисленный"
         :default-value 0
         })

(deftip {:tip :money
         :title "Денежный"
         :sql-field [:val_money]
         :validators [[nil? "Пустое значение"]
                      [is-not-number? "Нечисловой тип"]]
         :constructor bigdec
         :op+ #(-> (+ %1 %2) bigdec (.setScale 2 java.math.RoundingMode/HALF_UP))
         :op* #(-> (* %1 %2) bigdec (.setScale 2 java.math.RoundingMode/HALF_UP))
         :op_ #(.divide (bigdec %1) (bigdec %2) 2 java.math.RoundingMode/HALF_UP)
         :op-cmps {:< < := = :> >}
         :op-neg #(* % -1)
         :description "Денежный"
         :default-value 0.00M
         })

(deftip {:tip :numeric
         :title "Десятичный (*.3)"
         :sql-field [:val_numeric]
         :validators [[nil? "Пустое значение"]
                      [is-not-number? "Нечисловой тип"]]
         :constructor bigdec
         :op+ #(-> (+ %1 %2) bigdec (.setScale 3 java.math.RoundingMode/HALF_UP))
         :op* #(-> (* %1 %2) bigdec (.setScale 3 java.math.RoundingMode/HALF_UP))
         :op_ #(.divide (bigdec %1) (bigdec %2) 3 java.math.RoundingMode/HALF_UP)
         :op-cmps {:< < := = :> >}
         :op-neg #(* % -1)
         :description "Десятичный с точностью до 3 знака"
         :default-value 0.000M
         })

(deftip
  (letfn [(check-ratio-pair [x-num x-denum y-num y-denum]
            (when (or (= 0 x-denum) (= 0 y-denum))
              (throw (Exception. (str "Denum is 0: x-denum: " x-denum ", y-denum: " y-denum ))))

            (when (or (nil? x-denum) (nil? y-denum))
              (throw (Exception. (str "Denum is nil: x-denum: " x-denum ", y-denum: " y-denum ))))

            (when-not (= x-denum y-denum)
              (throw (Exception. (str "Denums is not equal: x-denum: " x-denum ", y-denum: " y-denum ))))

            (when (or (nil? x-num) (nil? y-num))
              (throw (Exception. (str "Num is nil: x-num: " x-num ", y-num: " y-num ))))

            true)]
    {:tip :ratio
     :title "Дробный"
     :sql-field [:val_ratio_num :val_ratio_denum]

     :get-default-val-from-webdoc-fn
     (comp ax-zero :val)
;;;#(ax-cr :ratio 0 (:val_ratio_denum %)) ;; !!!! OLD VARIANT - возможно теперь атавизм, надо убрать

     :validators [[nil? "Пустое значение"]
                  [empty? "Пустое значение"]
                  [#(not= 2 (count %)) "Неверное число аргументов"]
                  [(fn [[n d]] (or (nil? n) (nil? d))) "Пустое значение"]
                  [(fn [[n d]] (or (is-not-number? n) (is-not-number? d))) "Нечисловой тип"]
                  [(fn [[_ d]] (neg? d)) "Отрицательный знаменатель"]
                  [(fn [[_ d]] (== 0 d)) "Знаменатель равен 0"]
                  [(fn [[n d]] (or (is-has-a-fractional-part? n)
                                   (is-has-a-fractional-part? d))) "Обнаруженно число с остатком"]
                  ]

     :constructor (fn [n d] [(long n) (long d)])

     :op-cmps {:< (fn [[x xd] [y yd]] (when (check-ratio-pair x xd y yd) (< x y)))
               := (fn [[x xd] [y yd]] (when (check-ratio-pair x xd y yd) (= x y)))
               :> (fn [[x xd] [y yd]] (when (check-ratio-pair x xd y yd) (> x y)))}

     :op+ (fn [[x xd] [y yd]]
            (when (check-ratio-pair x xd y yd)
              [(long (+ x y)) xd]))

     :op* (fn [[x xd] k]
            [(long (* x k)) xd])

     :op_ (fn [[x xd] [y yd]]
            (when (check-ratio-pair x xd y yd)
              (long (/ x y))))

     :op-neg (fn [[n d]] [(* -1 n) d])

     :fns-trans [
                 (fn [{t :tip val-num :val_ratio_num val-denum :val_ratio_denum :as row}]
                   (assoc row :val (ax-cr t val-num val-denum)))

                 (fn [{v :val :as row}]
                   (let [[val-num val-denum] (ax-val v)]
                     (assoc row :val_ratio_num val-num :val_ratio_denum val-denum)))
                 ]
     :description "Дробный"}))



(declare prepare-tip transform-tip)

(defn prepare-fns-trans-for-tip-val [row]
  (-> row
      (as-> row
          (let [{t :tip} row]
            (if (nil? t) (dissoc row :tip)
                ((-> meta-tips-trans-functions-map t get-to-sql-fn) row))))
      (dissoc :val)
      prepare-tip))

(defn transform-fns-trans-for-tip-val [row]
  (-> row
      transform-tip
      (as-> row
          (let [{t :tip} row]
            (if (nil? t) row
                ((-> meta-tips-trans-functions-map t get-to-val-fn) row))))

      (#(reduce dissoc % meta-tips-sql-fields))))

;; Инициализация типов
(defn install-tips []
  (->> meta-tips
       vals
       (map #(select-keys % [:tip :title :description]))
       (map #(update-in % [:tip] name))
       (map #(ix/com-save-for-field tip :tip %))
       doall))

;; STANDART!
;; Способ инициализации
;;(install-tips)

;; -------------------------------------------------------------------------
;; END entity type
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN entity zone
;;* tag: <entity zone>
;;*
;;* description: Зоны учета
;;*
;;**************************************************************************************************

(defentity zone
  (pk :id)

  (prepare (fn [row] (->> row
                          (ix/prepare-as-string :keyname))))

  (transform (fn [row] (->> row
                            (ix/transform-as-keyword :keyname))))
  )

(defn zone-save
  "Сохранить зону"
  [zone-row]
  ;; SAVE
  (ix/com-save-for-field zone :keyname zone-row))

;; END entity zone
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN entity counted attribute
;;* tag: <entity ca>
;;*
;;* description: Атрибуты счета
;;*
;;**************************************************************************************************


(defentity ca
  (pk :id)

  (prepare (fn [row] (->> row
                          prepare-tip
                          ;;print-row
                          (ix/prepare-as-string :keyname))))

  (transform (fn [row] (->> row
                            transform-tip
                            (ix/transform-as-keyword :keyname))))
  )


(defn ca-save
  "Сохранить счетный атрибут"
  [ca-row]
  (transaction
   ;; SPECIFIC VALIDATION
   (let [{id :id parent-id :parent_id keyname :keyname title :title} ca-row]
     (when-not (empty? (select ca (where (and (not (= :id id)) (= :parent_id parent-id) (= :keyname (name keyname))))))
       (throw (Exception. (str "Запись а :keyname = " keyname " уже существует."))))
     (when-not (empty? (select ca (where (and (not (= :id id)) (= :parent_id parent-id) (= :title title)))))
       (throw (Exception. (str "Запись а :title = " title " уже существует.")))))



   (let [{id :id parent-id :parent_id root-id :root_id t :tip
          :as row} (->> ca-row
                        ;; VALIDATION
                        ;; :tip - проверяется в prepare
                        ;; :keyname nil? - проверяется в prepare
                        (ix/check-row-field :keyname nil? ":keyname не должен быть пустым.")
                        (ix/check-row-field :keyname #(-> % keyword? not) ":keyname должен иметь тип keyword.")
                        ;; :title nil? - проверяется в базе

                        ;; Пусть :root_id обновляется автоматом!
                        ;; (check-row #(and (-> % :id nil? not)
                        ;;                  (-> % :parent_id nil?)
                        ;;                  (not= (:id %) (:root_id %)))
                        ;;            "У основного счета значения по :id и :root_id должны совпадать при обновлении!")

                        ;; SAVE
                        (ix/com-save-for-id ca))]
     (if (and (not (nil? id))
              (nil? parent-id)
              (not= id root-id)) (ix/com-save-for-id ca {:id id :tip t :root_id id})
              row)
     )))



(def ca-standart-coll
  "Создает колекцию счетных атрибутов из типов"
  (->> meta-tips
       keys
       (reduce #(let [{{description :description title :title} %2} meta-tips
                      keyname (keyword (str "standart-" (name %2)))]
                  (assoc % keyname {:tip %2 :title title :description description}))
               {})
       ))


;; Переименовать
(defn create-ca-as-list
  "Сохранение из структуры описания"
  [cas & [tip root-id parent-id]]
  (doseq [keyname (keys cas)
          :let [{title :title description :description sub :sub :as row} (keyname cas)
                tip (or tip (:tip row))
                pre-row (first (select ca (where (and
                                                  (= :parent_id parent-id)
                                                  (= :keyname (name keyname))))))
                {id :id} (ca-save (merge pre-row {:parent_id parent-id
                                                  :root_id root-id
                                                  :keyname keyname
                                                  :title title
                                                  :tip tip
                                                  :description description}))]]
    (when-not (empty? sub)
      (create-ca-as-list sub tip (or root-id id) id))))

;; STANDART!
;; Способ инициализации
;; (create-ca-as-list ca-standart-coll)
;;


(defn ca-get-for-id
  "Получение атрибута и субатрибутов по id"
  [id]
  (transaction
   (let [ca-list (select ca (where (and (= :id id) (= :parent_id nil))))]
     (if (empty? ca-list) nil
         (let [root-ca (first ca-list)
               {id :id} root-ca]
           (->> (select ca (where (= :root_id id)))
                (ix/sort-tree-as-flat :id :parent_id)
                ))))))


(defn ca-get [keyname]
  (first (select ca (where (= :keyname (name keyname))))))

(defn ca-get-tip-for-id [id]
  (->> id (ix/com-find ca) :tip))

(defn print-ca
  "Вывод атрибутов в виде таблици"
  [ca]
  (println "\nPRINT COUNTED ATTRIBUTES:")
  (print-table ca)
  (println "\nEND PRINT")
  ca)


(def ca-list* (-> (select* ca) (where (= :parent_id nil))))


(defn ca-main-list []
  (-> ca-list* exec))


;; END
;;..............................................................................

;;**************************************************************************************************
;;* BEGIN entity counted object
;;* tag: <entity webdoc>
;;*
;;* description: Считаемые объекты
;;*
;;**************************************************************************************************

(def webdoc
  (-> ix/webdoc
      (prepare (fn [row] (-> row
                             prepare-fns-trans-for-tip-val
                             )))

      (transform (fn [row] (-> row
                               transform-fns-trans-for-tip-val
                               (dissoc :fts)
                               )))
      ))

(defn webdoc-save
  "Сохранение webdoc"
  [webdoc-row]
  (ix/com-save-for-id webdoc webdoc-row))

(declare swopv swopva)

(defn webdoc-delete [{id :id :as webdoc-row}]
  (transaction
   (do
     (delete swopva (where (= :swebdoc_id id)))
     (delete swopv (where (= :swebdoc_id id)))
     (ix/webdoc-delete webdoc-row))))


                                        ;TODO: написать тест
(defn webdoc-save-by-scod
  "Сохранение/обновление обьекта webdoc по scod"
  [webdoc-row]
  (ix/com-save-for-field webdoc :scod webdoc-row))

(def webdoc-select* (select* webdoc))

(defn webdoc-standart-fields* [webdoc-select*]
  (-> webdoc-select*
      (fields :id
              :keyname
              :description
              :tip
              :ca_id
              :val_bigint
              :val_money
              :val_numeric
              :val_ratio_num
              :val_ratio_denum
              :scod
              :cdate
              :udate
              :web_title
              :web_meta_subject
              :web_meta_keywords
              :web_meta_description
              :web_top_description
              :web_citems
              :web_description
              :web_title_image
              :url1
              :url1flag
              :ttitle
              )))


(defn webdoc-pred-product? [select*-1]
  (where select*-1
         (= :is_product true)))

(defn webdoc-pred-product-tips? [select*-1 tips]
  (where select*-1 {:tip [in (map name tips)]}))


                                        ;TODO: написать тесты
(defn webdoc-search [query]
  (-> webdoc-select*
      (ix/com-pred-full-text-search* :fts query)
      exec))


;; END entity counted object
;;..................................................................................................



;;**************************************************************************************************
;;* BEGIN entity account
;;* tag: <entity acc>
;;*
;;* description: Счета
;;*
;;**************************************************************************************************


(defentity acc
  (pk :id)
  (prepare prepare-fns-trans-for-tip-val)
  (transform transform-fns-trans-for-tip-val))

;; TODO: create tests!
(defn accca-cr
  "конструктор счетового контейнера"
  [row-acc row-ca] [row-acc row-ca])

;; TODO: написать тесты
(defn accca-get-acc [[row-acc _     ]] row-acc)
(defn accca-get-ca  [[_       row-ca]] row-ca)

(defn accca-do-val [[row-acc row-ca] f] [(assoc row-acc :val (-> row-acc :val f)) row-ca])
(defn accca-ax-add-val [accca val] (accca-do-val accca (partial ax-add val)))

(defn accca-get-acc-val [accca] (-> accca accca-get-acc :val))
(defn accca-set-acc-val [accca val] (accca-do-val accca (fn [_] val)))


(defn acccas-find [{zone-id :id :as zone-row}
                   {own-id :id :as own-row}
                   {webdoc-id :id tip :tip ca-root-id :ca_id :as webdoc-row}]
  ;;(println "own-id:" own-id " webdoc-id:" webdoc-id)
  (transaction
   (letfn [(create-acc [{ca-id :id ca-root-id :root_id ca-parent-id :parent_id tip :tip}]
             (insert acc (values {:version_id 0 ;;!пусть только что созданный счет имеет нулевую версию!
                                  :own_id own-id
                                  :zone_id zone-id
                                  :webdoc_id webdoc-id
                                  :tip tip
                                  :ca_id ca-id
                                  :ca_root_id ca-root-id
                                  :ca_parent_id ca-parent-id
                                  :val (let [m (-> meta-tips tip)]
                                         (cond (-> m :get-default-val-from-webdoc-fn nil? not) ((m :get-default-val-from-webdoc-fn) webdoc-row)
                                               (-> m :default-value nil? not) (ax-cr tip (m :default-value))
                                               :else (ax-cr tip nil)))
                                  }
                                 )))]

     (let [ca-list (ca-get-for-id ca-root-id)
           acc-list (select acc (where (and
                                        (= :own_id own-id)
                                        (= :zone_id zone-id)
                                        (= :webdoc_id webdoc-id)
                                        (= :ca_root_id ca-root-id)
                                        )))]
       (->> (for [{id :id :as row-ca} ca-list
                  :let [row-acc (->> acc-list
                                     (filter #(-> % :ca_id (= id)))
                                     first)]]
              [row-acc row-ca])

            (map (fn [[row-acc row-ca]]
                   (accca-cr
                    (if (nil? row-acc) (create-acc row-ca) row-acc)
                    row-ca)))
            )
       ))))

;; Переименовать функцию в accca-find-root!!!
(defn acccas-find-root [{zone-id :id :as zone-row}
                        {own-id :id :as own-row}
                        {webdoc-id :id ca-root-id :ca_id :as webdoc-row}]
  (transaction
   (->> (select acc (where (and
                            (= :zone_id zone-id)
                            (= :own_id own-id)
                            (= :webdoc_id webdoc-id)
                            (= :ca_parent_id nil))))
        (map (fn [{ca-id :ca_id :as acc-row}]
               (accca-cr acc-row (first (select ca (where (and (= :id ca-id) (= :parent_id nil))))))))
        doall
        first)))


(defn print-cas [accca]
  (println "\nPRINT COUNTED ATTRIBUTES:")
  (->> accca (map accca-get-ca) print-table)
  (println "\nEND PRINT")
  accca)

(defn print-accs [accca]
  (println "\nPRINT ACCOUNTS:")
  (->> accca (map accca-get-acc) print-table)
  (println "\nEND PRINT")
  accca)


(defn acccas-check
  "Проверка правильности структуры счетов и их значений"
  [acccas]

  (when (empty? acccas) (throw (Exception. "Ненайдено записей для сохранения")))

  (letfn [(check-acc [acc-list {ca-id :ca_id val :val t :tip :as acc-row} x]
            (let [sub-acc-list (filter #(-> % :ca_parent_id (= ca-id)) acc-list)]
              (cond (empty? sub-acc-list) true
                    (= x 5) (throw (Exception. "переполнение вложенности счета"))
                    :else (let [sub-sum (->> sub-acc-list (map :val) (reduce ax-add))]
                            (if (not= val sub-sum) (throw
                                                    (Exception.
                                                     (str "Сумма счета не ровна сумме подсчетов: "
                                                          val " != " sub-sum
                                                          ;;"\n    счет: " acc-row
                                                          ;;"\nподсчета: " sub-acc-list
                                                          )))
                                (->> sub-acc-list
                                     (map #(check-acc acc-list %1 (inc x)))
                                     (reduce #(and %1 %2) true)))
                            )
                    )))]
    (let [acc-list (map accca-get-acc acccas)
          root-acc (filter #(-> % :ca_parent_id nil?) acc-list)]

      (when (empty? root-acc) (throw (Exception. "нет корневого элемента")))
      (when (< 1 (count root-acc)) (throw (Exception. "корневых элементов больше одного" )))

      (let [[{virt? :virt ca-id :ca_id ca-root-id :ca_root_id t :tip v :val}] root-acc]
        (when-not (= ca-root-id ca-id) (throw (Exception. "у корня найдены несовпадающие ca-root-id")))
        (doseq [accca acccas
                :let [{t1 :tip ca-root-id-1 :ca_root_id ca-id-1 :ca_id ca-parent-id-1 :ca_parent_id
                       virt?-1 :virt v1 :val} (accca-get-acc accca)
                       {t2 :tip ca-root-id-2 :root_id ca-id-2 :id ca-parent-id-2 :parent_id} (accca-get-ca accca)]]
          (when (nil? (meta-tips t1)) (throw (Exception. "найдены несуществующие типы")))
          (when-not (= t t1 t2) (throw (Exception. "найдены несовпадающие типы")))

          (when-not (= t (ax-tag v1)) (throw (Exception. "несовпадающие типы")))
          (when-not (= ca-id-1 ca-id-2) (throw (Exception. "найдены несовпадающие ca-id")))
          (when-not (= ca-root-id ca-root-id-1 ca-root-id-2) (throw (Exception. "найдены несовпадающие ca-root-id")))
          (when-not (= ca-parent-id-1 ca-parent-id-2) (throw (Exception. "найдены несовпадающие ca-parent-id")))
          ))


      ;; TODO: написать тест
      (let [s (reduce #(conj %1 (%2 :ca_id)) #{nil} acc-list)]
        (doseq [{parent-id :ca_parent_id} acc-list]
          (when-not (contains? s parent-id) (throw (Exception. "разорванное дерево!")))))

      (check-acc acc-list (first root-acc) 0)
      )))

(defn acccas-save
  "Сохранить счет"
  [acccas]
  (transaction
   (check-transaction-isolation-serializable)
   (let [acccas-new (ix/sort-tree-as-flat #(-> % accca-get-acc :ca_id) #(-> % accca-get-acc :ca_parent_id) acccas)
         {zone-id :zone_id own-id :own_id webdoc-id :webdoc_id} (-> acccas-new first accca-get-acc)
         zone-row (ix/com-find zone zone-id)
         own-row  (ix/com-find own own-id)
         webdoc-row (ix/com-find webdoc webdoc-id)]
     (->> (acccas-find zone-row own-row webdoc-row)

          ;; Надо непременно мержить!
          (map (fn [accca-old]
                 (let [ca-id-old (-> accca-old accca-get-acc :ca_id)
                       accca-new (->> acccas-new (filter #(-> % accca-get-acc :ca_id (= ca-id-old))) first)]
                   (if (nil? accca-new) [false accca-old]
                       (let [val-old (accca-get-acc-val accca-old)
                             val-new (accca-get-acc-val accca-new)]
                         (if (= val-old val-new) [false accca-old]
                             [true (accca-set-acc-val accca-old val-new)]))))))

          (#(when (->> % (map second) acccas-check) %)) ;; проверка
          (filter first)   ;; фильтруем только то что изменилось
          (map second)     ;; получаем accca
          (map (fn [accca] ;; сохранение
                 (accca-cr
                  (-> accca accca-get-acc (update-in [:version_id] inc) ((partial ix/com-save-for-id acc)))
                  (accca-get-ca accca))))
          doall))))

;; END entity acount
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN opttype entity
;;* tag: <opttype entity>
;;*
;;* description: общий тип операций
;;*
;;**************************************************************************************************


(defentity opttype
  (pk :id)
  (prepare (fn [row] (->> row (ix/prepare-as-string :opttype))))
  (transform (fn [row] (->> row (ix/transform-as-keyword :opttype)))))


(defn opttype-save [row] (ix/com-save-for-id opttype row))

;; END opttype entity
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN opt entity
;;* tag: <opt entity>
;;*
;;* description: Типы операций
;;*
;;**************************************************************************************************

(defentity opt
  (pk :id)
  (prepare (fn [row] (->> row
                          (ix/prepare-as-string :opttype)
                          (ix/prepare-as-string :opt))))
  (transform (fn [row] (->> row
                            (ix/transform-as-keyword :opttype)
                            (ix/transform-as-keyword :opt)))))

(defn opt-save [row] (ix/com-save-for-id opt row))

;; END opt entity
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN op entity
;;* tag: <op entity>
;;*
;;* description: Операций
;;*
;;**************************************************************************************************

(defentity op
  (pk :id)

  (prepare (fn [row] (->> row
                          (ix/prepare-as-string :opt)
                          (ix/prepare-as-string :opttype)
                          (ix/prepare-clj-time-as-sql-time :cdate))))

  (transform (fn [row] (->> row
                            (ix/transform-as-keyword :opttype)
                            (ix/transform-as-keyword :opt)
                            (ix/transform-sql-time-as-clj-time :cdate))))
  )

(defn op-cr [{webuser-id :id :as webuser} opt-k]
  (ix/com-save-for-id op {:webuser_id webuser-id
                          :opt opt-k
                          :opttype (-> (select opt
                                               (fields :opttype)
                                               (where (= :opt (name opt-k))))
                                       first
                                       :opttype)
                          :cdate (tl/local-now)}))

;; END op entity
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN opi entity
;;* tag: <opi entity>
;;*
;;* description: история операций
;;*
;;**************************************************************************************************

(defentity opi
  (pk :id)

  (prepare (fn [row]
             (-> row
                 ((partial ix/prepare-as-string :opt))
                 ((partial ix/prepare-as-string :opttype))
                 prepare-fns-trans-for-tip-val)))

  (transform (fn [row]
               (-> row
                   ((partial ix/transform-as-keyword :opt))
                   ((partial ix/transform-as-keyword :opttype))
                   transform-fns-trans-for-tip-val)))
  )

(defn save-opi-description [opi-row]
  (ix/com-save-for-id opi (select-keys opi-row [:id :description])))


;; END opi entity
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN opiacc
;;* tag: <entity opiacc>
;;*
;;* description: Сущность сохранения состояний счетов после операций
;;*
;;**************************************************************************************************

(defentity opiacc
  (pk :id)
  (prepare prepare-fns-trans-for-tip-val)
  (transform transform-fns-trans-for-tip-val))

;; END opiacc
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN box and items
;;* tag: <box>
;;*
;;* description: определение коробки перемещения
;;*
;;**************************************************************************************************

;; TODO: написать тесты
(defn op-item-cr [accca] [(accca-get-ca accca) (-> accca accca-get-acc-val ax-zero)])
(defn op-item-get-ca  [[o _]] o)
(defn op-item-get-val [[_ o]] o)

(defn op-item-do-val   [[ca val] fun] [ca (fun val)])
;;--------------------
;;;(defn op-item-ax-add-val [val op-item] (op-item-do-val (partial ax-add val) op-item))
(defn op-item-set-val  [op-item val] (op-item-do-val op-item (fn [_] val)))
(defn op-item-ax-neg-val [op-item]     (op-item-do-val op-item ax-neg))
;;-------------------





(defn box-cr [from-zone-row from-own-row to-zone-row to-own-row webdoc-row]
  [from-zone-row
   from-own-row
   to-zone-row
   to-own-row
   webdoc-row
   (map op-item-cr (acccas-find from-zone-row from-own-row webdoc-row))])
;; selectors --------
(defn box-get-from-zone   [[o _ _ _ _ _]] o)
(defn box-get-from-own    [[_ o _ _ _ _]] o)
(defn box-get-to-zone     [[_ _ o _ _ _]] o)
(defn box-get-to-own      [[_ _ _ o _ _]] o)
(defn box-get-webdoc      [[_ _ _ _ o _]] o)
(defn box-get-op-items    [[_ _ _ _ _ o]] o)

(defn box-do-on-op-items  [[fz fo tz to c i] do-fn]
  [fz fo tz to c (do-fn i)])

(defn box-do-on-each-op-item [box do-fn]
  (box-do-on-op-items box (partial map do-fn)))
;;-------------------
(defn box-do-op-item-for-ca [box {ca-id :id} do-fn]
  (box-do-on-each-op-item box #(if (-> % op-item-get-ca :id (= ca-id)) (do-fn %) %)))

(defn box-set-op-item-val-for-ca [box ca val]
  (box-do-op-item-for-ca box ca #(op-item-set-val % val)))
;;-------------------


(defn box-get-op-item-for-idx [box idx]
  (-> box box-get-op-items vec (nth idx)))

(defn box-get-op-item-ca-for-idx [box idx]
  (-> box (box-get-op-item-for-idx idx) op-item-get-ca))

;;-------------------
;; Используется пока только в тестах
(defn box-fill-op-item-vals-for-idxs [box flist]
  (reduce (fn [box [idx val]]
            (box-set-op-item-val-for-ca box (box-get-op-item-ca-for-idx box idx) val))
          box flist))







(defn move [{op-id :id
             opt-k :opt
             opttype-k :opttype
             :as op-row}
            box]
  (transaction
   (check-transaction-isolation-serializable)

   (let [from-zone-row  (box-get-from-zone box)
         from-own-row   (box-get-from-own  box)
         to-zone-row    (box-get-to-zone   box)
         to-own-row     (box-get-to-own    box)
         webdoc-row     (box-get-webdoc      box)
         ;; Создаем карту items по :ca_id как :id
         map-items-for-ca-id (->> box
                                  box-get-op-items
                                  (reduce #(assoc %
                                                  (-> %2 op-item-get-ca :id)
                                                  (-> %2 op-item-get-val)) {}))

         ;; поднимаем текущие счета
         from-acccas (acccas-find from-zone-row from-own-row webdoc-row)
         to-acccas   (acccas-find to-zone-row   to-own-row   webdoc-row)]


     ;; ДЛЯ ОТЛАДКИ
     #_(println

        "из зоны: " from-zone-row "\n"
        "от владельца: " from-own-row "\n"
        "в зону " to-zone-row "\n"
        "к владельцу " to-own-row "\n"
        "товар " webdoc-row "\n"

        "Содержимое коробки\n"
        (map #(str "\t" % "\n") (box-get-op-items box)) "\n"

        "количество " map-items-for-ca-id "\n"

        )
     ;; ДЛЯ ОТЛАДКИ


     (letfn [(acccas-ax-add [acccas fn-on-items-val]
               (->> acccas
                    ;; математика - сложение значения счета с результатом приминения функции
                    ;; также формируeм структуру, содержащую новый счет и результат приминения функции
                    ;; который понадобится потом при сохранении в opi
                    (reduce (fn [[acccas-new map-v-ca-id] accca]
                              (let [ca-id (-> accca accca-get-ca :id)
                                    v     (-> ca-id map-items-for-ca-id fn-on-items-val)]
                                [(conj acccas-new (accca-ax-add-val accca v))
                                 (assoc map-v-ca-id ca-id v)]))
                            [[] {}])
                    ;; сохраняем новые варианты счетов
                    ((fn [[acccas-new map-v-ca-id]] [(acccas-save acccas-new) map-v-ca-id]))
                    ;; формируем историю opi и сохраняем ее
                    ((fn [[acccas-saved map-v-ca-id]]
                       (letfn [(rename-key [m from-k to-k] (-> m (dissoc from-k) (assoc to-k (m from-k))))]
                         (->> acccas-saved
                              (map #(let [{ca-id :ca_id :as acc-row} (accca-get-acc %)]
                                      (assoc acc-row :val (map-v-ca-id ca-id))))
                              (map #(select-keys % [:id :version_id :zone_id :own_id :webdoc_id :ca_id :ca_root_id :ca_parent_id :tip :val]))
                              (map #(-> %
                                        (rename-key :version_id :acc_version_id)
                                        (rename-key :id :acc_id)))
                              (map #(assoc % :op_id op-id :opt opt-k :opttype opttype-k))
                              (map #(insert opi (values %)))
                              doall
                              ;; сохраняем состояние счета в таблицу opiacc
                              (reduce #(assoc %1 (%2 :acc_id) (%2 :id)) {})
                              ((fn [map-acc-id-<-opi-id]
                                 (->> acccas-saved
                                      (map accca-get-acc)
                                      (map #(select-keys % [:id :version_id :tip :val]))
                                      (map #(assoc % :opi_id (-> % :id map-acc-id-<-opi-id)))
                                      (map #(insert opiacc (values %)))
                                      ;;checking!
                                      (map (fn [a b]
                                             (if (= (select-keys (accca-get-acc a) [:id :version_id :tip :val])
                                                    (select-keys b [:id :version_id :tip :val])) true
                                                    (throw (Exception. "несовпадающее состояние acc и opiacc, сбой работы операции"))))
                                           acccas-saved)
                                      doall
                                      (reduce #(and %1 %2)))))
                              ))))
                    ))]
       ;; из одного счета вычитаем и столько же прибавляем в другой
       (and
        (acccas-ax-add from-acccas (fn [v] (ax-neg v)))
        (acccas-ax-add to-acccas   (fn [v] v)))
       ))))

;; END box and items
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN owner roles
;;* tag: <ownarl>
;;*
;;* description: Роли учета
;;*
;;**************************************************************************************************

(defn arole [keyname title description] (:keyname (init-arl keyname title description)))

;; END owner roles
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN static owns
;;* tag: <static owns>
;;*
;;* description: статические владельци счетов
;;*
;;**************************************************************************************************

(defn static-own [own-row aroles]
  (let [r (ix/com-save-for-field own :keyname own-row)
        {id :id keyname :keyname :as own-row} r]
    (println r)
    (delete ownarl (where (= :own_id id)))
    (doseq [arole-k aroles
            :let [arl-row (arl-find-for-keyname arole-k)]]
      (ownarl-add-rel own-row arl-row))
    own-row))

;; END static owns
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Acconting zones
;;* tag: <acc zones>
;;*
;;* description: Описание зон учета
;;*
;;**************************************************************************************************

;; keywords ------------------------------

(def key-scheme-version     :version)
(def key-path-set           :path-set)
(def key-troles-set         :troles-set)
(def key-aroles-set         :aroles-set)
(def key-rules              :rules)
(def key-validators         :validators)
(def key-rules-for-operatio :rules-for-operatio)
(def key-azones-set         :azones-set)
(def key-azones             :azones)
(def key-map-id-keyname     :map-id-keyname)
(def key-azone-sql-prefix   :sql-prefix)

;; scheme constructor --------------------

(def main-scheme-clean {key-azones {} key-map-id-keyname {}})
(def main-scheme main-scheme-clean)

(defn main-scheme-new [] (def main-scheme main-scheme-clean))

;; zone ----------------------------------

;; TODO: сделать валидацию, написать тесты
(defn azone [d-map]
  (when (check-req-keys-in-map [key-keyname
                                key-title
                                key-azone-sql-prefix
                                key-path-set] d-map)
    (let [d-map      (on-map-if-nil?-set-default-vals d-map {key-rules []
                                                             key-description "нет описания"
                                                             key-aroles-set #{}})
          keyname    (key-keyname d-map)
          keyname-k  (keyword keyname)]

      (when-not (-> main-scheme key-azones keyname-k nil?)
        (throw (Exception. (str "Zone " keyname-k " is predefined!"))))

      (def main-scheme (-> d-map
                           ;; DO SQL
                           (select-keys [key-keyname key-title key-description])
                           (update-in [key-keyname] name)
                           ((partial ix/com-save-for-field zone :keyname))
                           ;; DO META
                           ((partial merge d-map))
                           (update-in [key-path-set] #(conj % keyname-k))
                           ;; ADD META TO SCHEME
                           (as-> row
                               (-> main-scheme
                                   (assoc-in [key-azones keyname-k] row)
                                   (assoc-in [key-map-id-keyname (row :id)] keyname-k)))))
      keyname-k)))

(defn has-in [& parent-azones]
  (let [not-defined-keys (vec (filter #(-> main-scheme key-azones % nil?) parent-azones))]
    (when-not (empty? not-defined-keys)
      (throw (Exception. (str "Zones " not-defined-keys " are not defined!")))))
  (->> parent-azones
       (map #(-> main-scheme key-azones % key-path-set))
       (apply clojure.set/union)))


;; selectors ----------------------------

(defn azone-row [keyname] (-> main-scheme key-azones keyname))

(defn- azones-find-* [azone-k parents?]
  (let [path-set (-> main-scheme key-azones azone-k key-path-set)]
    (filter (fn [z]
              (let [z-path-set (z key-path-set)]
                (and (not (= z-path-set path-set))
                     (if parents?
                       (clojure.set/subset? z-path-set path-set)
                       (clojure.set/subset? path-set z-path-set)))))
            (-> main-scheme key-azones vals))))

(defn- azones-find-this-* [azone-k parents?]
  (-> (azones-find-* azone-k parents?)
      (conj (-> main-scheme key-azones azone-k))))

(defn azones-find-childs  [azone-k] (azones-find-* azone-k false))
(defn azones-find-parents [azone-k] (azones-find-* azone-k true))

(defn azones-find-this-and-childs  [azone-k] (azones-find-this-* azone-k false))
(defn azones-find-this-and-parents [azone-k] (azones-find-this-* azone-k true))

;; END
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Price entity
;;* tag: <entity price>
;;*
;;* description: Таблица цен. Необходима для валидации соотношения обмена
;;*
;;**************************************************************************************************






;; END Price entity
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Azone validators functions
;;* tag: <azone validators functions>
;;*
;;* description: Функции валидации и проверки для зон
;;*
;;**************************************************************************************************

;; END
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN box directions
;;* tag: <box directions>
;;*
;;* description: напоравления движения коробок
;;*
;;**************************************************************************************************
                                        ;TODO: Отработать обнуление при задании новой схемы учета
(def meta-directions-> #{})

(defn direction-> [from-azone-k to-azone-k]
  (let [new-direction-> [from-azone-k to-azone-k]]
    (def meta-directions-> (conj meta-directions-> new-direction->))
    new-direction->))

(defn direction->-from-azone [[o _]] o)
(defn direction->-to-azone   [[_ o]] o)


(defn box-get-direction [box]
  (direction->
   (-> box box-get-from-zone :keyname)
   (-> box box-get-to-zone   :keyname)))

(defn boxes-get-directions-set [boxes]
  (reduce #(conj %1 (box-get-direction %2)) #{} boxes))


;; END box directions
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN opt defining metadata
;;* tag: <opt def meta>
;;*
;;* description: метаданные определения операций
;;*
;;**************************************************************************************************

(def key-directions-set :directions-set)
(def key-swops-set :swops-set)
(def key-owns :owns)
(def key-products :products)

(def meta-opttypes {})
(def meta-opts {})


;; типы операций
;;------------------------------------------------------------------------------
;; BEGIN: optype
;; tag: <optype>
;; description: Типы операций
;;------------------------------------------------------------------------------

(defn opttype-describe [row]
  (when (check-req-keys-in-map [:opttype :title] row)
    (let [{opttype-k :opttype
           :as new-opttype-row} (-> row
                                    (select-keys [:opttype :title :description])
                                    (update-in [:opttype] name)
                                    ((partial ix/com-save-for-field opttype :opttype)))]
      (def meta-opttypes (assoc meta-opttypes opttype-k new-opttype-row))
      opttype-k)))


;; Задаем старндартные типы операций

(defn opttype-std-prod+  [] (opttype-describe {:opttype :prod+   :title "приход товара"}))
(defn opttype-std-prod-  [] (opttype-describe {:opttype :prod-   :title "списание товара"}))
(defn opttype-std-prod-> [] (opttype-describe {:opttype :prod->  :title "перемещение товара"}))

(defn opttype-std-money+ [] (opttype-describe {:opttype :money+  :title "приход денег"}))
(defn opttype-std-money+ [] (opttype-describe {:opttype :money-> :title "перемещение денег"}))
(defn opttype-std-money- [] (opttype-describe {:opttype :money-  :title "списание денег"}))


(defn opttype-std-prod$      [] (opttype-describe {:opttype :prod$       :title "продажа товара"}))
(defn opttype-std-prod$-ret  [] (opttype-describe {:opttype :prod$-ret   :title "продажа товара"}))



;; END optype
;;..............................................................................




(defn opt-describe [row]
  (when (check-req-keys-in-map [:opt :opttype :title] row)
    (let [row (on-map-if-nil?-set-default-vals row {key-validators []
                                                    key-rules-for-operatio []
                                                    key-owns {}
                                                    key-products {}
                                                    key-description "нет описания"
                                                    key-directions-set #{}
                                                    key-swops-set #{}
                                                    key-troles-set #{}})
          opt-k (:opt row)]
      (transaction
       (def meta-opts
         (-> row
             ;; SQL
             (select-keys [:opt :opttype :title :description])
             (update-in [:opt]     name)
             (update-in [:opttype] name)
             ((partial ix/com-save-for-field opt :opt))
             ((partial merge row))
             ((partial assoc meta-opts opt-k))))
       opt-k))))

                                        ;TODO: Обязательно написать тесты
(defn opt-select-by-troles [troles-set]
  (->> meta-opts
       vals
       (filter (comp not
                     empty?
                     (partial clojure.set/intersection troles-set)
                     :troles-set))))


;; END opt defining
;;..................................................................................................


;;**************************************************************************************************
;;* BEGIN Opertio
;;* tag: <operatio>
;;*
;;* description: Выполнятор операций
;;*
;;**************************************************************************************************

;; TODO: написать тесты
(defn operatio-cr [webuser op-tip boxes] [webuser op-tip boxes])
(defn operatio-webuser    [[o _ _]] o)
(defn operatio-op-tip     [[_ o _]] o)
(defn operatio-boxes      [[_ _ o]] o)

;; TODO: написать тесты
(defn operatio? [operatio]
  (cond (empty? operatio) (throw (Exception. "вызов с пустым параметром"))
        (nil? (operatio-webuser  operatio)) (throw (Exception. "неуказан параметр webuser"))
        (nil? (operatio-op-tip operatio)) (throw (Exception. "неуказан параметр op-tip"))
        (nil? (operatio-boxes  operatio)) (throw (Exception. "неуказан параметр boxes"))
        (nil? (->> operatio operatio-op-tip meta-opts)) (throw (Exception. (str "операция " (operatio operatio-op-tip) " не определена")))
        :else true))


(defn do-operatio [operatio]
  (when (operatio? operatio)
    (let [opt-meta-row (->> operatio operatio-op-tip meta-opts)]
      ;; TODO: проработать проверки и тесты
      ;; 1. проверка на дублирование ящиков! - перенести в валидаторы

      ;; сделать проверку на права выполнения данной операции пользователем
      (when-not  (->> operatio
                      operatio-webuser
                      ix/webuserwebrole-own-get-rels-set
                      (some #(-> opt-meta-row key-troles-set (contains? %))))
        (throw (Exception. (str "вызов: " (operatio-op-tip operatio) " - "
                                " у пользователя " (operatio-webuser operatio) " нет прав для выполнения "
                                "данной операции."))))

      ;; Предварительная проверка - прогон валидаторов
      (doseq [validator (opt-meta-row key-rules-for-operatio)]
        (try
          (validator operatio)
          (catch Exception ex
            (throw (Exception. (str "вызов: " (operatio-op-tip operatio) " - "
                                    "выявлено невалидное состояние аргумента, сообщение валидатора: "
                                    (.getMessage ex)))))))

      ;; Проверка по правилам обмена
                                        ;TODO: надо реализовать
      ;;(doseq [box operatio-boxes]
      ;;  (println "Проверка про правилам обмена " box))



      ;;сделать проверку на зоны
      (let [allowed-directions-set (key-directions-set opt-meta-row)
            directions-set (->> operatio
                                operatio-boxes
                                boxes-get-directions-set)]
        (when-not (clojure.set/subset? directions-set allowed-directions-set)
          (throw (Exception. (str "Попытка провести следующие недопустимые направления: "
                                  (clojure.set/difference directions-set allowed-directions-set))))))


      ;; Цикл выполнения операции
      (loop [i 0]
        (if (> i 50) (throw (Exception. "50 errors: could not serialize access due to concurrent update"))
            (let [r (try
                      (transaction
                       (exec-raw  "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;")
                       (check-transaction-isolation-serializable)
                       (let [op-row (op-cr (operatio-webuser operatio) (operatio-op-tip operatio))]
                         (when (->> operatio ;; ПРОВЕДЕНИЕ ОПЕРАЦИИ
                                    operatio-boxes
                                    (map (partial move op-row))
                                    doall
                                    (reduce #(and %1 %2)))


                           ;; Пост проверка - проверка по зонам
                           ;; Проверяем вообще в итоге что получилось
                           (let [map-azones-id-key (main-scheme key-map-id-keyname)]
                             (->> operatio
                                  operatio-boxes
                                  (reduce #(conj %1
                                                 (acccas-find-root (box-get-from-zone %2)
                                                                   (box-get-from-own  %2)
                                                                   (box-get-webdoc    %2))
                                                 (acccas-find-root (box-get-to-zone   %2)
                                                                   (box-get-to-own    %2)
                                                                   (box-get-webdoc    %2)))
                                          [])
                                  (map (fn [accca-row]
                                         (->> accca-row
                                              accca-get-acc
                                              :zone_id
                                              map-azones-id-key
                                              azones-find-this-and-parents
                                              (map
                                               (fn [{rules key-rules keyname key-keyname :as azone-row}]
                                                 ;;(println "Валидация в зоне " keyname validators)
                                                 (try
                                                   (->> rules
                                                        (map (fn [rule-validator]
                                                               (if (rule-validator accca-row) true
                                                                   (throw (Exception.
                                                                           (str "вызов: " ;;;;;;(operatio-op-tip operatio)
                                                                                " - "
                                                                                "выявлено нарушение в зоне " keyname
                                                                                " валидатор выполнился но не вернул"
                                                                                " true"))))))
                                                        (reduce #(and %1 %2) true))
                                                   (catch Exception ex
                                                     (throw (Exception.
                                                             (str "вызов: " ;;;;;;(operatio-op-tip operatio)
                                                                  " - "
                                                                  "выявлено нарушение в зоне "
                                                                  keyname " сообщение валидатора: "
                                                                  (.getMessage ex))))))))
                                              (reduce #(and %1 %2) true))))
                                  (reduce #(and %1 %2) true)))
                           )))

                      (catch org.postgresql.util.PSQLException e
                        (do (println ">>>>>>>>>>>>>>>" (.getSQLState e))
                            (cond (= "40001" (-> e .getSQLState)) false
                                  (= "40P01" (-> e .getSQLState)) false
                                  :else (throw e))))
                      (catch java.sql.BatchUpdateException e
                        (let [et (type e)]
                          (println ">EEE>"  et " >> " (.getMessage e))
                          false))
                      )]
              (if (true? r) true
                  (do (Thread/sleep 100)
                      (recur (inc i))))))))))

;; END Opertio
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN swop engine
;;* tag: <swop engine entity>
;;*
;;* description: система контроля операций обмена
;;*
;;**************************************************************************************************


(defentity swop
  (pk :id)
  (prepare (fn [row]
             (-> row
                 ((partial ix/prepare-as-string :swop)))))

  (transform (fn [row]
               (-> row
                   ((partial ix/transform-as-keyword :swop)))))
  )


(defentity swopv
  (pk :id)
  (prepare (fn [row]
             (-> row
                 ((partial ix/prepare-as-string :swop))
                 ((partial ix/prepare-as-string :opt))
                 ((partial ix/prepare-as-string :opttype)))))

  (transform (fn [row]
               (-> row
                   ((partial ix/transform-as-keyword :swop))
                   ((partial ix/transform-as-keyword :opt))
                   ((partial ix/transform-as-keyword :opttype)))))
  )


(defentity swopva
  (pk :id)
  (prepare (fn [row]
             (-> row
                 ((partial ix/prepare-as-string :keyname))
                 prepare-fns-trans-for-tip-val)))

  (transform (fn [row]
               (-> row
                   ((partial ix/transform-as-keyword :swop))
                   ((partial ix/transform-as-keyword :keyname))
                   transform-fns-trans-for-tip-val)))
  )


;; Метаданные для цен
(def meta-swops {})


(def key-swop :swop)
(def key-swop-variants :variants)
(def key-swop-sql-prefix :sql-prefix)

;;TODO: Написать тесты
(defn swop-describe [row]
  (when (check-req-keys-in-map [key-swop key-title key-swop-sql-prefix] row)
    (let [row (on-map-if-nil?-set-default-vals row {key-description "нет описания"
                                                    key-swop-variants []})
          swop-k (:swop row)]
      (transaction
       (def meta-swops
         (-> row
             ;; SQL
             (select-keys [:swop :description])
             (update-in [:swop] name)
             ((partial ix/com-save-for-field swop :swop))
             ;; DEF
             ((partial merge row))
             ((partial assoc meta-swops swop-k))))
       swop-k))))


;;TODO: Написать тесты
(defn swop-get-current-structure
  "вытащить оценочную структуру"
  [{opt-type :opttype opt-key :opt :as opt-row}
   {swebdoc-id :id :as swebdoc-row}  swop-key]
  (transaction
   ;;получить для начала объект swopv
   (let [select-result (select swopv (where (and (= :swop (name swop-key))
                                                 (= :opt (name opt-key))
                                                 (= :opttype (name opt-type))
                                                 (= :swebdoc_id swebdoc-id))))
         {swopv-id :id :as swopv-row} (if (not (empty? select-result)) (first select-result)
                                          (insert swopv (values {:swop (name swop-key)
                                                                 :opt (name opt-key)
                                                                 :opttype (name opt-type)
                                                                 :swebdoc_id swebdoc-id})))]

     ;;далее получить необходимый набор атрибутов swopva
     (letfn [(get-current-attr [keyname {webdoc-id :id ca-id :ca_id tip-k :tip :as webdoc-row}]
               (let [result (select swopva (where (and (= :swopv_id swopv-id)
                                                       (= :webdoc_id webdoc-id))))]
                 (if (not (empty? result)) (first result)
                     (insert swopva (values {:swopv_id swopv-id
                                             :swop (name swop-key)
                                             :swebdoc_id swebdoc-id
                                             :webdoc_id webdoc-id
                                             :ca_id ca-id
                                             :tip tip-k
                                             :val (let [m (-> meta-tips tip-k)]  ;; Дублируется из accca
                                                    (cond (-> m :get-default-val-from-webdoc-fn nil? not) ((m :get-default-val-from-webdoc-fn) webdoc-row)
                                                          (-> m :default-value nil? not) (ax-cr tip-k (m :default-value))
                                                          :else (ax-cr tip-k nil)))
                                             :keyname (name keyname)
                                             })))))]
       (-> swop-key
           meta-swops
           (assoc :swopv swopv-row)
           (update-in [key-swop-variants] #(reduce (fn [a [k v]]
                                                     (assoc a k (get-current-attr k v)))
                                                   {} (seq %1)))
           )))))


                                        ;TODO: Написать тесты
(defn swopv-update [{id :id :as swopv-row}]
  (update swopv (set-fields swopv-row) (where (= :id id))))

                                        ;TODO: написать тесты
(defn swopva-update [{id :id :as swopva-row}]
  (update swopva (set-fields swopva-row) (where (= :id id))))

(defn swop-select-all-opts []
  (filter #(-> % :swops-set empty? not) (vals meta-opts)))

(defn swop-select-all-opts-with-swop-rows []
  (->> (swop-select-all-opts)
       (map #(update-in % [key-swops-set] (partial map meta-swops)))))

(defn swop-opts-and-swops-by-webdoc [webdoc-row]
  (transaction
   (->> (swop-select-all-opts)
        (map (fn [opt-group]
               (update-in opt-group [key-swops-set]
                          (partial map (partial swop-get-current-structure opt-group webdoc-row))))))))

(defn swop-opts-and-swops-by-webdoc-id [id]
  (transaction
   (swop-opts-and-swops-by-webdoc (ix/com-find webdoc id))))








;; END swop engine
;;..................................................................................................




;;**************************************************************************************************
;;* BEGIN VALIDATORS
;;* tag: <validators>
;;*
;;* description: валидаторы
;;*
;;**************************************************************************************************

(def validator-provocator!
  (fn [_]
    (println "!!!провоцируем ошибку валидации!!!")
    (throw (Exception. "тестовая невалидная проверка - от провокатора"))))

;;------------------------------------------------------------------------------
;; BEGIN: rules validators for operatio
;; tag: <rules operatio validators>
;; description: валидаторы для описания операций
;;------------------------------------------------------------------------------

(defn rule-o-boxes-directions-check [direction check-fn?]
  (fn [operatio]
    (->> operatio
         operatio-boxes
         (filter #(= direction (box-get-direction %)))
         (reduce #(and %1 (check-fn? %2)) true))))


(defn rule-o-boxes-directions-tips [direction tips-set]
  (rule-o-boxes-directions-check
   direction
   (fn [box]
     (let [tip-k (-> box box-get-webdoc :tip)]
       (if (contains? tips-set tip-k) true
           (throw (Exception. (str "в данной операции в направлении из зоны "
                                   (direction->-from-azone direction)
                                   " в зону "
                                   (direction->-to-azone   direction)
                                   " для проведения в данном направлении допускаются следующие типы "
                                   tips-set " была попытка провести " tip-k))))))))







;; END rules validators for operatio
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: rules validators for post exec do-operatio
;; tag: <rules post validators do-operatio>
;; description: валидаторы для проверки после выполнения операции
;;------------------------------------------------------------------------------



;; END rules validators for post exec do-operatio
;;..............................................................................

;;------------------------------------------------------------------------------
;; BEGIN: rules validators for zones
;; tag: <rules post validators do-operatio>
;; description: валидаторы для проверки после выполнения операции
;;------------------------------------------------------------------------------



;; END rules validators for zones
;;..............................................................................


(defn rule-zone-vals>0 [accca]
  (let [v (accca-get-acc-val accca)]
    (if (ax>= v (ax-zero v)) true
        (throw (Exception. "Значения в данной зоне не могут быть меньше нуля!")))))

(defn rule-zone-tips [tips]
  (fn [accca]
    (let [tip-k (-> accca accca-get-acc-val ax-tag)]
      (if (contains? tips tip-k) true
          (throw (Exception. (str "в данной зоне недопускается использование типа " tip-k )))))))

;; END VALIDATORS
;;..................................................................................................




;;**************************************************************************************************
;;* BEGIN Products lists select tools
;;* tag: <products list select tools>
;;*
;;* description: Функционал формирования информационного списка товаров
;;*
;;**************************************************************************************************


(defn fill-product-row [{id :id :as row} {:keys [swops?
                                                 accas?
                                                 ]}]
  (transaction
   (-> row
       
       (as-> row
           (if swops?
             (assoc row :swops (select swopva (where (= :swebdoc_id id))))
             row))

       (as-> row
           (if accas?
             (assoc row :accas (select acc (where (= :webdoc_id id))))
             row))
       )))


(defn fill-product-rows [opts rows]
  (transaction (doall (map #(fill-product-row % opts) rows))))


;; END Products lists select tools
;;..................................................................................................










;;**************************************************************************************************
;;* BEGIN Superselect
;;* tag: <superselect>
;;*
;;* description: Суперзарос для выбора данных по товару
;;*
;;**************************************************************************************************

(defn tip-select*-add-fields-for-prefix-table-name [query* table-name & [also-fields]]
  (let [table-name (name table-name)]
    (->> meta-tips
         vals
         (mapcat :sql-field)
         (into [:ca_id :tip])
         (into (or also-fields []))
         (map
          (fn [k]
            (let [o (keyword (str table-name "." (name k)))]
              [o o])))
         (reduce #(fields %1 %2) query*))))

;; Общая функция для выборки записей по префиксу
(defn transform-ax-val-for-prefix [row prefix conj-group-key & [also-fields fields-fns post-row-fn]]
  (let [sql-fields (->> meta-tips
                        vals
                        (mapcat :sql-field)
                        (into [:ca_id :tip])
                        (into (or also-fields []))
                        (reduce
                         (fn [a k]
                           (assoc a (keyword (str (name prefix) "." (name k))) k))
                         {}))

        kk (keys sql-fields)

        v-0  (-> row
                 (select-keys kk)
                 seq

                 (as-> row
                     (reduce
                      (fn [a [k v]]
                        (let [k (sql-fields k)]
                          (assoc a k
                                 (if fields-fns
                                   (if-let [f (fields-fns k)]
                                     (f v) v)
                                   v))))
                      {} row))
                 )

        ;;_ (println prefix ">>>>>" v-0)
        v (if (nil? (:tip v-0))
            nil
            (-> v-0 (update-in [:tip] keyword) transform-fns-trans-for-tip-val))

        next-row (reduce dissoc row kk)
        ]

    (if v
      (-> next-row
          (update-in [conj-group-key]
                     (fn [conj-group-key]
                       (if conj-group-key (conj conj-group-key v) [v]) )))
      next-row)))


;;----------------------------------------------------------
;; BEGIN: Построение выбора цен
;; tag: <>
;; description:
;;----------------------------------------------------------

(defn webdoc-swop-join* [webdoc-select* swaps-keys-seq]
  (if (empty? swaps-keys-seq) webdoc-select*
      (let [swaps-set (set swaps-keys-seq)]
        (->> meta-swops
             vals
             (filter (comp (partial contains? swaps-set) key-swop))
             (reduce
              (fn [select-webdoc* {swop key-swop sql-prefix key-swop-sql-prefix}]
                (let [sql-prefix (name sql-prefix)]
                  (-> select-webdoc*
                      (join "left outer" [swopva (keyword sql-prefix)]
                            (and (= (keyword (str sql-prefix ".swebdoc_id")) :webdoc.id)
                                 (= (keyword (str sql-prefix ".swop")) (name swop))))
                      (tip-select*-add-fields-for-prefix-table-name
                       sql-prefix [:id :swopv_id :swop :keyname :swebdoc_id]))))
              webdoc-select*)))))

(defn webdoc-swop-transfom-tips [swaps-keys-seq rows]
  (if (empty? swaps-keys-seq) rows
      (let [swaps-set (set swaps-keys-seq)
            swaps (->> meta-swops
                       vals
                       (filter (comp (partial contains? swaps-set) key-swop))
                       (map (comp name key-swop-sql-prefix)))]
        (map
         (fn [row]
           (reduce #(transform-ax-val-for-prefix
                     %1 %2 :swops
                     [:id :swopv_id :swop :keyname :swebdoc_id]
                     {:swop keyword}) row swaps))
         rows))))


;; END Построение выбора цен
;;..........................................................

;; ;;----------------------------------------------------------
;; ;; BEGIN: Построение зароса по зонам
;; ;; tag: <>
;; ;; description:
;; ;;----------------------------------------------------------


;; ;;(->> :store-1 azones-find-this-and-childs (map :keyname))

;; (defn webdoc-acc-for-zones* [webdoc-select* zones-keys-set]
;;   (if (empty? zones-keys-set) webdoc-select*
;;       (->> main-scheme
;;            :azones
;;            vals
;;            (filter (comp (partial contains? zones-keys-set) :keyname))
;;            (map #(select-keys % [key-sql-prefix-k :id]))
;;            (reduce
;;             (fn [webdoc-select* {:keys [sql-prefix id]}]
;;               (let [sql-prefix (name sql-prefix)]
;;                 (-> webdoc-select*
;;                     (join "left outer" [acc (keyword sql-prefix)]
;;                           (and (= (keyword (str sql-prefix ".webdoc_id")) :webdoc.id)
;;                                (= (keyword (str sql-prefix ".zone_id")) id)
;;                                (= (keyword (str sql-prefix ".ca_parent_id")) nil)))
;;                     (tip-select*-add-fields-for-prefix-table-name
;;                      sql-prefix [:ca_id :zone_id :own_id :id]))))
;;             webdoc-select*))))

;; (defn webdoc-transfom-tips-for-zones [zones-keys-set rows]
;;   (if (empty? zones-keys-set) rows
;;       (let [sql-prefix-s (->> main-scheme
;;                               :azones
;;                               vals
;;                               (filter (comp (partial contains? zones-keys-set) :keyname))
;;                               (map key-sql-prefix-k))]
;;         (map
;;          (fn [row]
;;            (reduce #(transform-ax-val-for-prefix
;;                      %1 %2 :accass
;;                      [:ca_id :zone_id :own_id :id]
;;                      nil
;;                      (fn [row]
;;                        (println row)
;;                        row)

;;                      ) row sql-prefix-s))
;;          rows))))

;; ;; END Построение зароса по зонам
;; ;;..........................................................



;; (def webdoc-select-for-products-list*
;;   (-> webdoc-select*
;;       (fields :id :scod :keyname :description
;;               :tip :ca_id :val_bigint :val_money :val_numeric :val_ratio_num :val_ratio_denum
;;               :cdate :udate)
;;       webdoc-pred-product?))

;; (defn webdocs-products-list [{:keys [page
;;                                      page-size
;;                                      fts-query
;;                                      product-tips
;;                                      swops-keys-set
;;                                      zones-keys-set]
;;                               :or {page 1
;;                                    page-size 10
;;                                    fts-query ""
;;                                    product-tips nil}}]
;;   (transaction
;;    (->> (-> webdoc-select-for-products-list*

;;             (ix/com-pred-page* (dec page) page-size)

;;             (as-> query
;;                 (let [fts-query (clojure.string/trim fts-query)]
;;                   (if (empty? fts-query)
;;                     query
;;                     (ix/webdoc-pred-search? query fts-query))))

;;             (as-> query
;;                 (if (empty? product-tips)
;;                   query
;;                   (webdoc-pred-product-tips? query product-tips)))



;;             (webdoc-swop-join* swops-keys-set)

;;             (webdoc-acc-for-zones* zones-keys-set)


;;             (order :id :desc)

;;             ;;as-sql
;;             ix/com-exec
;;             ) ;;Выполнение запроса

;;         ;; Далее идет пост обработка ----------------------------------

;;         ;; Преобразование цен
;;         (webdoc-swop-transfom-tips swops-keys-set)

;;         ;; Преобразование счетов
;;         (webdoc-transfom-tips-for-zones zones-keys-set)

;;         )))




;; ;; END Superselect
;; ;;..................................................................................................
