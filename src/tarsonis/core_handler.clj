(ns tarsonis.core-handler
  (:use compojure.core)
  (:use hiccup.core)
  (:use hiccup.page)
  (:use hiccup.form)
  (:use hiccup.element)
  (:use hiccup.util)

  ;;(:use ixinfestor.transit)


  (:require [clojure.java.io :as io]
            [ring.middleware.reload :refer (wrap-reload)] ;; reload temlates
            [ring.middleware.json :as json]
            [ring.middleware.session :as session]
            [ring.middleware.multipart-params :as multipart]

            [compojure.handler :as handler]
            [compojure.route :as route]

            [korma.core :as korc]

            [ixinfestor.core :as ix]
            [ixinfestor.core-web :as cw]
            [ixinfestor.core-web-bootstrap :as cwb]

            [ixinfestor.transit :as transit]

            [tarsonis.core :as ts]

            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])

            )
  )

;;**************************************************************************************************
;;* BEGIN Owners reference book
;;* tag: <own ref book>
;;*
;;* description: Справочник управления пользователями
;;*
;;**************************************************************************************************

(defn rest-own-search [{{:keys [page page-size fts-query arls]
                         :or {page 1 page-size 10
                              fts-query "" arls nil}} :params
                              :as request}]
  (-> ts/own-select*
      (ix/com-pred-page* (dec page) page-size)

      (as-> query
          (let [fts-query (clojure.string/trim fts-query)]
            (if (empty? fts-query)
              query
              (ts/own-pred-search? query fts-query))))

      (as-> query
          (if (empty? arls)
            query
            (->> arls
                 (map ts/arl-find-for-keyname)
                 (ts/own-pred-by-arls? query))))

      (korma.core/order :id :desc)
      ix/com-exec
      korma.db/transaction))

(defn rest-own-edit [{{id :id} :params :as request}]
  {:row (if id (ix/com-find ts/own id) {})
   :arls (let [own-arls-set (ts/ownarl-own-get-rels-set {:id id})]
           (map (fn [{keyname :keyname :as arl-row}]
                  (assoc arl-row :inc? (contains? own-arls-set keyname)))
                (korc/select ts/arl)))
   })

(defn rest-own-save [{{:keys [row arls]} :params}]
  (let [row (ix/com-save-for-id ts/own row)]
    (->> arls
         (filter :inc?)
         (ts/ownarl-add-rels row))
    row))

(defn rest-own-delete [request]
  (->> request
       :params
       :id
       (ix/com-delete-for-id ts/own)
       (assoc {} :result)))


(defn routes-own* [roles-set]
  (routes
   (context "/tc/rb/own" []

            ;; TRANSIT
            (POST "/search" request
                  (-> request
                      rest-own-search
                      transit/response-transit))

            (POST "/edit" request
                  (-> request
                      rest-own-edit
                      transit/response-transit))

            (POST "/save" request
                  (friend/authorize
                   roles-set
                   (->> request
                        rest-own-save
                        (transit/response-transit))))

            (POST "/delete" request
                  (friend/authorize
                   roles-set
                   (->> request
                        rest-own-delete
                        transit/response-transit)))

            )))

;; END Owners reference book
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN product routes
;;* tag: <product routes rest>
;;*
;;* description: Вебсервыисы для работы с продуктами
;;*
;;**************************************************************************************************


(defn rest-product-search [{{:keys [page page-size fts-query product-tips]
                             :or {page 1
                                  page-size 10
                                  fts-query ""
                                  product-tips nil}} :params
                                  :as request}]
  (->> (-> ts/webdoc-select*
           (ix/com-pred-page* (dec page) page-size)

           (as-> query
               (let [fts-query (clojure.string/trim fts-query)]
                 (if (empty? fts-query)
                   query
                   (ix/webdoc-pred-search? query fts-query))))

           ts/webdoc-pred-product?

           (as-> query
               (if (empty? product-tips)
                 query
                 (ts/webdoc-pred-product-tips? query product-tips)))

           (korma.core/order :id :desc)
           ix/com-exec)

       (ts/fill-product-rows {:swops? true
                              :accas? true})

       korma.db/transaction))


(defn rest-product-search-for-scod [{{:keys [scod]} :params :as request}]
  (-> ts/webdoc-select*
      (korma.core/where (= :scod scod))
      ts/webdoc-pred-product?
      ix/com-exec

      (as-> result
          (if (empty? result)
            (throw
             (Exception.
              (str "Товар по коду: " scod " не найден.")))
            (first result)))

      (as-> webdoc-row
          (let [{:keys [ca_id val]} webdoc-row]
            (->> ca_id
                 ts/ca-get-for-id
                 (map #(assoc % :val val))
                 vec
                 (assoc webdoc-row :caval))))))

(defn rest-product-edit [{{id :id} :params :as request}]
  (-> {:row (if id (ix/com-find ts/webdoc id) {})}
      ;;spec-edit-fn
      (assoc-in [:rb :ca-list] (ts/ca-main-list))))


(defn rest-product-save [request]
  (-> request
      :params
      ts/webdoc-save))

(defn rest-product-tags-edit-table [request]
  (-> request
      :params
      (ix/webdoctag-tag-tree-as-flat-groups-with-patches :path)))

(defn rest-product-tags-save [{{:keys [product-id
                                       webdoctag-edit-table]} :params}]
  (->> webdoctag-edit-table
       (reduce concat)
       (filter :contain?)
       (ix/webdoctag-update-tags {:id product-id}))
  {:result :ok})

(defn rest-product-swaps [request]
  (->> request
       :params
       :id
       (ts/swop-opts-and-swops-by-webdoc-id)
       (map (fn [row]
              (-> row
                  (update-in [:swops-set]
                             (partial map #(select-keys % [:id :measure :swop
                                                           :title :description
                                                           :variants :measure
                                                           :swopv])))
                  (select-keys [:description :title :id :opttype :opt :swops-set])
                  )))))

(defn rest-product-swaps-save [{{swops :swaps} :params}]
  (doseq [{:keys[swops-set]} swops]
    (doseq [{:keys[variants swopv]} swops-set]
      (do
        (ts/swopv-update swopv)
        (doseq [variant (vals variants)]
          (ix/com-save-for-id ts/swopva variant)))))
  {:result :ok})


(defn rest-product-delete [request]
  (->> request
       :params
       (ts/webdoc-delete)
       (assoc {} :result)))





(defn rest-product-images-list [request]
  (-> request
      :params
      (ix/files_rel-select-files-by-* :webdoc_id)
      ix/file-pred-images*
      ix/com-exec))

(defn rest-product-files-list [request]
  (-> request
      :params
      (ix/files_rel-select-files-by-* :webdoc_id)
      (ix/file-pred-images* :not-image)
      ix/com-exec))


(defn rest-product-files-rel-delete [{{:keys [id file-id]} :params}]
  (ix/files_rel-delete :webdoc_id {:id file-id} {:id id})
  {:result "OK"})



(defn rest-product-upload-image [request id]
  (ix/web-file-upload
   (partial
    ix/file-upload-rel-on-o ts/webdoc :webdoc_id
    {:id (Long/parseLong id)}
    {:path-prefix "/image/"})
   (-> request :params :uploader))
  "OK")

(defn rest-product-upload-file [request id]
  (ix/web-file-upload
   (partial
    ix/file-upload-rel-on-o ts/webdoc :webdoc_id
    {:id (Long/parseLong id)}
    {:path-prefix "/file/"})
   (-> request :params :uploader))
  "OK")



(defn routes-product* [roles-set]
  (routes
   (context "/tc/rb/product" []

            (POST "/search" request
                  (-> request
                      rest-product-search
                      transit/response-transit))

            (POST "/search/scod" request
                  (-> request
                      rest-product-search-for-scod
                      transit/response-transit))

            (POST "/edit" request
                  (-> request
                      rest-product-edit
                      transit/response-transit))

            (POST "/save" request
                  (friend/authorize
                   roles-set
                   (-> request
                       rest-product-save
                       transit/response-transit)))

            (POST "/tags-edit-table" request
                  (-> request
                      rest-product-tags-edit-table
                      transit/response-transit))

            (POST "/tags-edit-table/save" request
                  (friend/authorize
                   roles-set
                   (-> request
                       rest-product-tags-save
                       transit/response-transit)))

            (POST "/swops" request
                  (-> request
                      rest-product-swaps
                      transit/response-transit))

            (POST "/swops/save" request
                  (friend/authorize
                   roles-set
                   (-> request
                       rest-product-swaps-save
                       transit/response-transit)))

            (POST "/delete" request
                  (friend/authorize
                   roles-set
                   (-> request
                       rest-product-delete
                       transit/response-transit)))

            ;; ---------------------------------------------------------------
            ;; TRANSIT
            (POST "/images-list" request
                  (-> request
                      rest-product-images-list
                      transit/response-transit))

            ;; TRANSIT
            (POST "/files-list" request
                  (-> request
                      rest-product-files-list
                      transit/response-transit))

            ;; TRANSIT
            (POST "/files_rel/delete" request
                  (friend/authorize
                   roles-set
                   (-> request
                       rest-product-files-rel-delete
                       transit/response-transit)))



            (context "/upload/:id" [id]

                     (multipart/wrap-multipart-params
                      (POST "/image" request
                            (friend/authorize
                             roles-set
                             (ring.util.response/response
                              (rest-product-upload-image request id)
                              ))))

                     (multipart/wrap-multipart-params
                      (POST "/file" request
                            (friend/authorize
                             roles-set
                             (ring.util.response/response
                              (rest-product-upload-file request id)
                              ))))
                     )

            ;; --------------------------------------------------------------


            )))

;; END product routes
;;..................................................................................................



;;**************************************************************************************************
;;* BEGIN OP MOVE
;;* tag: <op move>
;;*
;;* description: Операция перемещения
;;*
;;**************************************************************************************************



(defn rest-do-operatio [{{{current :current} :cemerick.friend/identity} :session
                         {{:keys [opt-key items]} :operatio} :params :as request}]

  (println "***************************************************************************************")
  (println "DO OPERATIO START :")

  (-> (ts/operatio-cr (ix/webuser-find-by-username current)
                      opt-key
                      (->> items
                           (map (fn [{:keys [product-id
                                             caval
                                             from-zone-key
                                             from-own-id
                                             to-zone-key
                                             to-own-id]}]

                                  (reduce

                                   (fn [box {:keys [id val]}]
                                     (ts/box-set-op-item-val-for-ca box {:id id} val))

                                   (ts/box-cr
                                    (ts/azone-row from-zone-key)
                                    (ix/com-find ts/own from-own-id)
                                    (ts/azone-row to-zone-key)
                                    (ix/com-find ts/own to-own-id)
                                    (ix/com-find ts/webdoc product-id))

                                   caval)) )
                           ;;doall
                           ;;clojure.pprint/pprint
                           ))
      ;;clojure.pprint/pprint
      ts/do-operatio)

  (println "DO OPERATIO END")
  {:result :ok})

;; (defn page-op [{{:keys [op-key opt]} :params}]
;;     (cwb/template-main
;;      {}
;;      (list
;;       ;;[:nav#top-nav {:class "navbar navbar-default navbar-fixed-top"}]
;;       [:div#main]
;;       (javascript-tag (str "opt = '" opt "';"))
;;       (condp = op-key
;;         "prod->" (include-js "/js/c/op-move/op-move.js")
;;         (str "Неопределенный ключ: " op-key)))))



(def version (.getTime (new java.util.Date)))

(defn page-op [request]
  (cwb/template-main
   {:header-additions
    (list
     [:style "body {margin-top: 52px;}"]
     )}
   (list
    [:div#top-nav]
    [:div#main]
    (include-js (str "/js/c/tarsonis/main.js?v=" version)))))


(defn routes-ops* [{:keys [users-roles-set admin-roles-set]
                    :or {users-roles-set #{:user}
                         admin-roles-set #{:admin}}}]
  (let [users-roles-set (clojure.set/union users-roles-set admin-roles-set)]
    (routes
     (context
      "/tc/opt" []

      (ANY "/" request
           (friend/authorize users-roles-set
                             (page-op request)))


      (POST "/meta" request
            (friend/authorize
             users-roles-set
             (transit/response-transit
              (let [current (get-in request [:session
                                             :cemerick.friend/identity
                                             :current
                                             ])]
                {:authentication {:user (get-in request [:session
                                                         :cemerick.friend/identity
                                                         :authentications
                                                         current])
                                  }
                 :meta-opt (->> current
                                ix/webuser-find-by-username
                                ix/webuserwebrole-own-get-rels-set
                                ts/opt-select-by-troles
                                (reduce (fn [a {opt :opt :as r}]
                                          (assoc a opt
                                                 (select-keys r [:directions-set
                                                                 :id
                                                                 :opt
                                                                 :opttype
                                                                 :owns
                                                                 :swops-set
                                                                 :title
                                                                 :troles-set
                                                                 :validators
                                                                 :description
                                                                 :products
                                                                 ;;:rules-for-operatio
                                                                 ])))
                                        {}))
                 }))))



      (context "/rest" []

               (POST "/ax/calc" request
                     (->> request
                          :params
                          :expr
                          ts/ax-calc-symbolic-expression
                          (assoc {} :result)
                          transit/response-transit))
               )


      (POST "/op/do-operatio" request
            (-> request
                rest-do-operatio
                transit/response-transit))





      ))))


;; END OP MOVE
;;..................................................................................................


