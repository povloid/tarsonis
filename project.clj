(defproject tarsonis "0.1.1"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [ixinfestor "0.1.3"] ;; <-- CLOJURESCRIPT LIBRARIES TO

                 ;; CLOJURESCRIPT LIBRARIES
                 [org.clojure/clojurescript "1.7.48"]
                 ]

  :plugins [[lein-ancient "0.6.7"]
            [cider/cider-nrepl "0.9.1"]]

  :source-paths ["src" "src-cljs"]

  :aot [tarsonis.core
        tarsonis.core-handler]

  ;;:omit-source true
  :jar-exclusions [#"(?:^|tarsonis/)*.clj\z"]


  )
